running...
Reading polygon list
Reading table list
reading weight list
running gold code...
gold run time is 34.47 s
running cuda code...
command is all
command is all

=======  running cuda initiate P7  ======
comparing result...
Gold 0.0808933 relative error inf
Gold 0.0393128 error 29372.7
max error = 29372.7
cuda PIXEL_7 run time is 1 s
speed up is 34.47 X



=======  running cuda LUT_2  ======
allocating device memory...
start kernel...
comparing result...
Gold 7.57168e-09 relative error 0.0025865
Gold 0.100008 error 3.47197e-06
max error = 3.47197e-06
cuda LUT_2 run time is 16.25 s
speed up is 2.12123 X



=======  running cuda LUT_3  ======
allocating device memory...
start kernel...
comparing result...
Gold 7.57168e-09 relative error 0.0025865
Gold 0.0871086 error 3.33041e-06
max error = 3.33041e-06
cuda LUT_3 run time is 15.52 s
speed up is 2.22101 X



=======  running cuda PIXEL_1  ======
tilewidth = 129, tileheight = 49
comparing result...
Gold 0.00293053 relative error 1.65259e-05
Gold 0.158824 error 1.2368e-06
max error = 1.2368e-06
cuda PIXEL_1 run time is 1.22 s
speed up is 28.2541 X



=======  running cuda PIXEL_2  ======
comparing result...
Gold 0.00315912 relative error 7.29636e-06
Gold 0.155344 error 3.8743e-07
max error = 3.8743e-07
cuda PIXEL_2 run time is 0.78 s
speed up is 44.1923 X



=======  running cuda PIXEL_3  ======
comparing result...
Gold 0.00315912 relative error 7.29636e-06
Gold 0.155344 error 3.8743e-07
max error = 3.8743e-07
cuda PIXEL_3 run time is 0.87 s
speed up is 39.6207 X



=======  running cuda PIXEL_4  ======
comparing result...
Gold 0.00315912 relative error 7.29636e-06
Gold 0.155344 error 3.8743e-07
max error = 3.8743e-07
cuda PIXEL_4 run time is 0.91 s
speed up is 37.8791 X



=======  running cuda PIXEL_5  ======
Overhead Time is 0
comparing result...
Gold 0.00315912 relative error 7.29636e-06
Gold 0.155344 error 3.8743e-07
max error = 3.8743e-07
cuda PIXEL_5 run time is 0.58 s
speed up is 59.431 X



=======  running cuda PIXEL_6  ======
clock_start

comparing result...
Gold 0.00293053 relative error 1.65259e-05
Gold 0.158824 error 1.2368e-06
max error = 1.2368e-06
cuda PIXEL_6 run time is 3.08 s
speed up is 11.1916 X



=======  running cuda PIXEL_7  ======
comparing result...
Gold 0.0808933 relative error inf
Gold 0.0393128 error 29372.7
max error = 29372.7
cuda PIXEL_7 run time is 0.97 s
speed up is 35.5361 X



=======  running cuda PIXEL_8  ======
Overhead Time is 0
comparing result...
Gold 1.00532e-11 relative error inf
Gold 0.173814 error 0.000294581
max error = 0.000294581
cuda PIXEL_8 run time is 0.58 s
speed up is 59.431 X



======== running cuda UCLA =========
comparing result...
Gold 0.00487519 relative error 0.00444196
Gold 0.147782 error 0.000313729
max error = 0.000313729
cuda UCLA run time is 35.55 s
speed up is 0.96962 X



=======  running cuda SHARE_1  ======
Overhead Time is 0.01
comparing result...
Gold 0.00315912 relative error 7.29636e-06
Gold 0.155344 error 3.8743e-07
max error = 3.8743e-07
cuda SHARE_1 run time is 0.88 s
speed up is 39.1705 X


