/*
 * Copyright 1993-2007 NVIDIA Corporation.  All rights reserved.
 *
 * NOTICE TO USER:
 *
 * This source code is subject to NVIDIA ownership rights under U.S. and
 * international Copyright laws.  Users and possessors of this source code
 * are hereby granted a nonexclusive, royalty-free license to use this code
 * in individual and commercial software.
 *
 * NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE
 * CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR
 * IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS,  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION,  ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOURCE CODE.
 *
 * U.S. Government End Users.   This source code is a "commercial item" as
 * that term is defined at  48 C.F.R. 2.numIterations1 (OCT 1995), consisting  of
 * "commercial computer  software"  and "commercial computer software
 * documentation" as such terms are  used in 48 C.F.R. 12.212 (SEPT 1995)
 * and is provided to the U.S. Government only as a commercial end item.
 * Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through
 * 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the
 * source code with only those rights set forth herein.
 *
 * Any use of this source code in individual and commercial software must
 * include, in the user documentation and internal comments to the code,
 * the above Disclaimer and U.S. Government End Users Notice.
 */
 
/* Matrix transpose with Cuda 
 * Host code.

 * This example transposes arbitrary-size matrices.  It compares a naive
 * transpose kernel that suffers from non-coalesced writes, to an optimized
 * transpose with fully coalesced memory access and no bank conflicts.  On 
 * a G80 GPU, the optimized transpose can be more than 10x faster for large
 * matrices.
 */

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// includes, project
#include <cutil.h>

// includes, kernels
#include <litho_kernel.cu>
#define NumBlocks 12
////////////////////////////////////////////////////////////////////////////////
// declaration, forward
void runTest( int argc, char** argv);
extern "C" 
void lithoGold(const float* kernel,const int* input_x,const int* input_y, float* img_data);


   
////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int
main( int argc, char** argv) 
{
    runTest( argc, argv);

    CUT_EXIT(argc, argv);
}

////////////////////////////////////////////////////////////////////////////////
//! Run a simple test for CUDA
////////////////////////////////////////////////////////////////////////////////
void
runTest( int argc, char** argv) 
{
    // size of the matrix
	const unsigned int size_x=400;
	const unsigned int size_y=400;
    // size of memory required to store the matrix
    const unsigned int mem_size = sizeof(float) * size_x * size_y;
    
     cudaChannelFormatDesc floatTex = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
    unsigned int timer;
    cutCreateTimer(&timer);
  int numIterations = 2000;
    CUT_DEVICE_INIT();

    // allocate host memory
    float* kernel_CPU = (float*) malloc(mem_size);
    int * input_x_CPU=(int*) malloc(2*N*sizeof(int));
    int * input_y_CPU=(int*) malloc(2*N*sizeof(int));
    float* img_data_CPU = (float*) malloc(pixel_max*pixel_max*sizeof(float));
    float* img_data_GPU = (float*) malloc(NumBlocks*pixel_max*pixel_max*sizeof(float));
    // initalize the memory
  
    for( unsigned int i = 0; i < (size_x * size_y); ++i) 
    {
        kernel_CPU[i] =  (float)(rand()%N); 
    }
    
    for( unsigned int i = 0; i < 2*N; ++i) 
    {
        input_x_CPU[i] = (rand()%N) ;   
        input_y_CPU[i] = (rand()%N) ; 
    }
   
 // set texture parameters
  
    // allocate device memory
    int * input_x;
    int * input_y;
    float* img_data;
	cudaArray* kernel;
	float* kernel_data;
    CUDA_SAFE_CALL( cudaMallocArray(&kernel, &floatTex, pixel_max*10, pixel_max*10) );
    CUDA_SAFE_CALL( cudaMalloc( (void**) &input_x, 2*N*sizeof(int)));
    CUDA_SAFE_CALL( cudaMalloc( (void**) &input_y, 2*N*sizeof(int)));
    CUDA_SAFE_CALL( cudaMalloc( (void**) &img_data,NumBlocks*pixel_max*pixel_max*sizeof(float)));
    CUDA_SAFE_CALL( cudaMalloc( (void**) &kernel_data,mem_size));
    // copy host memory to device
      CUDA_SAFE_CALL( cudaMemcpyToArray(kernel, 0, 0, kernel_CPU, mem_size, cudaMemcpyHostToDevice) );
    texData.addressMode[0] = cudaAddressModeWrap;
    texData.addressMode[1] = cudaAddressModeWrap;
    texData.filterMode = cudaFilterModeLinear;
    texData.normalized = false;    
      
      CUDA_SAFE_CALL( cudaBindTextureToArray(texData, kernel,floatTex) );
    // setup execution parameters
    dim3 grid(NumBlocks,1,1);
    dim3 threads(BLOCK_DIM, BLOCK_DIM, 1);


    CUDA_SAFE_CALL( cudaMemcpy( input_x, input_x_CPU, 2*N*sizeof(int),
                                cudaMemcpyHostToDevice) );
    CUDA_SAFE_CALL( cudaMemcpy( input_y, input_y_CPU, 2*N*sizeof(int),
                                cudaMemcpyHostToDevice) );
    CUDA_SAFE_CALL( cudaMemcpy( kernel_data,kernel_CPU,  mem_size,
                              cudaMemcpyHostToDevice) );
       litho<<< grid, threads >>> ( input_x, input_y,img_data,kernel_data);
    
     CUDA_SAFE_CALL( cudaMemcpy(img_data_GPU ,img_data,NumBlocks*pixel_max*pixel_max*sizeof(float),
                                cudaMemcpyDeviceToHost) );

    // warmup so we don't time CUDA startup
       cutStartTimer(timer);
    for (int i = 0; i < numIterations; i++)
    {
    
    CUDA_SAFE_CALL( cudaMemcpy( input_x, input_x_CPU, 2*N*sizeof(int),
                                cudaMemcpyHostToDevice) );
    CUDA_SAFE_CALL( cudaMemcpy( input_y, input_y_CPU, 2*N*sizeof(int),
                                cudaMemcpyHostToDevice) );
  
    litho<<< grid, threads >>> ( input_x, input_y,img_data,kernel_data);
    

     CUDA_SAFE_CALL( cudaMemcpy(img_data_GPU ,img_data,NumBlocks*pixel_max*pixel_max*sizeof(float),
                          cudaMemcpyDeviceToHost) );
                          
      for( unsigned int j = 0; j < (pixel_max * pixel_max); ++j) 
        for(int s=1;s<NumBlocks;s++)
     {
		img_data_GPU[j]+=img_data_GPU[s*pixel_max * pixel_max+j];
	 }
    } 
    

    cutStopTimer(timer);
    float optimizedTime = cutGetTimerValue(timer);

    printf("Optimized litho average time in GPU: %3.3f ms\n\n", optimizedTime/(float)numIterations);

    // check if kernel execution generated and error
    CUT_CHECK_ERROR("Kernel execution failed");
  CUT_SAFE_CALL( cutDeleteTimer(timer));
    // copy result from device to    host
  //  cutStartTimer(timer);
  clock_t start,finish;
	start=clock();
      for (int i = 0; i < numIterations; i++)
        lithoGold( kernel_CPU,input_x_CPU, input_y_CPU, img_data_CPU);
    finish=clock();
//	cutStopTimer(timer);
	 double cpuTime = difftime(finish,start)/CLOCKS_PER_SEC*1000;

    printf("Optimized litho average time in CPU: %3.7lf ms\n\n", cpuTime/numIterations);
    // check result
   
/*	for(int j=0;j< pixel_max*pixel_max;j++)
		if(fabs(img_data_GPU[j]-img_data_CPU[j])>1e-4)
		 printf("j: %d cpu_data %g gpu_data %g",j,img_data_CPU[j],img_data_GPU[j]);
*/		 
}
