#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <utility>
#include <iostream>
#include "goldAerialImage.h"

using namespace std;

// /brief 
// Each corner is computed its relative position inside the LUT only once. Every
// pixel is updated continuously from the left to right in each row, and its corner
// coordinates is updated accordingly.
// NOTE. This version changes the image memory access sequence. One image is
// loaded and then updated by a set of involving corners. Not like the verion_1
// that is processed independently by each corner.
void goldPrintImage_continuous_2(Rectangle* polygonArray,
  INT4 rectNum,
  float* weightArray,
  DTYPE** tableArray,
  DTYPE* &goldImage,
  float &goldTime,
  Rectangle boundBox) 
{
  clock_t startTime = clock();

  int boundwidth = (boundBox.right - boundBox.left) / GRID_SIZE;
  int boundheight = (boundBox.top - boundBox.bottom) / GRID_SIZE;

  DTYPE * field_real = (DTYPE *) malloc(sizeof(DTYPE)*boundwidth*boundheight);
  DTYPE * field_imag = (DTYPE *) malloc(sizeof(DTYPE)*boundwidth*boundheight);

  Rectangle polygonImage;

  for (int i = 0; i < boundwidth * boundheight; i++){
    goldImage[i] = 0;
  }
 
  for (int weightIdx = 0, kernelIdx = 0; weightIdx < KERNEL_NUMBER; weightIdx++, kernelIdx += 2) {
    for (int i = 0; i < boundwidth * boundheight; i++) {
      field_real[i] = 0;
      field_imag[i] = 0;
    }

    // Create ONE halo-lookuptable that works for all rectangles. LUT is extended into
    // imageSize by adding halo to LUT. Halo is assigned boundary values.
    int haloWidth = boundwidth + TABLE_SIZE;
    int haloHeight = boundheight + TABLE_SIZE; 
    DTYPE* haloTable1 = (DTYPE*) malloc(sizeof(DTYPE) * haloWidth * haloHeight);
    DTYPE* haloTable2 = (DTYPE*) malloc(sizeof(DTYPE) * haloWidth * haloHeight);
    int h = min(TABLE_SIZE, haloHeight);
    int w = min(TABLE_SIZE, haloWidth);
    for (int y = 0; y < h; ++y) {
      memcpy(haloTable1+y*haloWidth, tableArray[kernelIdx]+y*TABLE_SIZE, sizeof(DTYPE)*w);
      memcpy(haloTable2+y*haloWidth, tableArray[kernelIdx+1]+y*TABLE_SIZE, sizeof(DTYPE)*w);
    }   
    // Set boundary values to halo
    for (int y = 0; y < h; ++y) {
      fill_n(haloTable1+y*haloWidth+w, haloWidth - w, tableArray[kernelIdx][y*TABLE_SIZE+TABLE_SIZE-1]);
      fill_n(haloTable2+y*haloWidth+w, haloWidth - w, tableArray[kernelIdx+1][y*TABLE_SIZE+TABLE_SIZE-1]);
    }   
    if (h != haloHeight) {
      DTYPE* upperBoundaryRow1 = (DTYPE*) malloc(sizeof(DTYPE)*haloWidth);
      DTYPE* upperBoundaryRow2 = (DTYPE*) malloc(sizeof(DTYPE)*haloWidth);
      int x = 0;
      for (; x < w; ++x) {
        upperBoundaryRow1[x] = tableArray[kernelIdx][(TABLE_SIZE-1)*TABLE_SIZE+x];
        upperBoundaryRow2[x] = tableArray[kernelIdx+1][(TABLE_SIZE-1)*TABLE_SIZE+x];
      }
      for (; x < haloWidth; ++x) {
        upperBoundaryRow1[x] = tableArray[kernelIdx][TABLE_SIZE*TABLE_SIZE-1];
        upperBoundaryRow2[x] = tableArray[kernelIdx+1][TABLE_SIZE*TABLE_SIZE-1];
      }
      for (int y = h; y < haloHeight; ++y) {
        memcpy(haloTable1+y*haloWidth, upperBoundaryRow1, sizeof(DTYPE)*haloWidth);
        memcpy(haloTable2+y*haloWidth, upperBoundaryRow2, sizeof(DTYPE)*haloWidth);
      }
      free(upperBoundaryRow1);
      free(upperBoundaryRow2);
    }

    for (int i = 0; i < rectNum; i++) {
      // Note that all polygons are inside the left and bottom boundary of the
      // aerial image, but some of them exceed the right and top boundary.
      polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom)/GRID_SIZE;
      polygonImage.top    = (polygonArray[i].top    - boundBox.bottom)/GRID_SIZE;
      polygonImage.left   = (polygonArray[i].left   - boundBox.left)  /GRID_SIZE;
      polygonImage.right  = (polygonArray[i].right  - boundBox.left)  /GRID_SIZE;
      // Ignore the polygon that is totally outside the aerial image
      if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
        continue;
      // The polygon's top/right boundary over some limit is meaningless, i.e.
      // LUT won't be used to check those pixels. The limit is as follows:
      // FIXME. NOT SURE....
      // (boundheight-1) (the right most pixel in the aerial image) +
      // TABLE_RADIUS + 1.
      polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
      polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

      pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
      corners[0] = make_pair(polygonImage.right, polygonImage.top);
      corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
      corners[2] = make_pair(polygonImage.left, polygonImage.top);
      corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

      // Suppose the coordinate of the corner is with their corner idx
      // Bottom-left corner: (x1-radius, y1-radius) -> (x1+radius, y1+radius)
      // Top-left corner: (x1-radius, y1-radius) -> (x2+radius, y2+radius)
      // Bottom-right corner: (x1-radius, y1-radius) -> (x3+radius, y3+radius)
      // Top-right corner: (x1-radius, y1-radius) -> (x0+radius, y0+radius)
      // While x1==x2 && y1==y3 && x0==x3 && y0==y2, only (x1, y1) and (x0, y0)
      // is enough
      Rectangle impactRegions[2]; impactRegions[1].bottom = impactRegions[0].bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
      impactRegions[1].left = impactRegions[0].left = max(polygonImage.left - KERNEL_RADIUS, 0);
      impactRegions[0].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
      impactRegions[0].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);
      impactRegions[1].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
      impactRegions[1].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);

      // The following codes are to create an individual halo-lookuptable for this rectangle
#if 0
      // Add halo to LUT
      int haloHeight = polygonImage.top - (impactRegion.bottom - TABLE_RADIUS);
      int haloWidth = polygonImage.right - (impactRegion.left - TABLE_RADIUS);
      // for tableArray[kernelIdx][TABLE_SIZE*TABLE_SIZE]
      float* haloTable1 = (float *) malloc(sizeof(float) * haloHeight * haloWidth);
      // for tableArray[kernelIdx+1][TABLE_SIZE*TABLE_SIZE]
      float* haloTable2 = (float *) malloc(sizeof(float) * haloHeight * haloWidth);

      // 1. (use either 1 or 2 to initialize haloTable)
      // Load chunks of cotinuous memory from tableArray to haloTable.
      int h = min(TABLE_SIZE, haloHeight);
      int w = min(TABLE_SIZE, haloWidth);
      for (int y = 0; y < h; ++y) {
        memcpy(haloTable1+y*haloWidth, tableArray[kernelIdx]+y*TABLE_SIZE, sizeof(float)*w);
        memcpy(haloTable2+y*haloWidth, tableArray[kernelIdx+1]+y*TABLE_SIZE, sizeof(float)*w);
      }   
      // Set boundary values to halo
      for (int y = 0; y < h; ++y) {
        fill_n(haloTable1+y*haloWidth+w, haloWidth - w, tableArray[kernelIdx][y*TABLE_SIZE+TABLE_SIZE-1]);
        fill_n(haloTable2+y*haloWidth+w, haloWidth - w, tableArray[kernelIdx+1][y*TABLE_SIZE+TABLE_SIZE-1]);
      }   
      if (h != haloHeight) {
        float* upperBoundaryRow1 = (float*) malloc(sizeof(float)*haloWidth);
        float* upperBoundaryRow2 = (float*) malloc(sizeof(float)*haloWidth);
        int x = 0;
        for (; x < w; ++x) {
          upperBoundaryRow1[x] = tableArray[kernelIdx][(TABLE_SIZE-1)*TABLE_SIZE+x];
          upperBoundaryRow2[x] = tableArray[kernelIdx+1][(TABLE_SIZE-1)*TABLE_SIZE+x];
        }
        for (; x < haloWidth; ++x) {
          upperBoundaryRow1[x] = tableArray[kernelIdx][TABLE_SIZE*TABLE_SIZE-1];
          upperBoundaryRow2[x] = tableArray[kernelIdx+1][TABLE_SIZE*TABLE_SIZE-1];
        }
        for (int y = h; y < haloHeight; ++y) {
          memcpy(haloTable1+y*haloWidth, upperBoundaryRow1, sizeof(float)*haloWidth);
          memcpy(haloTable2+y*haloWidth, upperBoundaryRow2, sizeof(float)*haloWidth);
        }
      }

#if 0
      // 2.
      // Load by index
      for (int y = 0, idx = 0; y < haloHeight; ++y)
        for (int x = 0; x < haloWidth; ++x) {
          if (x < TABLE_SIZE && y < TABLE_SIZE) {
            // FIXME. y*TABLE_SIZE+x should be precalculated.
            haloTable1[idx] = tableArray[kernelIdx][y*TABLE_SIZE+x];
            haloTable2[idx] = tableArray[kernelIdx+1][y*TABLE_SIZE+x];
          } else if (x >= TABLE_SIZE && y >= TABLE_SIZE) {
            // FIXME. TABLE_SIZE*TABLE_SIZE-1 should be precalculated.
            haloTable1[idx] = tableArray[kernelIdx][TABLE_SIZE*TABLE_SIZE-1];
            haloTable2[idx] = tableArray[kernelIdx+1][TABLE_SIZE*TABLE_SIZE-1];
          } else if (x >= TABLE_SIZE) {
            // FIXME. Should precalculate.
            haloTable1[idx] = tableArray[kernelIdx][y*TABLE_SIZE+(TABLE_SIZE-1)];
            haloTable2[idx] = tableArray[kernelIdx+1][y*TABLE_SIZE+(TABLE_SIZE-1)];
          } else {
            // FIXME. Should precalcuate.
            haloTable1[idx] = tableArray[kernelIdx][(TABLE_SIZE-1)*TABLE_SIZE+x];
            haloTable2[idx] = tableArray[kernelIdx+1][(TABLE_SIZE-1)*TABLE_SIZE+x];
          }
          idx++;
        }
#endif
#endif

#if 0
      {
        // impactRegions[1] (bottom-left corner): common region for all corners
        // impactRegions[2] (top-left corner): common region for top-left and top-right corner
        // impactRegions[3] (bottom-right corner): common region for bottom-right corner and top-right corner
        // impactRegions[0] (top-right corner)
        int rely[4], relx[4], tidx[4], start_tidx[4];
        for (int c = 0; c < 4; ++c) {
          rely[c] = corners[c].second - (impactRegions[0].bottom - TABLE_RADIUS) - 1; 
          relx[c] = corners[c].first - (impactRegions[0].left - TABLE_RADIUS) - 1;
          start_tidx[c] = tidx[c] = rely[c]*haloWidth+relx[c];
        }
        int start_fidx, fidx;
        start_fidx = fidx = impactRegions[0].bottom*boundwidth+impactRegions[0].left;
        for (int j = impactRegions[0].bottom; j < impactRegions[1].top; ++j) {
          int k;
          for (k = impactRegions[0].left; k < impactRegions[1].right; ++k) {
            fidx = j*boundwidth+k;
            for (int c = 0; c < 4; ++c) {
              tidx[c] = (rely[c]-(j-impactRegions[0].bottom))*haloWidth+(relx[c]-(k-impactRegions[0].left));
            }
            for (int c = 2; c < 4; ++c) {
              field_real[fidx] -= haloTable1[tidx[c]]; 
              //field_imag[fidx] -= haloTable1[tidx[c]--]; 
              field_imag[fidx] -= haloTable1[tidx[c]]; 
            }
            for (int c = 0; c < 2; ++c) {
              field_real[fidx] += haloTable1[tidx[c]]; 
              //field_imag[fidx] += haloTable1[tidx[c]--]; 
              field_imag[fidx] += haloTable1[tidx[c]]; 
            }
            //fidx++;
          }
          for (; k < impactRegions[3].right; ++k) {
            fidx = j*boundwidth+k;
            for (int c = 0; c < 4; ++c) {
              tidx[c] = (rely[c]-(j-impactRegions[0].bottom))*haloWidth+(relx[c]-(k-impactRegions[0].left));
            }
            field_real[fidx] -= haloTable1[tidx[3]]; 
            //field_imag[fidx] -= haloTable1[tidx[3]--]; 
            field_imag[fidx] -= haloTable1[tidx[3]]; 
            field_real[fidx] += haloTable1[tidx[0]]; 
            //field_imag[fidx] += haloTable1[tidx[0]--]; 
            field_imag[fidx] += haloTable1[tidx[0]]; 
            //fidx++;
          }
#if 0
          start_fidx += boundwidth;
          fidx = start_fidx;
          for (int c = 0; c < 4; ++c) {
            start_tidx[c] -= haloWidth;
            tidx[c] = start_tidx[c];
          }
#endif
        }
        for (int j = impactRegions[1].top; j < impactRegions[0].top; ++j) {
          int k;
          for (k = impactRegions[0].left; k < impactRegions[2].right; ++k) {
            fidx = j*boundwidth+k;
            for (int c = 0; c < 4; ++c) {
              tidx[c] = (rely[c]-(j-impactRegions[0].bottom))*haloWidth+(relx[c]-(k-impactRegions[0].left));
            }
            field_real[fidx] -= haloTable1[tidx[2]]; 
            //field_imag[fidx] -= haloTable1[tidx[2]--]; 
            field_imag[fidx] -= haloTable1[tidx[2]]; 
            field_real[fidx] += haloTable1[tidx[0]]; 
            //field_imag[fidx] += haloTable1[tidx[0]--]; 
            field_imag[fidx] += haloTable1[tidx[0]]; 
            //fidx++;
          }
          for (; k < impactRegions[0].right; ++k) {
            fidx = j*boundwidth+k;
            for (int c = 0; c < 4; ++c) {
              tidx[c] = (rely[c]-(j-impactRegions[0].bottom))*haloWidth+(relx[c]-(k-impactRegions[0].left));
            }
            field_real[fidx] += haloTable1[tidx[0]]; 
            //field_imag[fidx] += haloTable1[tidx[0]--]; 
            field_imag[fidx] += haloTable1[tidx[0]]; 
            //fidx++;
          }
#if 0
          start_fidx += boundwidth;
          fidx = start_fidx;
          start_tidx[0] -= haloWidth;
          tidx[0] = start_tidx[0];
          start_tidx[2] -= haloWidth;
          tidx[2] = start_tidx[2];
#endif
        }
      }
#endif
      //DEBUG
      {
        int j = impactRegions[0].bottom;
        int k = impactRegions[0].left;
        int rely[4], relx[4];
        for (int c = 0; c < 4; ++c) {
          rely[c] = corners[c].second - (impactRegions[0].bottom - TABLE_RADIUS) - 1; 
          relx[c] = corners[c].first - (impactRegions[0].left - TABLE_RADIUS) - 1;
        }
        int xSteps1 = impactRegions[1].right - impactRegions[0].left;
        int xSteps2 = impactRegions[0].right - impactRegions[0].left;
        int ySteps1 = impactRegions[1].top - impactRegions[0].bottom;
        int ySteps2 = impactRegions[0].top - impactRegions[0].bottom;
        for (int ys = 0; ys < ySteps1; ++ys) 
          for (int xs = 0; xs < xSteps1; ++xs) {
            for (int c = 2; c < 4; ++c) {
              field_real[(j+ys) * boundwidth + (k+xs)] -= haloTable1[(rely[c] - ys) * haloWidth + (relx[c] - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] -= haloTable2[(rely[c] - ys) * haloWidth + (relx[c] - xs)];
            }
            for (int c = 0; c < 2; ++c) {
              field_real[(j+ys) * boundwidth + (k+xs)] += haloTable1[(rely[c] - ys) * haloWidth + (relx[c] - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] += haloTable2[(rely[c] - ys) * haloWidth + (relx[c] - xs)];
            }
          }
        for (int ys = ySteps1; ys < ySteps2; ++ys)
          for (int xs = 0; xs < xSteps1; ++xs) {
              field_real[(j+ys) * boundwidth + (k+xs)] -= haloTable1[(rely[2] - ys) * haloWidth + (relx[2] - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] -= haloTable2[(rely[2] - ys) * haloWidth + (relx[2] - xs)];
              field_real[(j+ys) * boundwidth + (k+xs)] += haloTable1[(rely[0] - ys) * haloWidth + (relx[0] - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] += haloTable2[(rely[0] - ys) * haloWidth + (relx[0] - xs)];
          }
        for (int ys = 0; ys < ySteps1; ++ys)
          for (int xs = xSteps1; xs < xSteps2; ++xs) {
              field_real[(j+ys) * boundwidth + (k+xs)] -= haloTable1[(rely[3] - ys) * haloWidth + (relx[3] - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] -= haloTable2[(rely[3] - ys) * haloWidth + (relx[3] - xs)];
              field_real[(j+ys) * boundwidth + (k+xs)] += haloTable1[(rely[0] - ys) * haloWidth + (relx[0] - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] += haloTable2[(rely[0] - ys) * haloWidth + (relx[0] - xs)];
          }
        for (int ys = ySteps1; ys < ySteps2; ++ys) 
          for (int xs = xSteps1; xs < xSteps2; ++xs) {
              field_real[(j+ys) * boundwidth + (k+xs)] += haloTable1[(rely[0] - ys) * haloWidth + (relx[0] - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] += haloTable2[(rely[0] - ys) * haloWidth + (relx[0] - xs)];
          }
      }

#if 0
      free(haloTable1);
      free(haloTable2);
#endif
    }
    free(haloTable1);
    free(haloTable2);

    for (int i=0;i<boundwidth*boundheight;i++){
      goldImage[i]+=((DTYPE)weightArray[weightIdx])*(field_real[i]*field_real[i]+field_imag[i]*field_imag[i]);
    }
  }
  free(field_real);
  free(field_imag);

  goldTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
}


