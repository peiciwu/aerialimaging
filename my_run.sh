#!/bin/bash

index=14
postfix="_ac33"

echo "build/aerial data/small_1.txt $index > report/small_1$postfix.txt"
build/aerial data/small_1.txt $index > report/small_1$postfix.txt
echo "build/aerial data/small_2.txt $index > report/small_2$postfix.txt"
build/aerial data/small_2.txt $index > report/small_2$postfix.txt
echo "build/aerial data/small_3.txt $index > report/small_3$postfix.txt"
build/aerial data/small_3.txt $index > report/small_3$postfix.txt
echo "build/aerial data/small_4.txt $index > report/small_4$postfix.txt"
build/aerial data/small_4.txt $index > report/small_4$postfix.txt

echo "build/aerial data/intermedian_1.txt $index > report/intermedian_1$postfix.txt"
build/aerial data/intermedian_1.txt $index > report/intermedian_1$postfix.txt
echo "build/aerial data/intermedian_2.txt $index > report/intermedian_2$postfix.txt"
build/aerial data/intermedian_2.txt $index > report/intermedian_2$postfix.txt
echo "build/aerial data/intermedian_3.txt $index > report/intermedian_3$postfix.txt"
build/aerial data/intermedian_3.txt $index > report/intermedian_3$postfix.txt
echo "build/aerial data/intermedian_4.txt $index > report/intermedian_4$postfix.txt"
build/aerial data/intermedian_4.txt $index > report/intermedian_4$postfix.txt

echo "build/aerial data/input_1.txt $index > report/input_1$postfix.txt"
build/aerial data/input_1.txt $index > report/input_1$postfix.txt
echo "build/aerial data/input_2.txt $index > report/input_2$postfix.txt"
build/aerial data/input_2.txt $index > report/input_2$postfix.txt
echo "build/aerial data/input_3.txt $index > report/input_3$postfix.txt"
build/aerial data/input_3.txt $index > report/input_3$postfix.txt
echo "build/aerial data/input_4.txt $index > report/input_4$postfix.txt"
build/aerial data/input_4.txt $index > report/input_4$postfix.txt
