#include "cudaCommon_Tan.cuh"

#include <ctime>
#include <cstdio>
#include <iostream>
using namespace std;

#define BLOCK_NUMBER_LUT 30
#define BLOCK_SIZE_COMBINE 256

__global__ void kernel_combine_image_1(float * d_image_r, float * d_image_i, float * d_image, INT4 image_size) {
  int pix_idx = threadIdx.x + blockIdx.x * blockDim.x;
  if (pix_idx >= image_size) {
    return;
  }
  float real_part = d_image_r[pix_idx];
  float imaginary_part = d_image_i[pix_idx];

  float intensity = weight * (real_part * real_part + imaginary_part * imaginary_part);

  d_image[pix_idx] += intensity;
}

__global__ void kernel_compute_image_from_LUT_1(float* d_image_r, float* d_image_i, INT4 image_width, INT4 image_height, float* d_LUT_r, float* d_LUT_i, Rectangle * d_rects, INT4 rectNum, Rectangle boundBox) {
  INT4 rect_id = blockIdx.x * blockDim.x + threadIdx.x;
  if (rect_id >= rectNum) {
    return;
  }

  Rectangle rect;
  fetch_rect(rect, boundBox, rect_id, d_rects);

  Rectangle range;
  compute_range(range, rect, image_width, image_height);

  for (INT4 r = range.bottom; r < range.top; ++r) {
    for (INT4 c = range.left; c < range.right; ++c) {
      accumulate_one_pix(r, c, rect, image_width, d_image_r, d_image_i, d_LUT_r, d_LUT_i);
    }
  }
}

void cudaPrintImage_LUT_1(Rectangle * polygonArray,
		    INT4 rectNum,
		    float * weightArray,
		    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
		    Rectangle & boundBox,
		    float * cudaImage,
		    float & cudaTime) {
  /* timing */
  double runtime = .0;
  
  /* allocate device space */
  Rectangle * d_rects; // all rectangles
  float * d_image_i;  // real and imaginary part of the image
  float * d_image_r;
  float * d_image; // output image on device
  float * d_LUT_r; // lookup table, 
  float * d_LUT_i;

  int image_height = (boundBox.top - boundBox.bottom) / GRID_SIZE;
  int image_width = (boundBox.right - boundBox.left) / GRID_SIZE;
  int image_size = image_height * image_width;

  cout << "allocating device memory..." << endl;  

  cudaMalloc((void**)& d_rects, rectNum * sizeof(Rectangle));
  cudaMalloc((void**)& d_image_i, image_height * image_width * sizeof(float));
  cudaMalloc((void**)& d_image_r, image_height * image_width * sizeof(float));
  cudaMalloc((void**)& d_image, image_height * image_width * sizeof(float));
  cudaMalloc((void**)& d_LUT_r, TABLE_SIZE * TABLE_SIZE * sizeof(float));
  cudaMalloc((void**)& d_LUT_i, TABLE_SIZE * TABLE_SIZE * sizeof(float));

  // set up grid dim and block dim
  dim3 dimGrid_LUT(BLOCK_NUMBER_LUT);
  INT4 thread_number = rectNum / BLOCK_NUMBER_LUT + ((rectNum % BLOCK_NUMBER_LUT == 0) ? 0 : 1);
  dim3 dimBlock_LUT(thread_number);
  dim3 dimBlock_combine(BLOCK_SIZE_COMBINE);
  dim3 dimGrid_combine(image_size / BLOCK_SIZE_COMBINE + ((image_size % BLOCK_SIZE_COMBINE == 0) ? 0 : 1));

  // copy rectangles 
  cudaMemcpy(d_rects, polygonArray, rectNum * sizeof(Rectangle), cudaMemcpyHostToDevice);

  cudaMemset(d_image, 0, image_height * image_width * sizeof(float));

  cout << "start kernel..." << endl;  
  // run through all the kernels

  for (int i = 0; i < KERNEL_NUMBER; ++i) {
    // copy data
    cudaMemcpy(d_LUT_r, tableArray[i * 2 + 1], TABLE_SIZE * TABLE_SIZE * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_LUT_i, tableArray[i * 2], TABLE_SIZE * TABLE_SIZE * sizeof(float), cudaMemcpyHostToDevice);

    // timing
    cudaThreadSynchronize();
    clock_t start_time = clock();

    //copy const mem
    cudaMemcpyToSymbol(weight, &(weightArray[i]), sizeof(float), 0, cudaMemcpyHostToDevice);

    // initialize image
    cudaMemset(d_image_i, 0, image_height * image_width * sizeof(float));
    cudaMemset(d_image_r, 0, image_height * image_width * sizeof(float));

    // call kernel
    kernel_compute_image_from_LUT_1<<<dimGrid_LUT, dimBlock_LUT>>>(d_image_r, d_image_i, image_width, image_height, d_LUT_r, d_LUT_i, d_rects, rectNum, boundBox);

    kernel_combine_image_1<<<dimGrid_combine, dimBlock_combine>>>(d_image_r, d_image_i, d_image, image_size);
    //    combine_image(dimGrid_combine, dimBlock_combine, d_image_r, d_image_i, d_image, image_size);

    // timing
    cudaThreadSynchronize();
    runtime += clock() - start_time;
  }

  // copy result image back
  cudaMemcpy(cudaImage, d_image, image_height * image_width * sizeof(float), cudaMemcpyDeviceToHost);

  cudaFree(d_rects);
  cudaFree(d_image_i);
  cudaFree(d_image_r);
  cudaFree(d_image);
  cudaFree(d_LUT_r);
  cudaFree(d_LUT_i);
  
  // in the end report the run time by modifying the value of cudaTime;
  cudaTime = runtime / (double) CLOCKS_PER_SEC;
}
