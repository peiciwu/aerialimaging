/* Matrix transpose with Cuda 
 * Host code.

 * This example transposes arbitrary-size matrices.  It compares a naive
 * transpose kernel that suffers from non-coalesced writes, to an optimized
 * transpose with fully coalesced memory access and no bank conflicts.  On 
 * a G80 GPU, the optimized transpose can be more than 10x faster for large
 * matrices.
 */

// includes, system
#include <stdlib.h>
#include <cutil.h>
#include <iostream>

#include "cudaAerialImage_PIXEL.h"

#define NumBlocks 12
#define BLOCK_DIM 16
#define pixel_max 40
#define pixel_max_2 1600
#define IMUL(a, b) __mul24((a), (b))

#define CUDA_KERNEL_XSIZE 8
#define CUDA_KERNEL_YSIZE 16
#define CUDA_KERNEL_NUMBER 128

#define __MAX__(a, b) (((a)>(b))?(a):(b))
#define __MIN__(a, b) (((a)>(b))?(b):(a))

using namespace std;

////////////////////////////////////////////////////////////////////////////////
// Here is the novel run program
////////////////////////////////////////////////////////////////////////////////

__global__ void cudaPrintImage_kernel_UCLA( const int* input_x,
					    const int* input_y, 
					    float* img_data,
					    int left,
					    int right,
					    int vertexNum,
					    int kernelIndex);

texture<float, 3, cudaReadModeElementType> texTable;

void cudaPrintImage_UCLA(Rectangle* polygonArray,
	INT4 rectNum,
	float * weightArray,
	float tableArray[TABLE_NUMBER][TABLE_SIZE*TABLE_SIZE],
	float * &cudaImage,
	float &cudaTime,
	Rectangle boundBox)
{
    clock_t start = clock();

    // input rectangle set up
    // transfer the polygon to vertex
    INT4 * input_x_CPU=(INT4 *) malloc(4*rectNum*sizeof(INT4));
    INT4 * input_y_CPU=(INT4 *) malloc(4*rectNum*sizeof(INT4));
    int j=0;
    for (int i=0;i< rectNum;i++){
	input_x_CPU[j]   = (polygonArray[i].left   - boundBox.left  ) / GRID_SIZE - 1;
	input_y_CPU[j++] = (polygonArray[i].bottom - boundBox.bottom) / GRID_SIZE - 1;
	input_x_CPU[j]   = (polygonArray[i].right  - boundBox.left  ) / GRID_SIZE - 1;
	input_y_CPU[j++] = (polygonArray[i].bottom - boundBox.bottom) / GRID_SIZE - 1;
	input_x_CPU[j]   = (polygonArray[i].right  - boundBox.left  ) / GRID_SIZE - 1;
	input_y_CPU[j++] = (polygonArray[i].top    - boundBox.bottom) / GRID_SIZE - 1;
	input_x_CPU[j]   = (polygonArray[i].left   - boundBox.left  ) / GRID_SIZE - 1;
	input_y_CPU[j++] = (polygonArray[i].top    - boundBox.bottom) / GRID_SIZE - 1;
    }
//    cout << "left" << polygonArray[0].left;
//    cout << " right" << polygonArray[0].right;
//    cout << " bottom" << polygonArray[0].bottom;
//    cout << " top" << polygonArray[0].top<<endl;

//    for (int i=0; i< 4* rectNum; i++){
//        cout<<input_x_CPU[i]<<" ";
//    }
//    cout<<endl;
//    for (int i=0; i< 4* rectNum; i++){
//	cout<<input_y_CPU[i]<<" ";
//    }
//    cout<<endl;
    INT4 imagewidth = (boundBox.right-boundBox.left) / GRID_SIZE;
    INT4 imageheight = (boundBox.top-boundBox.bottom) / GRID_SIZE;
//    cout << "width " << imagewidth<<endl;
//    cout << "height "<< imageheight<<endl;
    for (int i=0; i< imagewidth*imageheight; i++){
	cudaImage[i] = 0.0;
    }
    int * input_x;
    int * input_y;
    CUDA_SAFE_CALL(cudaMalloc( (void**) &input_x, 4*rectNum*sizeof(int)));
    CUDA_SAFE_CALL(cudaMalloc( (void**) &input_y, 4*rectNum*sizeof(int)));
    CUDA_SAFE_CALL(cudaMemcpy( input_x, input_x_CPU, 4*rectNum*sizeof(int), cudaMemcpyHostToDevice) );
    CUDA_SAFE_CALL(cudaMemcpy( input_y, input_y_CPU, 4*rectNum*sizeof(int), cudaMemcpyHostToDevice) );
 
    // set up the kernel
    float *table = (float*)malloc(sizeof(float)*TABLE_NUMBER*TABLE_SIZE*TABLE_SIZE);
    int k=0;
    for (int i=0;i<TABLE_NUMBER;++i){
        memcpy(&table[k], tableArray[i],sizeof(float)/sizeof(char)*TABLE_SIZE*TABLE_SIZE);
	k += TABLE_SIZE*TABLE_SIZE;
    }
    cudaArray* cuArray=NULL;
    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float>();
    cudaExtent ext = make_cudaExtent(TABLE_SIZE, TABLE_SIZE, TABLE_NUMBER);
    cudaMalloc3DArray(&cuArray, &channelDesc, ext);

    cudaMemcpy3DParms copyParams = {0};
    copyParams.extent = make_cudaExtent(TABLE_SIZE, TABLE_SIZE, TABLE_NUMBER);
    copyParams.kind = cudaMemcpyHostToDevice;
    copyParams.dstArray = cuArray;
    copyParams.srcPtr = make_cudaPitchedPtr((void*)&table[0], ext.width*sizeof(float), ext.width, ext.height);
    cudaMemcpy3D(&copyParams);

    texTable.addressMode[0] = cudaAddressModeClamp;
    texTable.addressMode[1] = cudaAddressModeClamp;
//    texTable.addressMode[2] = cudaAddressModeClamp;
//    texTable.filterMode = cudaFilterModeLinear;
    texTable.normalized = false;

    cudaBindTextureToArray(texTable ,cuArray, channelDesc);

    // output image setup
    float *img_data;
    float *img_data_temp;

    CUDA_SAFE_CALL(cudaMalloc((void**)&img_data,NumBlocks*sizeof(float)*pixel_max*pixel_max));
    img_data_temp = (float *)malloc(NumBlocks*pixel_max*pixel_max*sizeof(float));
//    CUDA_SAFE_CALL(cudaMalloc((void**)&img_data,NumBlocks*sizeof(float)*imagewidth*imageheight));
//    img_data_temp = (float *)malloc(NumBlocks*imagewidth*imageheight*sizeof(float));

    for (int i=0; i<TABLE_NUMBER; i++){
//	cudaMemset(img_data, 0, sizeof(float)*imagewidth*imageheight);

	// setup execution parameters

	for (int bottom = 0; bottom < imageheight; bottom += pixel_max){
	    for (int left = 0; left < imagewidth; left += pixel_max){
		dim3 grid(NumBlocks,1,1);
	        dim3 threads(BLOCK_DIM, BLOCK_DIM, 1);
//	        cout << "new kernel" << endl;
//		cout << "bottom " << bottom << " left " << left << endl;
		cudaPrintImage_kernel_UCLA<<<grid, threads>>>(input_x, input_y, img_data, left, bottom, rectNum*4, i);

	        cudaThreadSynchronize();

		cudaMemcpy(img_data_temp,img_data, NumBlocks*pixel_max*pixel_max*sizeof(float), cudaMemcpyDeviceToHost);

		for( int j = 0; j < (pixel_max * pixel_max); ++j){
		    for(int s=1;s<NumBlocks;s++){
//			cout<< img_data_temp[s*pixel_max*pixel_max+j]<<endl;
		        img_data_temp[j]+=img_data_temp[s*pixel_max*pixel_max+j];
		    }
		}

		for (int x = 0; x < pixel_max; ++x){
		    for (int y = 0; y < pixel_max; ++y){
			if (((x+left) < imagewidth) && ((y + bottom) < imageheight)){
			    cudaImage[(y+bottom) * imagewidth+x+left]+= img_data_temp[y*pixel_max+x]*img_data_temp[y*pixel_max+x]*weightArray[i/2];
			}
		    }
		}
	    }
	}
//	for (int j = 0; j < (pixel_max*pixel_max); ++j){
//	    cudaImage[j] += img_data_temp[j]*img_data_temp[j]*weightArray[i];
//	}
	
    }
    cudaFreeArray(cuArray);
    cudaFree(img_data);
    cudaFree(input_x);
    cudaFree(input_y);
    free(table);
    free(input_x_CPU);
    free(input_y_CPU);

    // in the end report the run time by modifying the value of cudaTime;
    cudaTime = (clock()-start)/(double)CLOCKS_PER_SEC;
}

__global__ void cudaPrintImage_kernel_UCLA( const int* input_x,
					    const int* input_y, 
					    float* img_data,
					    int left,
					    int bottom,
					    int vertexNum,
					    int kernelIndex)
{
    __shared__ float img_data_shared[pixel_max_2];

    for( unsigned int x=threadIdx.x;x<pixel_max;x+=blockDim.x)
	for( unsigned int y=threadIdx.y;y<pixel_max;y+=blockDim.y)
	    img_data_shared[y*pixel_max+x] = 0.0;
//	    img_data_shared[y*pixel_max+x]=1.0f/gridDim.x;

//    const unsigned int const_x1=IMUL(imagewidth, GRID_SIZE);
//    const unsigned int const_x2=IMUL(const_x1, 2);
//    const unsigned int const_y1=IMUL(imageheight, GRID_SIZE);
//    const unsigned int const_y2=IMUL(const_y1, 2);

    __syncthreads();
    for( unsigned int i = blockIdx.x; i < vertexNum; i += gridDim.x){
	int dx=input_x[i];
	int dy=input_y[i];
	for( unsigned int x=(threadIdx.x);x<pixel_max; x += (blockDim.x)){
	    for( unsigned int y=(threadIdx.y);y<pixel_max;y+=(blockDim.y)){
		int dx_diff = dx - x - left + TABLE_RADIUS;
		int dy_diff = dy - y - bottom + TABLE_RADIUS;
//		if(dx_diff<0) dx_diff = -1;
		if(dx_diff>=TABLE_SIZE) dx_diff = TABLE_SIZE-1;

//		if(dy_diff<0) dy_diff = -1;
		if(dy_diff>=TABLE_SIZE) dy_diff = TABLE_SIZE-1;

		if (dx_diff > -1 && dy_diff > -1)
		    if ((i%2)==0)
		        img_data_shared[y*pixel_max+x]+=tex3D(texTable,dx_diff,dy_diff,kernelIndex);
		    else
		        img_data_shared[y*pixel_max+x]-=tex3D(texTable,dx_diff,dy_diff,kernelIndex);
	    }
	}
	__syncthreads();
    }


    for( unsigned int x=threadIdx.x;x<pixel_max;x+=blockDim.x)
	for( unsigned int y=threadIdx.y;y<pixel_max;y+=blockDim.y)
	    img_data[blockIdx.x*(pixel_max*pixel_max)+y*pixel_max+x]=img_data_shared[y*pixel_max+x];
    __syncthreads();
}




