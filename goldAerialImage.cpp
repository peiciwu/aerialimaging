#include "goldAerialImage.h"
#include <time.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

bool overlapping(Rectangle& area, int x, int y, Rectangle polygonImage);

void goldPrintImage(Rectangle* polygonArray,
	INT4 rectNum,
	float* weightArray,
	double** tableArray,
	double* &goldImage,
	float &goldTime,
	Rectangle boundBox)
{
    // your code starts here

    clock_t startTime = clock();

    int boundwidth = (boundBox.right-boundBox.left) / GRID_SIZE;
    int boundheight = (boundBox.top-boundBox.bottom) / GRID_SIZE;

    double * field_real = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag = (double *) malloc(sizeof(double)*boundwidth*boundheight);

    Rectangle polygonImage;
    Rectangle overlapRegion;


    for (int iter = 0; iter < ITER_NUM; ++iter) {
    for (int i=0;i<boundwidth*boundheight;i++){
	goldImage[i] = 0;
    }

    for (int weightIdx=0, kernelIdx=0; weightIdx<KERNEL_NUMBER; weightIdx++, kernelIdx+=2){
	for (int i=0;i<boundwidth*boundheight;i++){
	    field_real[i] = 0;
	    field_imag[i] = 0;
	}

	for (int i=0;i < rectNum;i++){
	    polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom)/GRID_SIZE;
	    polygonImage.top    = (polygonArray[i].top    - boundBox.bottom)/GRID_SIZE;
	    polygonImage.left   = (polygonArray[i].left   - boundBox.left)  /GRID_SIZE;
	    polygonImage.right  = (polygonArray[i].right  - boundBox.left)  /GRID_SIZE;

	    // for the topright corner
	    for (int j = max(polygonImage.bottom-KERNEL_RADIUS, 0); j < min(polygonImage.top+KERNEL_RADIUS, boundheight);++j)
		for (int k = max(polygonImage.left-KERNEL_RADIUS,0); k < min(polygonImage.right+KERNEL_RADIUS, boundwidth);++k) 
                {
		    //if(overlapping(overlapRegion, k, j, polygonImage)){
                    {
                        overlapping(overlapRegion, k, j, polygonImage);
			field_real[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
			field_imag[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
			if (overlapRegion.left!=-1){
			    field_real[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
			    field_imag[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
			}
			if (overlapRegion.bottom!=-1){
			    field_real[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
			    field_imag[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
			}
			if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
			    field_real[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
			    field_imag[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
			}
		    }
                }
	}

	for (int i=0;i<boundwidth*boundheight;i++){
	    goldImage[i]+=((double)weightArray[weightIdx])*(field_real[i]*field_real[i]+field_imag[i]*field_imag[i]);
	}
    }
    }
    free(field_real);
    free(field_imag);

    goldTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
}

bool overlapping(Rectangle& area, int x, int y, Rectangle polygonImage)
{
    int tableTop, tableBottom, tableLeft, tableRight;
    tableTop    = y+TABLE_RADIUS+1;
    tableBottom = y-TABLE_RADIUS;
    tableLeft   = x-TABLE_RADIUS;
    tableRight  = x+TABLE_RADIUS+1;
    
#if 0
    // FIXME. This condition is NOP.
    if (polygonImage.top <= tableBottom ||		// modified here
	    polygonImage.bottom >= tableTop ||
	    polygonImage.left >= tableRight ||
	    polygonImage.right <= tableLeft)		// modified here
	return false;
#endif

    area.top = min(tableTop, polygonImage.top)-tableBottom-1;     // modified here
    area.bottom = max(tableBottom, polygonImage.bottom)-tableBottom-1;    // modified here
    area.left = max(tableLeft, polygonImage.left)-tableLeft-1;	    // modified here
    area.right = min(tableRight, polygonImage.right)-tableLeft-1;	// modified here

    return true;
}



