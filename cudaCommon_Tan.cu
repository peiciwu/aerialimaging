#include "cudaCommon_Tan.cuh"
#include <iostream>
using namespace std;

__global__ void kernel_combine_image(float * d_image_r, float * d_image_i, float * d_image, INT4 image_size) {
  int pix_idx = threadIdx.x + blockIdx.x * blockDim.x;
  if (pix_idx >= image_size) {
    return;
  }
  float real_part = d_image_r[pix_idx];
  float imaginary_part = d_image_i[pix_idx];

  float intensity = weight * (real_part * real_part + imaginary_part * imaginary_part);

  d_image[pix_idx] += intensity;
}

void combine_image(const dim3 & dimGrid, const dim3 & dimBlock, float * d_image_r, float * d_image_i, float * d_image, INT4 image_size) {
  cout << "grid_size = " << dimGrid.x << ", block_size = " << dimBlock.x << endl;
    kernel_combine_image<<<dimGrid, dimBlock>>>(d_image_r, d_image_i, d_image, image_size);
}
