// This file is made by Hongbo Zhang, April-May.
// The whole version is used for pixel-based approach compared to rectangle-based approach. We need to 
// consider the comparison between the over-all table-lookup method with fft2 method in a large scale.
// Anyway, we can not cover everything here. The whole project will generate a reasonable result here.


#include "cudaAerialImage_PIXEL.h"
#include <stdio.h>

// This is a naive version where shared memory is used for the rectangle store and every pixel will read
// all polygons. Each block corresponding to a tile and each thread will corresponding to a pixel and
// each block reads all rect once

#define CUDA_KERNEL_XSIZE 8
#define CUDA_KERNEL_YSIZE 4
#define CUDA_KERNEL_NUMBER 32

#define __MAX__(a, b) (((a)>(b))?(a):(b))
#define __MIN__(a, b) (((a)>(b))?(b):(a))


// without constant memory use
__global__ void cudaPrintImage_kernel_PIXEL_1(Rectangle*, 
					      INT4,
					      float*,
					      INT4,
					      INT4,
					      float**,
					      float*,
					      Rectangle);

// with oncstant memory use
__global__ void cudaPrintImage_kernel_PIXEL_2(Rectangle*, 
					      INT4,
					      float*,
					      INT4,
					      INT4,
					      Rectangle);

__constant__ float * cudaTableArray[TABLE_NUMBER];
__constant__ float cudaWeightArray[KERNEL_NUMBER];



void cudaPrintImage_PIXEL_1(Rectangle* polygonArray,
	INT4 rectNum,
	float* weightArray,
	float tableArray[TABLE_NUMBER][TABLE_SIZE*TABLE_SIZE],
	float* &cudaImage,
	float &cudaTime,
	Rectangle boundBox)
{
//    float abs[2]={17.0,18.0};
    // your code starts here
    clock_t start = clock();
    float *image;
    Rectangle *cudaPolygonArray;
    INT4 imagewidth = (boundBox.right-boundBox.left) / GRID_SIZE;
    INT4 imageheight = (boundBox.top-boundBox.bottom) / GRID_SIZE;
    int tilewidth = imagewidth/CUDA_KERNEL_XSIZE+!!(imagewidth%CUDA_KERNEL_XSIZE);
    int tileheight = imageheight/CUDA_KERNEL_YSIZE+!!(imagewidth%CUDA_KERNEL_YSIZE);

    cudaMalloc((void**)&image, sizeof(float)*imagewidth*imageheight);
    cudaMalloc((void**)&cudaPolygonArray, sizeof(Rectangle)*rectNum);
    cudaMemcpy(cudaPolygonArray, polygonArray, sizeof(Rectangle)*rectNum, cudaMemcpyHostToDevice);
    cudaMemset(image, 0, sizeof(float)*imagewidth*imageheight);

    float * tablearray_temp [TABLE_NUMBER];
    float ** cudaTableArray;
    float * cudaWeightArray;

    for (int i=0;i<TABLE_NUMBER;++i){
    // debug
//	tablearray_temp[i]=NULL;
	cudaMalloc((void **)& (tablearray_temp[i]), sizeof(float)*TABLE_SIZE*TABLE_SIZE);
	cudaMemcpy(tablearray_temp[i], tableArray[i], sizeof(float)*TABLE_SIZE*TABLE_SIZE, cudaMemcpyHostToDevice);
    }
    cudaMalloc((void**)&cudaTableArray, sizeof(float*)*TABLE_NUMBER);
    cudaMemcpy(cudaTableArray, tablearray_temp, sizeof(float*)*TABLE_NUMBER, cudaMemcpyHostToDevice);

    cudaMalloc((void**)&cudaWeightArray, sizeof(float)*KERNEL_NUMBER);
    cudaMemcpy(cudaWeightArray, weightArray, sizeof(float)*KERNEL_NUMBER, cudaMemcpyHostToDevice);

//    cudaMemcpyToSymbol("cudaTableArray", tablearray_temp, sizeof(float*)*TABLE_NUMBER, 0, cudaMemcpyHostToDevice);
//    cudaMemcpyToSymbol("cudaWeightArray", weightArray, sizeof(float)*KERNEL_NUMBER, 0, cudaMemcpyHostToDevice);

    dim3 GridDim(tilewidth, tileheight);
    dim3 BlockDim(CUDA_KERNEL_XSIZE, CUDA_KERNEL_YSIZE);
    cudaPrintImage_kernel_PIXEL_1<<<GridDim, BlockDim>>>(cudaPolygonArray,
							 rectNum,
							 image,
							 imagewidth,
							 imageheight,
							 cudaTableArray,
							 cudaWeightArray,
							 boundBox);
    cudaThreadSynchronize();

    cudaMemcpy(cudaImage, image, sizeof(float)*imagewidth*imageheight, cudaMemcpyDeviceToHost);

    for (int i=0;i<TABLE_NUMBER;++i){
	cudaFree(tablearray_temp[i]);
    }
    cudaFree(image);
    cudaFree(cudaPolygonArray);
    cudaFree(cudaTableArray);

    // in the end report the run time by modifying the value of cudaTime;
    cudaTime = (clock()-start)/(double)CLOCKS_PER_SEC;
}

__global__ void cudaPrintImage_kernel_PIXEL_1(Rectangle* polygon,
                                              INT4 rectNum,
					      float* image,
					      INT4 imagewidth,
					      INT4 imageheight,
					      float**cudaTableArray,
					      float* cudaWeightArray,
					      Rectangle boundBox)
{
    __shared__ Rectangle rectArray[CUDA_KERNEL_NUMBER];

    INT4 x = blockIdx.x*CUDA_KERNEL_XSIZE + threadIdx.x;
    INT4 y = blockIdx.y*CUDA_KERNEL_YSIZE + threadIdx.y;
    INT4 tid = threadIdx.y*CUDA_KERNEL_XSIZE + threadIdx.x;
    __shared__ INT4 task;
    Rectangle area;
    __shared__ float * table[TABLE_NUMBER];
    float field[TABLE_NUMBER];
    float pixelimage = 0;

    for (int k=0;k<TABLE_NUMBER;++k)
        field[k] = 0;

    if (tid<TABLE_NUMBER)
        table[tid] = cudaTableArray[tid];

    for (int i = 0; i < rectNum; i += CUDA_KERNEL_NUMBER){
        __syncthreads();
	if (tid==0)
	    task = (i+CUDA_KERNEL_NUMBER<=rectNum)?CUDA_KERNEL_NUMBER:(rectNum-i);
	__syncthreads();

	if (tid<task){
	    area = polygon[tid+i];
	    rectArray[tid].bottom = (area.bottom - boundBox.bottom)/GRID_SIZE;
	    rectArray[tid].top    = (area.top    - boundBox.bottom)/GRID_SIZE;
	    rectArray[tid].left   = (area.left   - boundBox.left)/GRID_SIZE;
	    rectArray[tid].right  = (area.right  - boundBox.left)/GRID_SIZE;
	}
	__syncthreads();

	if (y >= imageheight || x >= imagewidth){
	    continue;
	}

	for (int j=0;j<task;++j){
	    area.top    = y+TABLE_RADIUS+1;
	    area.bottom = y-TABLE_RADIUS;
	    area.left   = x-TABLE_RADIUS;
	    area.right  = x+TABLE_RADIUS+1;
	    if (rectArray[j].top    <= area.bottom ||
	        rectArray[j].bottom >= area.top ||
		rectArray[j].left   >= area.right ||
		rectArray[j].right  <= area.left){
		continue;
	    }
	    area.top    = __MIN__(__MIN__(area.top,    rectArray[j].top)    - area.bottom, TABLE_SIZE) -1;
	    area.bottom = __MAX__(__MAX__(area.bottom, rectArray[j].bottom) - area.bottom, 0)          -1;
	    area.right  = __MIN__(__MIN__(area.right,  rectArray[j].right)  - area.left,   TABLE_SIZE) -1;
	    area.left   = __MAX__(__MAX__(area.left,   rectArray[j].left)   - area.left,   0)          -1;

	    for (int k=0;k<TABLE_NUMBER;++k){
	        field[k] += table[k][area.top*TABLE_SIZE+area.right];
		if (area.left!=-1)
		    field[k] -= table[k][area.top*TABLE_SIZE+area.left];
		if (area.bottom != -1)
		    field[k] -= table[k][area.bottom*TABLE_SIZE+area.right];
		if (area.left != -1 && area.bottom != -1)
		    field[k] += table[k][area.bottom*TABLE_SIZE+area.left];
	    }
	}
    }
    if (x<imagewidth && y<imageheight){
	for (int k=0;k<TABLE_NUMBER;++k){
	    pixelimage += cudaWeightArray[k>>1]*field[k]*field[k];
	}
        image[y*imagewidth+x]=pixelimage;
    }
}

// with constant memory use

void cudaPrintImage_PIXEL_2(Rectangle* polygonArray,
	INT4 rectNum,
	float* weightArray,
	float tableArray[TABLE_NUMBER][TABLE_SIZE*TABLE_SIZE],
	float* &cudaImage,
	float &cudaTime,
	Rectangle boundBox)
{
    // your code starts here
    clock_t start = clock();
    float *image;
    Rectangle *cudaPolygonArray;
    INT4 imagewidth = (boundBox.right-boundBox.left) / GRID_SIZE;
    INT4 imageheight = (boundBox.top-boundBox.bottom) / GRID_SIZE;
    int tilewidth = imagewidth/CUDA_KERNEL_XSIZE+!!(imagewidth%CUDA_KERNEL_XSIZE);
    int tileheight = imageheight/CUDA_KERNEL_YSIZE+!!(imagewidth%CUDA_KERNEL_YSIZE);

    cudaMalloc((void**)&image, sizeof(float)*imagewidth*imageheight);
    cudaMalloc((void**)&cudaPolygonArray, sizeof(Rectangle)*rectNum);
    cudaMemcpy(cudaPolygonArray, polygonArray, sizeof(Rectangle)*rectNum, cudaMemcpyHostToDevice);
    cudaMemset(image, 0, sizeof(float)*imagewidth*imageheight);

    float * tablearray_temp [TABLE_NUMBER];
    float ** cudaTableArray;
    float * cudaWeightArray;

    for (int i=0;i<TABLE_NUMBER;++i){
	cudaMalloc((void **)& (tablearray_temp[i]), sizeof(float)*TABLE_SIZE*TABLE_SIZE);
	cudaMemcpy(tablearray_temp[i], tableArray[i], sizeof(float)*TABLE_SIZE*TABLE_SIZE, cudaMemcpyHostToDevice);
    }

    cudaMemcpyToSymbol("cudaTableArray", tablearray_temp, sizeof(float*)*TABLE_NUMBER, 0, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol("cudaWeightArray", weightArray, sizeof(float)*KERNEL_NUMBER, 0, cudaMemcpyHostToDevice);

    dim3 GridDim(tilewidth, tileheight);
    dim3 BlockDim(CUDA_KERNEL_XSIZE, CUDA_KERNEL_YSIZE);
    cudaPrintImage_kernel_PIXEL_1<<<GridDim, BlockDim>>>(cudaPolygonArray,
							 rectNum,
							 image,
							 imagewidth,
							 imageheight,
							 boundBox);
    cudaThreadSynchronize();

    cudaMemcpy(cudaImage, image, sizeof(float)*imagewidth*imageheight, cudaMemcpyDeviceToHost);

    for (int i=0;i<TABLE_NUMBER;++i){
	cudaFree(tablearray_temp[i]);
    }
    cudaFree(image);
    cudaFree(cudaPolygonArray);
    cudaFree(cudaTableArray);

    // in the end report the run time by modifying the value of cudaTime;
    cudaTime = (clock()-start)/(double)CLOCKS_PER_SEC;
}

__global__ void cudaPrintImage_kernel_PIXEL_2(Rectangle* polygon,
                                              INT4 rectNum,
					      float* image,
					      INT4 imagewidth,
					      INT4 imageheight,
					      Rectangle boundBox)
{
    __shared__ Rectangle rectArray[CUDA_KERNEL_NUMBER];

    INT4 x = blockIdx.x*CUDA_KERNEL_XSIZE + threadIdx.x;
    INT4 y = blockIdx.y*CUDA_KERNEL_YSIZE + threadIdx.y;
    INT4 tid = threadIdx.y*CUDA_KERNEL_XSIZE + threadIdx.x;
    __shared__ INT4 task;
    Rectangle area;
    float field[TABLE_NUMBER];
    float pixelimage = 0;

    for (int k=0;k<TABLE_NUMBER;++k)
        field[k] = 0;

    for (int i = 0; i < rectNum; i += CUDA_KERNEL_NUMBER){
        __syncthreads();
	if (tid==0)
	    task = (i+CUDA_KERNEL_NUMBER<=rectNum)?CUDA_KERNEL_NUMBER:(rectNum-i);
	__syncthreads();

	if (tid<task){
	    area = polygon[tid+i];
	    rectArray[tid].bottom = (area.bottom - boundBox.bottom)/GRID_SIZE;
	    rectArray[tid].top    = (area.top    - boundBox.bottom)/GRID_SIZE;
	    rectArray[tid].left   = (area.left   - boundBox.left)/GRID_SIZE;
	    rectArray[tid].right  = (area.right  - boundBox.left)/GRID_SIZE;
	}
	__syncthreads();

	if (y >= imageheight || x >= imagewidth){
	    continue;
	}

	for (int j=0;j<task;++j){
	    area.top    = y+TABLE_RADIUS+1;
	    area.bottom = y-TABLE_RADIUS;
	    area.left   = x-TABLE_RADIUS;
	    area.right  = x+TABLE_RADIUS+1;
	    if (rectArray[j].top    <= area.bottom ||
	        rectArray[j].bottom >= area.top ||
		rectArray[j].left   >= area.right ||
		rectArray[j].right  <= area.left){
		continue;
	    }
	    area.top    = __MIN__(__MIN__(area.top,    rectArray[j].top)    - area.bottom, TABLE_SIZE) -1;
	    area.bottom = __MAX__(__MAX__(area.bottom, rectArray[j].bottom) - area.bottom, 0)          -1;
	    area.right  = __MIN__(__MIN__(area.right,  rectArray[j].right)  - area.left,   TABLE_SIZE) -1;
	    area.left   = __MAX__(__MAX__(area.left,   rectArray[j].left)   - area.left,   0)          -1;

	    for (int k=0;k<TABLE_NUMBER;++k){
	        field[k] += cudaTableArray[k][area.top*TABLE_SIZE+area.right];
		if (area.left!=-1)
		    field[k] -= cudaTableArray[k][area.top*TABLE_SIZE+area.left];
		if (area.bottom != -1)
		    field[k] -= cudaTableArray[k][area.bottom*TABLE_SIZE+area.right];
		if (area.left != -1 && area.bottom != -1)
		    field[k] += cudaTableArray[k][area.bottom*TABLE_SIZE+area.left];
	    }
	}
    }
    if (x<imagewidth && y<imageheight){
	for (int k=0;k<TABLE_NUMBER;++k){
	    pixelimage += cudaWeightArray[k>>1]*field[k]*field[k];
	}
        image[y*imagewidth+x]=pixelimage;
    }
}




// This is a third version where local polygons are stored in the constant
// memory and shared by all the pixels. constant memory size is 65536! good
// for small area with limitation in the aerial image area which is 32umx32um
void cudaPrintImage_PIXEL_3(Rectangle* polygonArray,
	INT4 rectNum,
	float* weightArray,
	float tableArray[TABLE_NUMBER][TABLE_SIZE*TABLE_SIZE],
	float* &cudaImage,
	float &cudaTime,
	Rectangle boundBox)
{
    // your code starts here
    float b;
//    cudaMalloc








    // in the end report the run time by modifying the value of cudaTime;
    cudaTime = 0.0;
}

// This is a four version where local polygons are stored in the shared memory
// and each table is partitioned and stored in the constant memory. 
// cuda is called for several times.
