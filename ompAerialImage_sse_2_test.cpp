#include <iostream>
#include <time.h>
#include <complex>
#include <xmmintrin.h>
#include <emmintrin.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifndef __APPLE__
#include <malloc.h>
#endif
#include <omp.h>
#include <limits.h>

#include "ompAerialImage.h"

using namespace std;

static __m128 zeroAllMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0x0));
static __m128 zeroLeastMask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0xFFffFFff, 0x0));
static __m128 zeroLeast2Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0x0, 0x0));
static __m128 zeroLeast3Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0x0, 0x0, 0x0));
static __m128 zeroMostMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0xFFffFFff, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost2Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost3Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0xFFffFFff));
static __m128 zeroLeastMasks[3] = {zeroLeastMask, zeroLeast2Mask, zeroLeast3Mask};
static __m128 zeroMostMasks[3] = {zeroMostMask, zeroMost2Mask, zeroMost3Mask};
static __m128 leaveLeastMasks[4] = {zeroAllMask, zeroMost3Mask, zeroMost2Mask, zeroMostMask};
static __m128 leaveMostMasks[4] = {zeroAllMask, zeroLeast3Mask, zeroLeast2Mask, zeroLeastMask};

// /brief
// Each SSE instruction simultaneously updates several pixels in the same image.
// The framework is similar to "gold_continuous", except that "gold_continuous"
// only updates 1 pixel one time.
// Current implementation is to update 4 pixels at the same time. In order to
// use sse instructions, we have 
// 1. The size of sseImage MUST be a multiple of 4.
//  - Here, the memory padding/allocatio of sseImage is done inside the caller function,
//    i.e. main function, paddedImageSize is the size after padding
// 2. The size of field image MUST be a multiple of 4.
//  - The allocation is the same as sseImage
// 3. When updating field_sse by each row of haloTable, we have the following two
// situations:
//  3.1. If start idx and end idx is the start of a multiple of 4 and end of
//  multiple of 4, do nothing
//  3.2. If NOT,
//      a. (CURRENT IMPLEMENTATION), manually update the remainder at
//      the first and last slot
//      b. padding..........(has to change the allocation of haloTable)
// 3. The size of haloTable MUST be a multiple of 4
//  - pad into a multiple of 4 by inserting to the end of the array
// FIXME. According to impactRegion, allocate smaller haloTable???? But we
// cannot allocate a single haloTable that works on all rectangels...
void ompPrintImage_sse_2_test(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weightArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize)
{
  #pragma omp parallel
  {
    if (omp_get_thread_num() == 0)
      cout << "The total # of threads: " << omp_get_num_threads() << endl;
  }

  double ompTimer = omp_get_wtime();
  //clock_t startTime = clock();

  int boundwidth = (boundBox.right - boundBox.left) >> 2; // divide by GRID_SIZE(4)
  int boundheight = (boundBox.top - boundBox.bottom) >> 2;

  // Find the left/right/top/bottom-most boundary among all rectangels.
  Rectangle rectBound;
  rectBound.bottom = rectBound.left = 0; //(bottom-RADIUS) is always <= 0
  rectBound.top = rectBound.right = INT_MIN;
  int rectWidth = 0;
  int rectHeight = 0;
  for (int i = 0; i < rectNum; ++i) {
    if ((polygonArray[i].right - polygonArray[i].left) > rectWidth)
      rectWidth = polygonArray[i].right - polygonArray[i].left;
    if ((polygonArray[i].top - polygonArray[i].bottom) > rectHeight)
      rectHeight = polygonArray[i].top - polygonArray[i].bottom;
  }
  rectWidth = rectWidth >> 2;
  rectHeight = rectHeight >> 2;
  rectBound.top = min(boundheight, rectHeight) + TABLE_SIZE;
  if (rectBound.top < TABLE_SIZE)
    rectBound.top = TABLE_SIZE;
  rectBound.right = min(boundwidth, rectWidth) + TABLE_SIZE;
  int haloWidth = rectBound.right;
  int haloHeight = rectBound.top;
  //int haloWidth = (rectBound.right - rectBound.left) + TABLE_SIZE;
  //int haloHeight = (rectBound.top - rectBound.bottom) + TABLE_SIZE;
  // We want haloWidth%4 == boundwidth%4
  haloWidth += (boundwidth%4 - haloWidth%4 + 4)%4;
  int haloTotalSize = haloWidth * haloHeight;
  //cout << "rectBound.left: " << rectBound.left << ", bottom: " << rectBound.bottom << endl;

  __m128* image_sse = (__m128*) sseImage; //stored in reverse order.....
  int sz_sse = paddedImageSize >> 2;
  omp_set_num_threads(NUM_THREADS);
  //#pragma omp parallel for
  #pragma omp parallel for
  for (int i = 0; i < sz_sse; i++) {
    image_sse[i] = _mm_setzero_ps();
  }

  //#pragma omp parallel num_threads(4)
  {
  float* field; //is an array of paddedImageSizee
//#ifdef __APPLE__
  field = (float *) malloc(sizeof(float) * paddedImageSize);
  __m128* field_sse = (__m128*) field; //stored in reverse order.....

  Rectangle polygonImage;

  //#pragma omp for
  for (int tableIdx = 0; tableIdx < TABLE_NUMBER; tableIdx++) {
    int weightIdx = tableIdx >> 1;

    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for
    for (int i = 0; i < paddedImageSize>>2; i++) {
      field_sse[i] = _mm_setzero_ps(); 
    }

    // Create ONE halo-lookuptable that works for all rectangles. LUT is extended into
    // imageSize by adding halo to LUT. Halo is assigned boundary values.
#if 0
    // Besides, we want haloWidth%4 is equal to boundwidth%4. So, although
    // haloWidth = boundwidth + TABLE_SIZE is enough, +3(=0+4-257%4) is necessary.
    // FIXME. Does haloHeight also need '+3'?
    // FIXME. Can haloWidth/haloHeight be defined based on the
    // left/right/up/down-most rectangels? By doing so, we can reduce some
    // memory allocation. Does this help speedup???
    int haloWidth = boundwidth + TABLE_SIZE + 3;
#if 0
    int haloHeight = boundheight + TABLE_SIZE + 3; 
#endif
    int haloHeight = boundheight + TABLE_SIZE; 
    int haloTotalSize = haloWidth * haloHeight;
#endif
    float* haloTable[4]; //0: no shift, 1: shift right one index, 2: shift right two, 3: shift right three
    // Allocate and initialize haloTable[0]. The origianl haloTable but is
    // reversed.
    // Allocate other haloTables
    omp_set_num_threads(4);
    #pragma omp parallel for
    for (int i = 0; i < 4; ++i) {
//#ifdef __APPLE__
    haloTable[i] = (float*) malloc(sizeof(float) * (haloTotalSize + i));
    }
    int h = min(TABLE_SIZE, haloHeight);
    int w = min(TABLE_SIZE, haloWidth);
    // For the origianl rows >= TABLE_SIZE, now they start from row 0
    if (h != haloHeight) {
      float* upperBoundaryRow = (float*) malloc(sizeof(float)*haloWidth);
      // Set corner boundary value first, i.e. x >= TABLE_SIZE && y >=
      // TABLE_SIZE
      fill_n(upperBoundaryRow, haloWidth - w, tableArray[tableIdx][TABLE_SIZE*TABLE_SIZE-1]);
      // Reverse the upper boundary table row
      omp_set_num_threads(min(w, NUM_THREADS));
      #pragma omp parallel for firstprivate(haloWidth, w, tableIdx)
      for (int x = haloWidth-w; x < haloWidth; ++x) {
        int tx = w-1-(x-(haloWidth-w));
        upperBoundaryRow[x] = tableArray[tableIdx][(TABLE_SIZE-1)*TABLE_SIZE+tx];
      }
      omp_set_num_threads(NUM_THREADS);
      #pragma omp parallel for firstprivate(haloWidth)
      for (int y = 0; y < haloHeight-h; ++y) 
        memcpy(haloTable[0]+y*haloWidth, upperBoundaryRow, sizeof(float)*haloWidth);
      free(upperBoundaryRow);
    }
    // For the original rows from rectBound.bottom to TABLE_SIZE-1, now they start from
    // haloHeight-h
    // (tx, ty) is for the reversed table index
    omp_set_num_threads(min(h, NUM_THREADS));
    #pragma omp parallel for firstprivate(haloHeight, h, tableIdx)
    for (int y = haloHeight-h; y < haloHeight; ++y) {
      // Set boundary values first, i.e. x >= TABLE_SIZE
      int ty = h-1-(y-(haloHeight-h));
      fill_n(haloTable[0]+y*haloWidth, haloWidth - w, tableArray[tableIdx][ty*TABLE_SIZE+TABLE_SIZE-1]);
      for (int x = haloWidth - w, tx = w - 1; x < haloWidth; ++x, --tx)
        haloTable[0][y*haloWidth + x] = tableArray[tableIdx][ty*TABLE_SIZE+tx];
    }
//#if 0
    // Initialize other haloTables
    omp_set_num_threads(3);
    #pragma omp parallel for firstprivate(haloTotalSize)
    for (int i = 1; i < 4; ++i) {
      memset(haloTable[i], 0, sizeof(float)*i);
      memcpy(haloTable[i]+i, haloTable[0], sizeof(float)*haloTotalSize);
    }
//#endif

    __m128* table_sse[4];
//#if 0
    omp_set_num_threads(4);
    #pragma omp parallel for
    for (int i = 0; i < 4; ++i)
      table_sse[i] = (__m128*) haloTable[i];
//#endif
    //table_sse[0] = (__m128*) haloTable[0];

    for (int i = 0; i < rectNum; i++) {
      // Note that all polygons are inside the left and bottom boundary of the
      // aerial image, but some of them exceed the right and top boundary.
      polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom) >> 2;
      polygonImage.top    = (polygonArray[i].top    - boundBox.bottom) >> 2;
      polygonImage.left   = (polygonArray[i].left   - boundBox.left)   >> 2;
      polygonImage.right  = (polygonArray[i].right  - boundBox.left)   >> 2;
      // Ignore the polygon that is totally outside the aerial image
      if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
        continue;
      // The polygon's top/right boundary over some limit is meaningless, i.e.
      // LUT won't be used to check those pixels. The limit is as follows:
      // FIXME. NOT SURE....
      // (boundheight-1) (the right most pixel in the aerial image) +
      // TABLE_RADIUS + 1.
      polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
      polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

      pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
      corners[0] = make_pair(polygonImage.right, polygonImage.top);
      corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
      corners[2] = make_pair(polygonImage.left, polygonImage.top);
      corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

      Rectangle impactRegion;
      impactRegion.bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
      impactRegion.top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
      impactRegion.left = max(polygonImage.left - KERNEL_RADIUS, 0);
      impactRegion.right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);


      // top-left and bottom-right corners
      for (int c = 2; c < 4; ++c) {
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegion.bottom;
        int k = impactRegion.left;
        int rely = corners[c].second - (impactRegion.bottom - TABLE_RADIUS) - 1; 
        int relx = corners[c].first - (impactRegion.left - TABLE_RADIUS) - 1;
        //assert(rely < haloHeight);
        //assert(relx < haloWidth);
        int ySteps = min(impactRegion.top - impactRegion.bottom, rely+1);
        int xSteps = min(impactRegion.right - impactRegion.left, relx+1);
        if (xSteps <= 0)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
        omp_set_num_threads(min(ySteps, NUM_THREADS));
        //#pragma omp parallel for num_threads(NUM_THREADS)
        #pragma omp parallel for firstprivate(j, boundwidth, rely, numShifted) 
        //for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
        for (int ys = 0; ys < ySteps; ++ys) {
          int fj = (j + ys) * boundwidth;
          int ty = rely - ys;
          //FIXME. NOT SURE!!!!
          //int numShifted = ((fj+k)%4 - (haloTotalSize-(ty*haloWidth+relx)-1)%4 + 4)%4;
          //assert(numShifted == ((fj+k)%4 - (haloTotalSize-(ty*haloWidth+relx)-1)%4 + 4)%4);
          //assert(ty >= 0);
          //assert((relx-xSteps+1) >= 0);
          int start_field = (fj + k); // start index of field 
          int start_field_sse = (fj + k) >> 2; // start index of field_sse
          int end_field_sse = (fj + k + xSteps - 1) >> 2;
          int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          //assert(start_table >= 0);
//#if 0
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse

          int ti = start_table_sse;
          for (int fi = start_field_sse; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_sub_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the first slot
          __m128 sub = _mm_and_ps(table_sse[numShifted][start_table_sse], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[start_field_sse] = _mm_add_ps(field_sse[start_field_sse], sub);
          // Undo the paddings in the last slot
          //int end_table_sse = (start_table + xSteps - 1) / 4;
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_add_ps(field_sse[end_field_sse], sub);
//#endif
#if 0
          for (int fi = start_field_sse, ti = start_table-numFirstPadded_field; fi <= end_field_sse; ++fi, ti+=4) {
            __m128 x = _mm_set_ps(haloTable[0][ti+3], haloTable[0][ti+2], haloTable[0][ti+1], haloTable[0][ti]);
            field_sse[fi] = _mm_sub_ps(field_sse[fi], x);
          }
          // undo the first slot
          for (int idx = 0, fi = start_field_sse << 2, ti = start_table-numFirstPadded_field; idx < numFirstPadded_field; ++idx, ++fi, ++ti) {
            field[fi] += haloTable[0][ti];
          }
          // undo the last slot
          for (int idx = 0, fi = start_field + xSteps, ti = start_table + xSteps; idx < numLastPadded_field; ++idx, ++fi, ++ti) {
            field[fi] += haloTable[0][ti];
          }
#endif

#if 0
          fj += boundwidth;
          ty--;
#endif
        }
      }
      // top-right and bottom-left corners
      for (int c = 0; c < 2; ++c) {
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegion.bottom;
        int k = impactRegion.left;
        int rely = corners[c].second - (impactRegion.bottom - TABLE_RADIUS) - 1; 
        int relx = corners[c].first - (impactRegion.left - TABLE_RADIUS) - 1;
        //assert(rely < haloHeight);
        //assert(relx < haloWidth);
        int ySteps = min(impactRegion.top - impactRegion.bottom, rely+1);
        int xSteps = min(impactRegion.right - impactRegion.left, relx+1);
        if (xSteps <= 0)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
        omp_set_num_threads(min(ySteps, NUM_THREADS));
        //#pragma omp parallel for num_threads(NUM_THREADS)
        #pragma omp parallel for firstprivate(j, boundwidth, rely, numShifted) 
        //for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
        for (int ys = 0; ys < ySteps; ++ys) {
          int fj = (j + ys) * boundwidth;
          int ty = rely - ys;
          //FIXME. NOT SURE!!!!
          //int numShifted = ((fj+k)%4 - (haloTotalSize-(ty*haloWidth+relx)-1)%4 + 4)%4;
          //assert(ty >= 0);
          //assert((relx-xSteps+1) >= 0);
          //assert(numShifted == ((fj+k)%4 - (haloTotalSize-(ty*haloWidth+relx)-1)%4 + 4)%4);
#if 0
          if (numShifted != (((fj*boundwidth+k)%4 - (haloTotalSize-(ty*haloWidth+relx)-1)%4 + 4)%4)) {
            cout << "numShifted: " << numShifted << ", cur: " << ((fj*boundwidth+k)%4 - (haloTotalSize-(ty*haloWidth+relx)-1)%4 + 4)%4 << endl;
          }
#endif
          int start_field = (fj + k); // start index of field 
          int start_field_sse = (fj + k) >> 2; // start index of field_sse
          int end_field_sse = (fj + k + xSteps - 1) >> 2;
          int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          //assert(start_table >= 0);
//#if 0
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse

          int ti = start_table_sse;
          for (int fi = start_field_sse; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the first slot
          __m128 sub = _mm_and_ps(table_sse[numShifted][start_table_sse], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[start_field_sse] = _mm_sub_ps(field_sse[start_field_sse], sub);
          // Undo the paddings in the last slot
          //int end_table_sse = (start_table + xSteps - 1) / 4;
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);
//#endif
#if 0
          for (int fi = start_field_sse, ti = start_table-numFirstPadded_field; fi <= end_field_sse; ++fi, ti+=4) {
            __m128 x = _mm_set_ps(haloTable[0][ti+3], haloTable[0][ti+2], haloTable[0][ti+1], haloTable[0][ti]);
            field_sse[fi] = _mm_add_ps(field_sse[fi], x);
          }
          // undo the first slot
          for (int idx = 0, fi = start_field_sse << 2, ti = start_table-numFirstPadded_field; idx < numFirstPadded_field; ++idx, ++fi, ++ti) {
            field[fi] -= haloTable[0][ti];
          }
          // undo the last slot
          for (int idx = 0, fi = start_field + xSteps, ti = start_table + xSteps; idx < numLastPadded_field; ++idx, ++fi, ++ti) {
            field[fi] -= haloTable[0][ti];
          }
#endif

#if 0
          fj += boundwidth;
          ty--;
#endif
        }
      }
    }
//#if 0
    omp_set_num_threads(4);
    #pragma omp parallel for
    for (int i = 0; i < 4; ++i)
      free(haloTable[i]);
//#endif
    //free(haloTable[0]);
    int sz_sse = paddedImageSize >> 2;
    //#pragma omp critical
    omp_set_num_threads(NUM_THREADS);
#if 0
    int sz_thread = sz_sse/NUM_THREADS;
    int left_threads = sz_sse%NUM_THREADS;
    //cout << "sz_sse: " << sz_sse << endl;
    #pragma omp parallel
    {
      int startIdx = omp_get_thread_num() * sz_thread + min(omp_get_thread_num(), left_threads);
      int sz = sz_thread + (omp_get_thread_num() <= (left_threads-1) ? 1: 0);
      for (int i = startIdx; i < sz + startIdx; ++i) {
        __m128 square = _mm_mul_ps(field_sse[i], field_sse[i]);
        __m128 weight_sse = _mm_set_ps(weightArray[weightIdx], weightArray[weightIdx], weightArray[weightIdx], weightArray[weightIdx]);
        __m128 tmp = _mm_mul_ps(square, weight_sse);
        image_sse[i] = _mm_add_ps(image_sse[i], tmp);
      }
    }
#endif
    //#pragma omp critical
    float weightValue = weightArray[weightIdx];
    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for firstprivate(weightValue)
    for (int i = 0; i < sz_sse; i++) {
      __m128 square = _mm_mul_ps(field_sse[i], field_sse[i]);
      __m128 weight_sse = _mm_set_ps(weightValue, weightValue, weightValue, weightValue);
      __m128 tmp = _mm_mul_ps(square, weight_sse);
      image_sse[i] = _mm_add_ps(image_sse[i], tmp);
    }
  }
  free(field);
  }

  //ompTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
  ompTime = omp_get_wtime() - ompTimer;
}

