#include "fft.h"
#include <iostream>

using namespace std;

int main()
{
    complex<double> a[16];
    
    for (int i=0;i<16;i++){
	a[i] = complex<double>(i, 0);
	cout<<a[i]<<endl;
    }

    cout<<"result:"<<endl;
    ifft2(8, 2, a);

    for (int i=0;i<16;i++){
	cout<<a[i]<<endl;
    }    
}
