#ifndef DATASTRUCT_H
#define DATASTRUCT_H

#ifdef DDEBUG

#define KERNEL_SIZE 17
#define TABLE_SIZE 17
#define KERNEL_RADIUS 8
#define TABLE_RADIUS 8
#define TABLE_NUMBER 2
#define KERNEL_NUMBER 1
#define PADDED_TABLE_NUMBER 4 // Must be a multiple of 4
#define PADDED_KERNEL_NUMBER 2 // Must be a multiple of 2

#endif

#ifndef DDEBUG

#define KERNEL_SIZE 257
#define TABLE_SIZE 257
#define KERNEL_RADIUS 128
#define TABLE_RADIUS 128
#define TABLE_NUMBER 26 
#define KERNEL_NUMBER 13 
#define PADDED_TABLE_NUMBER 28 // Must be a multiple of 4
#define PADDED_KERNEL_NUMBER 14 // Must be a multiple of 2

#endif

#define GRID_SIZE 4
#define ITER_NUM 1
#define NUM_THREADS 4

#define USE_DOUBLE 0

using namespace std;

typedef int INT4;
typedef unsigned int UINT4;

#if USE_DOUBLE
typedef double DTYPE;
#else
typedef float DTYPE;
#endif

struct Rectangle{
    INT4 left;
    INT4 right;
    INT4 top;
    INT4 bottom;
};

#endif
