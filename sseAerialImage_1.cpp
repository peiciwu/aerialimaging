#include <iostream>
#include <time.h>
#include <complex>
#include <smmintrin.h>
#include <xmmintrin.h>
#include <stdlib.h>
#ifndef __APPLE__
#include <malloc.h>
#endif

#include "sseAerialImage.h"

using namespace std;

static bool overlapping(Rectangle& area, int x, int y, Rectangle polygonImage);

// /brief
// Each SSE instruction updates the same pixel in different images.
// Current implementation is to consider 2 images simultaneously
//  - that is 4 lookup tables (one image has two lookup tables: 1 real, 1
//  imaginary)
void ssePrintImage_1(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weightArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox)
{
  clock_t startTime = clock();
  int boundwidth = (boundBox.right - boundBox.left) / GRID_SIZE;
  int boundheight = (boundBox.top - boundBox.bottom) / GRID_SIZE;

//  __m128* sseVector = (__m128*) sseImage; //stored in reversed order...

  float* field; // is an array of 4*imageSize
//#ifdef __APPLE__
  field = (float *) malloc(sizeof(float) * boundwidth * boundheight * 4);
  __m128* field_sse = (__m128*) field; //stored in reverse order.....

  Rectangle polygonImage;
  Rectangle overlapRegion;

  // FIXME. sseImage should be vectorized.
  for (int i=0;i<boundwidth*boundheight;i++) {
    sseImage[i] = 0;
  }

  for (int weightIdx=0, kernelIdx=0; weightIdx<PADDED_KERNEL_NUMBER; weightIdx+=2, kernelIdx+=4) {
    // Re-arrage the memory layout of the lookup tables for SSE access (one sse
    // for 4 lookup tables)
    float* fourTables = (float*) malloc(sizeof(float) * TABLE_SIZE * TABLE_SIZE *4);
    __m128* table_sse = (__m128*) fourTables;
    for (int i = 0; i < TABLE_SIZE*TABLE_SIZE; ++i) {
      table_sse[i] = _mm_set_ps(tableArray[kernelIdx+3][i], tableArray[kernelIdx+2][i], tableArray[kernelIdx+1][i], tableArray[kernelIdx][i]);
    }
    for (int i=0; i < boundwidth*boundheight; i++) {
      field_sse[i] = _mm_setzero_ps(); //field_sse[i][0, 1, 2, 3] = 0
    }

    for (int i=0; i < rectNum; i++) {
      polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom)/GRID_SIZE;
      polygonImage.top    = (polygonArray[i].top    - boundBox.bottom)/GRID_SIZE;
      polygonImage.left   = (polygonArray[i].left   - boundBox.left)  /GRID_SIZE;
      polygonImage.right  = (polygonArray[i].right  - boundBox.left)  /GRID_SIZE;

      // for the topright corner
      // FIXME. if tableArray is in continus memory regarding kernelIdx, we can allocate x
      // by directing assessing a 128-bits memory, rather than indidually
      // setting them.
      __m128 x;
      for (int j = max(polygonImage.bottom-KERNEL_RADIUS, 0); j < min(polygonImage.top+KERNEL_RADIUS, boundheight); ++j)
        for (int k = max(polygonImage.left-KERNEL_RADIUS,0); k < min(polygonImage.right+KERNEL_RADIUS, boundwidth); ++k)
          //if (overlapping(overlapRegion, k, j, polygonImage)) {
          {
            overlapping(overlapRegion, k, j, polygonImage);
            int f_idx = j * boundwidth + k;
            int t_idx = overlapRegion.top * TABLE_SIZE + overlapRegion.right;
#if 0
            x = _mm_set_ps(tableArray[kernelIdx+3][t_idx], tableArray[kernelIdx+2][t_idx], tableArray[kernelIdx+1][t_idx], tableArray[kernelIdx][t_idx]);
            field_sse[f_idx] = _mm_add_ps(field_sse[f_idx], x);
#endif
            field_sse[f_idx] = _mm_add_ps(field_sse[f_idx], table_sse[t_idx]);

            if (overlapRegion.left != -1) {
              t_idx = overlapRegion.top * TABLE_SIZE + overlapRegion.left;
#if 0
              x = _mm_set_ps(tableArray[kernelIdx+3][t_idx], tableArray[kernelIdx+2][t_idx], tableArray[kernelIdx+1][t_idx], tableArray[kernelIdx][t_idx]);
              field_sse[f_idx] = _mm_sub_ps(field_sse[f_idx], x);
#endif
              field_sse[f_idx] = _mm_sub_ps(field_sse[f_idx], table_sse[t_idx]);
            }

            if (overlapRegion.bottom != -1) {
              t_idx = overlapRegion.bottom * TABLE_SIZE + overlapRegion.right;
#if 0
              x = _mm_set_ps(tableArray[kernelIdx+3][t_idx], tableArray[kernelIdx+2][t_idx], tableArray[kernelIdx+1][t_idx], tableArray[kernelIdx][t_idx]);
              field_sse[f_idx] = _mm_sub_ps(field_sse[f_idx], x);
#endif
              field_sse[f_idx] = _mm_sub_ps(field_sse[f_idx], table_sse[t_idx]);
            }

            if (overlapRegion.bottom != -1 && overlapRegion.left != -1) {
              t_idx = overlapRegion.bottom * TABLE_SIZE + overlapRegion.left;
#if 0
              x = _mm_set_ps(tableArray[kernelIdx+3][t_idx], tableArray[kernelIdx+2][t_idx], tableArray[kernelIdx+1][t_idx], tableArray[kernelIdx][t_idx]);
              field_sse[f_idx] = _mm_add_ps(field_sse[f_idx], x);
#endif
              field_sse[f_idx] = _mm_add_ps(field_sse[f_idx], table_sse[t_idx]);
            }
          }
    }

    //FIXME.  
    //1. weightArray is not stored in continus memory???
    //2. Has to check the performance i.e. horizontal add / dot product
    //   SSE2: addps, addss
    //   SSE3: haddps, haddps
    //   SSE4.1: dpps ------------ current implementation
    //3. Best instruction to store the least significant float from xmm
    //register?
    __m128 weight = _mm_set_ps(weightArray[weightIdx+1], weightArray[weightIdx+1], weightArray[weightIdx], weightArray[weightIdx]);
    const int mask = 0xf1;
    for (int i = 0;i < boundwidth * boundheight; i++) {
      __m128 tmp = _mm_mul_ps(field_sse[i], weight);
      __m128 sum = _mm_dp_ps(field_sse[i], tmp, mask);
      __m128 cur = _mm_load_ss(sseImage+i);
      sum = _mm_add_ss(cur, sum);
      _mm_store_ss(sseImage+i, sum);
    }
    free(fourTables);
  }
  free(field);

  sseTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
}

static bool overlapping(Rectangle& area, int x, int y, Rectangle polygonImage)
{
    int tableTop, tableBottom, tableLeft, tableRight;
    tableTop    = y+TABLE_RADIUS+1;
    tableBottom = y-TABLE_RADIUS;
    tableLeft   = x-TABLE_RADIUS;
    tableRight  = x+TABLE_RADIUS+1;

#if 0
    if (polygonImage.top <= tableBottom ||		// modified here
	    polygonImage.bottom >= tableTop ||
	    polygonImage.left >= tableRight ||
	    polygonImage.right <= tableLeft)		// modified here
	return false;
#endif

    area.top = min(tableTop, polygonImage.top)-tableBottom-1;     // modified here
    area.bottom = max(tableBottom, polygonImage.bottom)-tableBottom-1;    // modified here
    area.left = max(tableLeft, polygonImage.left)-tableLeft-1;	    // modified here
    area.right = min(tableRight, polygonImage.right)-tableLeft-1;	// modified here

    return true;
}
