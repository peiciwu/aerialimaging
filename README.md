# README

- Introduction

This project is to run aerial image simulation by using SSE commands. (If
possible, AVX is also an option).

The purpose is to compare the performace with running on GPU. 

The SSE commands only rely on CPU, and most CPUs contains SSE/SSE2 commands. SSE
contains eight 128-bit registers and SSE2 doubles them to sixteen.

Other than SSE, in order to be comparable with GPU performance,
it might be necessary to use some techniques of multiple threading, which will be
designed later.


* Reference: Accelerating Aerial Image Simulation with GPU

- How to Run the Program

Please use the provided makefile. 'make' and 'make debug' will generate
'aerial' and 'aerial-debug', repectively, in 'build' directory. Then you can
run it wiht './build/aerial data/small_1.txt 14'. 'aerial-debug' is enable
'DDEBUG' flag which aims to run the small test cases (test_1.txt and test_2.txt.)

Note that pleaase make sure your machine supports SSE4.1 and has enough memory. 
