running...
Reading polygon list
Reading table list
reading weight list
running gold code...
gold run time is 7.51 s
running cuda code...
command is all
command is all

=======  running cuda initiate P7  ======
comparing result...
Gold 0.0026619 relative error inf
Gold 0.0873927 error 6555.38
max error = 6555.38
cuda PIXEL_7 run time is 0.22 s
speed up is 34.1364 X



=======  running cuda LUT_2  ======
allocating device memory...
start kernel...
comparing result...
Gold 0.0053782 relative error 0.000180731
Gold 0.130456 error 1.47521e-06
max error = 1.47521e-06
cuda LUT_2 run time is 3.92 s
speed up is 1.91582 X



=======  running cuda LUT_3  ======
allocating device memory...
start kernel...
comparing result...
Gold 0.00187857 relative error 0.000297306
Gold 0.00505195 error 1.36578e-06
max error = 1.36578e-06
cuda LUT_3 run time is 3.66 s
speed up is 2.05191 X



=======  running cuda PIXEL_1  ======
tilewidth = 58, tileheight = 18
comparing result...
Gold 0.00525724 relative error 1.56776e-05
Gold 0.15344 error 1.07288e-06
max error = 1.07288e-06
cuda PIXEL_1 run time is 0.32 s
speed up is 23.4688 X



=======  running cuda PIXEL_2  ======
comparing result...
Gold 0.00408775 relative error 5.35409e-06
Gold 0.140202 error 2.98023e-07
max error = 2.98023e-07
cuda PIXEL_2 run time is 0.19 s
speed up is 39.5263 X



=======  running cuda PIXEL_3  ======
comparing result...
Gold 0.00408775 relative error 5.35409e-06
Gold 0.140202 error 2.98023e-07
max error = 2.98023e-07
cuda PIXEL_3 run time is 0.22 s
speed up is 34.1364 X



=======  running cuda PIXEL_4  ======
comparing result...
Gold 0.00408775 relative error 5.35409e-06
Gold 0.140202 error 2.98023e-07
max error = 2.98023e-07
cuda PIXEL_4 run time is 0.21 s
speed up is 35.7619 X



=======  running cuda PIXEL_5  ======
Overhead Time is 0
comparing result...
Gold 0.00408775 relative error 5.35409e-06
Gold 0.140202 error 2.98023e-07
max error = 2.98023e-07
cuda PIXEL_5 run time is 0.16 s
speed up is 46.9375 X



=======  running cuda PIXEL_6  ======
clock_start

comparing result...
Gold 0.00525724 relative error 1.56776e-05
Gold 0.15344 error 1.07288e-06
max error = 1.07288e-06
cuda PIXEL_6 run time is 0.76 s
speed up is 9.88158 X



=======  running cuda PIXEL_7  ======
comparing result...
Gold 0.0026619 relative error inf
Gold 0.0873927 error 6555.38
max error = 6555.38
cuda PIXEL_7 run time is 0.16 s
speed up is 46.9375 X



=======  running cuda PIXEL_8  ======
Overhead Time is 0.01
comparing result...
Gold 0.00416863 relative error 0.00496732
Gold 0.161276 error 0.000257015
max error = 0.000257015
cuda PIXEL_8 run time is 0.16 s
speed up is 46.9375 X



======== running cuda UCLA =========
comparing result...
Gold 0.00200538 relative error 0.00101077
Gold 0.0871857 error 3.21493e-05
max error = 3.21493e-05
cuda UCLA run time is 1.96 s
speed up is 3.83163 X



=======  running cuda SHARE_1  ======
Overhead Time is 0
comparing result...
Gold 0.00408775 relative error 5.35409e-06
Gold 0.140202 error 2.98023e-07
max error = 2.98023e-07
cuda SHARE_1 run time is 0.2 s
speed up is 37.55 X


