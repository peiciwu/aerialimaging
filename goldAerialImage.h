#ifndef GOLDAERIALIMAGE_H
#define GOLDAERIALIMAGE_H

#include"datastruct.h"

void goldPrintImage(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    double** tableArray,
	    double* &goldImage,
	    float &goldTime,
	    Rectangle boundBox);

void goldPrintImage_fft(Rectangle*,
	    INT4 rectNum,
	    float* weightArray,
	    float kernelarray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &goldImage,
	    float &goldTime,
	    Rectangle boundBox);

void goldPrintImage_continuous(Rectangle*,
            INT4 rectNum,
            float* weightArray,
            DTYPE** tableArray,
            DTYPE* &goldImage,
            float &goldTime,
            Rectangle);

void goldPrintImage_continuous_2(Rectangle*,
            INT4 rectNum,
            float* weightArray,
            DTYPE** tableArray,
            DTYPE* &goldImage,
            float &goldTime,
            Rectangle);
#endif
