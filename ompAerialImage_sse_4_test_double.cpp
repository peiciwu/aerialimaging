#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <utility>
#include <iostream>
#include <smmintrin.h>
#include <xmmintrin.h>
#include <omp.h>
#include "ompAerialImage.h"

using namespace std;

// /brie
// Each SSE instruction updates the same pixels in different images, that is
// consider 4 lookup tables simultaneously.
// The updating framework is from "gold_continuous_2"
void ompPrintImage_sse_4_test_double(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weightArray,
                   double** tableArray,
                   double* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize)
{
  double ompCompute = 0.f;
  #pragma omp parallel
  {
    if (omp_get_thread_num() == 0)
      cout << "The total # of threads: " << omp_get_num_threads() << endl;
  }
  //clock_t startTime = clock();
  omp_set_num_threads(NUM_THREADS);
  double ompTimer = omp_get_wtime();

  int boundwidth = (boundBox.right - boundBox.left) >> 2; // divide by GRID_SIZE(4)
  int boundheight = (boundBox.top - boundBox.bottom) >> 2;

  __m128d* image_sse = (__m128d*) sseImage; //stored in reverse order.....
  #pragma omp parallel for
  //for (int s = 0, i = 0; s < paddedImageSize; s += 4, i++) {
  for (int s = 0; s < paddedImageSize; s += 2) {
    int i = s >> 1;
    image_sse[i] = _mm_setzero_pd();
  }

  int imageSize = boundwidth * boundheight;
  //#pragma omp parallel
  {
  double* field; // is an array of 2*imageSize
//#ifdef __APPLE__
  field = (double *) malloc(sizeof(double) * imageSize * 2);
  __m128d* field_sse = (__m128d*) field; //stored in reverse order.....

#if 0
  // Get the haloWidth/haloHeight that works for all rectangles
  Rectangle rectBound;
  rectBound.bottom = rectBound.left = 0; //(bottom-RADIUS) is always <= 0
  //rectBound.top = rectBound.right = INT_MIN;
  int rectWidth = 0;
  int rectHeight = 0;
  for (int i = 0; i < rectNum; ++i) {
    if ((polygonArray[i].right - polygonArray[i].left) > rectWidth)
      rectWidth = polygonArray[i].right - polygonArray[i].left;
    if ((polygonArray[i].top - polygonArray[i].bottom) > rectHeight)
      rectHeight = polygonArray[i].top - polygonArray[i].bottom;
  }
  rectWidth = rectWidth >> 2;
  rectHeight = rectHeight >> 2;

  rectBound.top = min(boundheight, rectHeight) + TABLE_SIZE;
  if (rectBound.top < TABLE_SIZE)
    rectBound.top = TABLE_SIZE;
  rectBound.right = min(boundwidth, rectWidth) + TABLE_SIZE;
  int haloWidth = rectBound.right;
  int haloHeight = rectBound.top;
  // We want haloWidth%4 == boundwidth%4
  haloWidth += (boundwidth%4 - haloWidth%4 + 4)%4;
  int haloTotalSize = haloWidth * haloHeight;
#endif
  int haloWidth = boundwidth + TABLE_SIZE;
  int haloHeight = boundheight + TABLE_SIZE; 
  int haloTotalSize = haloWidth * haloHeight;

  //#pragma omp for
  //for (int weightIdx=0, kernelIdx=0; weightIdx<PADDED_KERNEL_NUMBER; weightIdx+=2, kernelIdx+=4) {
    for (int weightIdx=0; weightIdx<PADDED_KERNEL_NUMBER; weightIdx++) {
      int kernelIdx = weightIdx << 1;
    #pragma omp parallel for firstprivate(imageSize)
    for (int i=0; i < imageSize; i++) {
      field_sse[i] = _mm_setzero_pd(); //field_sse[i][0, 1, 2, 3] = 0
    }
    
    // Create FOUR halo-lookuptables, and re-arrage the memory layout of the lookup tables for SSE access 
    // (one sse for 4 lookup tables in continous memory locations)
    double* fourTables = (double*) malloc(sizeof(double) * haloTotalSize * 2);
    __m128d* haloTable_sse = (__m128d*) fourTables;
    int h = min(TABLE_SIZE, haloHeight);
    int w = min(TABLE_SIZE, haloWidth);
    // Set the original values
    omp_set_num_threads(NUM_THREADS);
    #pragma omp parallel for firstprivate(h, w, kernelIdx, haloWidth)
    for (int y = 0; y < h; ++y)
      for (int x = 0; x < w; ++x)
        haloTable_sse[y*haloWidth+x] = _mm_set_pd(tableArray[kernelIdx+1][y*TABLE_SIZE+x], tableArray[kernelIdx][y*TABLE_SIZE+x]);
    // Set boundary values to halo
    #pragma omp parallel for firstprivate(h, haloWidth, kernelIdx)
    for (int y = 0; y < h; ++y) {
      for (int x = w; x < haloWidth; ++x)
        haloTable_sse[y*haloWidth+x] = _mm_set_pd(tableArray[kernelIdx+1][y*TABLE_SIZE+TABLE_SIZE-1], tableArray[kernelIdx][y*TABLE_SIZE+TABLE_SIZE-1]);
    }   
    #pragma omp parallel for firstprivate(haloHeight, w, haloWidth, kernelIdx)
    for (int y = h; y < haloHeight; ++y) {
      for (int x = 0; x < w; ++x)
        haloTable_sse[y*haloWidth+x] = _mm_set_pd(tableArray[kernelIdx+1][(TABLE_SIZE-1)*TABLE_SIZE+x], tableArray[kernelIdx][(TABLE_SIZE-1)*TABLE_SIZE+x]);
      for (int x = w; x < haloWidth; ++x)
        haloTable_sse[y*haloWidth+x] = _mm_set_pd(tableArray[kernelIdx+1][(TABLE_SIZE-1)*TABLE_SIZE+TABLE_SIZE-1], tableArray[kernelIdx][(TABLE_SIZE-1)*TABLE_SIZE+TABLE_SIZE-1]);
    }

    for (int i = 0; i < rectNum; i++) {
      // Note that all polygons are inside the left and bottom boundary of the
      // aerial image, but some of them exceed the right and top boundary.
      Rectangle polygonImage;
      polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom)/GRID_SIZE;
      polygonImage.top    = (polygonArray[i].top    - boundBox.bottom)/GRID_SIZE;
      polygonImage.left   = (polygonArray[i].left   - boundBox.left)  /GRID_SIZE;
      polygonImage.right  = (polygonArray[i].right  - boundBox.left)  /GRID_SIZE;
      // Ignore the polygon that is totally outside the aerial image
      if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
        continue;
      polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
      polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

      pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
      corners[0] = make_pair(polygonImage.right, polygonImage.top);
      corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
      corners[2] = make_pair(polygonImage.left, polygonImage.top);
      corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

      Rectangle impactRegions[2]; 
      impactRegions[1].bottom = impactRegions[0].bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
      impactRegions[1].left = impactRegions[0].left = max(polygonImage.left - KERNEL_RADIUS, 0);
      impactRegions[0].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
      impactRegions[0].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);
      impactRegions[1].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
      impactRegions[1].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);

      int j = impactRegions[0].bottom;
      int k = impactRegions[0].left;
      int rely[4], relx[4];
      for (int c = 0; c < 4; ++c) {
        rely[c] = corners[c].second - (impactRegions[0].bottom - TABLE_RADIUS) - 1; 
        relx[c] = corners[c].first - (impactRegions[0].left - TABLE_RADIUS) - 1;
      }
      int xSteps1 = impactRegions[1].right - impactRegions[0].left;
      int xSteps2 = impactRegions[0].right - impactRegions[0].left;
      int ySteps1 = impactRegions[1].top - impactRegions[0].bottom;
      int ySteps2 = impactRegions[0].top - impactRegions[0].bottom;
      //double ompComputeTimer = omp_get_wtime();
      omp_set_num_threads(min(ySteps2, NUM_THREADS));
      #pragma omp parallel for firstprivate(ySteps1, ySteps2, xSteps1, xSteps2, relx, rely, boundwidth, haloWidth, j, k)
      for (int ys = 0; ys < ySteps2; ++ys) {
        if (ys < ySteps1) {
          for (int xs = 0; xs < xSteps1; ++xs) {
            __m128d fValue = field_sse[(j+ys)*boundwidth+(k+xs)];
            for (int c = 2; c < 4; ++c) {
              fValue = _mm_sub_pd(fValue, haloTable_sse[(rely[c] - ys) * haloWidth + (relx[c] - xs)]);
            }
            for (int c = 0; c < 2; ++c) {
              fValue = _mm_add_pd(fValue, haloTable_sse[(rely[c] - ys) * haloWidth + (relx[c] - xs)]);
            }
            field_sse[(j+ys)*boundwidth+(k+xs)] = fValue;
          }
          for (int xs = xSteps1; xs < xSteps2; ++xs) {
            __m128d fValue = field_sse[(j+ys)*boundwidth+(k+xs)];
            fValue = _mm_sub_pd(fValue, haloTable_sse[(rely[3] - ys) * haloWidth + (relx[3] - xs)]);
            fValue = _mm_add_pd(fValue, haloTable_sse[(rely[0] - ys) * haloWidth + (relx[0] - xs)]);
            field_sse[(j+ys)*boundwidth+(k+xs)] = fValue;
          }
        } else {
          for (int xs = 0; xs < xSteps1; ++xs) {
            __m128d fValue = field_sse[(j+ys)*boundwidth+(k+xs)];
            fValue = _mm_sub_pd(fValue, haloTable_sse[(rely[2] - ys) * haloWidth + (relx[2] - xs)]);
            fValue = _mm_add_pd(fValue, haloTable_sse[(rely[0] - ys) * haloWidth + (relx[0] - xs)]);
            field_sse[(j+ys)*boundwidth+(k+xs)] = fValue;
          }
          for (int xs = xSteps1; xs < xSteps2; ++xs) {
            field_sse[(j+ys) * boundwidth + (k+xs)] = _mm_add_pd(field_sse[(j+ys) * boundwidth + (k+xs)], haloTable_sse[(rely[0] - ys) * haloWidth + (relx[0] - xs)]);
          }
        }
      }
      //ompCompute += (omp_get_wtime()-ompComputeTimer);
#if 0
      omp_set_num_threads(min(ySteps1, NUM_THREADS));
      #pragma omp parallel for
      for (int ys = 0; ys < ySteps1; ++ys) 
        for (int xs = 0; xs < xSteps1; ++xs) {
          __m128 fValue = field_sse[(j+ys)*boundwidth+(k+xs)];
          for (int c = 2; c < 4; ++c) {
            fValue = _mm_sub_ps(fValue, haloTable_sse[(rely[c] - ys) * haloWidth + (relx[c] - xs)]);
          }
          for (int c = 0; c < 2; ++c) {
            fValue = _mm_add_ps(fValue, haloTable_sse[(rely[c] - ys) * haloWidth + (relx[c] - xs)]);
          }
          field_sse[(j+ys)*boundwidth+(k+xs)] = fValue;
        }
      omp_set_num_threads(min(ySteps2-ySteps1, NUM_THREADS));
      #pragma omp parallel for
      for (int ys = ySteps1; ys < ySteps2; ++ys)
        for (int xs = 0; xs < xSteps1; ++xs) {
          __m128 fValue = field_sse[(j+ys)*boundwidth+(k+xs)];
          fValue = _mm_sub_ps(fValue, haloTable_sse[(rely[2] - ys) * haloWidth + (relx[2] - xs)]);
          fValue = _mm_add_ps(fValue, haloTable_sse[(rely[0] - ys) * haloWidth + (relx[0] - xs)]);
          field_sse[(j+ys)*boundwidth+(k+xs)] = fValue;
        }
      omp_set_num_threads(min(ySteps1, NUM_THREADS));
      #pragma omp parallel for
      for (int ys = 0; ys < ySteps1; ++ys)
        for (int xs = xSteps1; xs < xSteps2; ++xs) {
          __m128 fValue = field_sse[(j+ys)*boundwidth+(k+xs)];
          fValue = _mm_sub_ps(fValue, haloTable_sse[(rely[3] - ys) * haloWidth + (relx[3] - xs)]);
          fValue = _mm_add_ps(fValue, haloTable_sse[(rely[0] - ys) * haloWidth + (relx[0] - xs)]);
          field_sse[(j+ys)*boundwidth+(k+xs)] = fValue;
        }
      omp_set_num_threads(min(ySteps2-ySteps1, NUM_THREADS));
      #pragma omp parallel for
      for (int ys = ySteps1; ys < ySteps2; ++ys) 
        for (int xs = xSteps1; xs < xSteps2; ++xs) {
          field_sse[(j+ys) * boundwidth + (k+xs)] = _mm_add_ps(field_sse[(j+ys) * boundwidth + (k+xs)], haloTable_sse[(rely[0] - ys) * haloWidth + (relx[0] - xs)]);
        }
#endif
    }

    __m128d weight = _mm_set_pd(weightArray[weightIdx], weightArray[weightIdx]);
    const int mask = 0xf1;
    //#pragma omp critical 
    #pragma omp parallel for firstprivate(weight, boundwidth, boundheight)
    for (int i = 0;i < boundwidth * boundheight; i++) {
      __m128d tmp = _mm_mul_pd(field_sse[i], weight);
      __m128d sum = _mm_dp_pd(field_sse[i], tmp, mask);
      __m128d cur = _mm_load_sd(sseImage+i);
      sum = _mm_add_sd(cur, sum);
      _mm_store_sd(sseImage+i, sum);
    }
    free(fourTables);
  }
  free(field);
  }

  //ompTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
  ompTime = omp_get_wtime() - ompTimer;
  //cout << "ompCompute: " << ompCompute << "\n";
}
