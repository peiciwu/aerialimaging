#include <complex>

using namespace std;

void fft(int, complex<double>*);
void shuffle(int, complex<double>*);
void swap(complex<double>*, complex<double>*);
int bitrev(int, int);
void dftmerge(int, complex<double>*);
complex<double> conjugate(complex<double>);
void ifft(int, complex<double> *);
void fft2(int, int, complex<double> *);
void ifft2(int, int, complex<double> *);
