#include "cudaCommon_Tan.cuh"

#include <ctime>
#include <cstdio>
#include <iostream>
using namespace std;

#define BLOCK_SIZE_LUT 256
#define BLOCK_SIZE_COMBINE 256

#define LUT_SIZE (TABLE_SIZE * TABLE_SIZE)

__constant__ float weights[KERNEL_NUMBER];

__global__ void kernel_combine_image_3(float * d_image_complex, float * d_image, INT4 image_size) {
  int pix_idx = threadIdx.x + blockIdx.x * blockDim.x;
  if (pix_idx >= image_size) {
    return;
  }

  float real_part;
  float imaginary_part;
  float intensity = .0;

  int i;
  for (i = 0; i < KERNEL_NUMBER; ++i) {
    int imaginary_idx;
    int real_idx;
    get_r_i_idx(real_idx, imaginary_idx, i, image_size, pix_idx);
    imaginary_part = d_image_complex[imaginary_idx];
    real_part = d_image_complex[real_idx];
    intensity += weights[i] * (real_part * real_part + imaginary_part * imaginary_part);
  }

  d_image[pix_idx] = intensity;
}


__global__ void kernel_compute_image_from_LUT_3(float* d_image_complex, INT4 image_width, INT4 image_height, INT4 image_size, float* d_LUT, Rectangle * d_rects, INT4 rectNum, Rectangle boundBox) {

  // compute the range affected by the rectangle using only 1 thread
  __shared__ int pix_per_thread;
  __shared__ int remain_pix;
  __shared__ int range_width; 
  __shared__ Rectangle rect;
  __shared__ Rectangle range;
  __shared__ int base;

  if (threadIdx.x == 0) {
    fetch_rect(rect, boundBox, blockIdx.x, d_rects);
    compute_range(range, rect, image_width, image_height);

    int range_height;
    int total_pix;
    range_width = range.right - range.left;
    range_height = range.top - range.bottom;
    total_pix = range_width * range_height;
    pix_per_thread = total_pix / BLOCK_SIZE_LUT;
    remain_pix = total_pix - BLOCK_SIZE_LUT * pix_per_thread;
    base = pix_per_thread * BLOCK_SIZE_LUT;
  }

  __syncthreads();

  for (int iter = 0; iter < pix_per_thread; ++iter) {
    int r, c;
    get_rc(r, c, range, range_width, iter * BLOCK_SIZE_LUT + threadIdx.x);
    accumulate_one_pix(r, c, rect, image_width, image_size, d_image_complex, d_LUT);
  }

  if (threadIdx.x < remain_pix) {
    int r, c;
    get_rc(r, c, range, range_width, base + threadIdx.x);
    accumulate_one_pix(r, c, rect, image_width, image_size, d_image_complex, d_LUT);
  }
}

void cudaPrintImage_LUT_3(Rectangle * polygonArray,
			  INT4 rectNum,
			  float * weightArray,
			  float tableArray[KERNEL_NUMBER*2][LUT_SIZE],
			  Rectangle & boundBox,
			  float * cudaImage,
			  float & cudaTime) {
  /* timing */
  double runtime = .0;

  /* allocate device space */
  Rectangle * d_rects; // all rectangles
  float * d_image_complex;  // real and imaginary part of the image
  float * d_image; // output image on device
  float * d_LUT; // lookup table, complex 

  int image_height = (boundBox.top - boundBox.bottom) / GRID_SIZE;
  int image_width = (boundBox.right - boundBox.left) / GRID_SIZE;
  int image_size = image_height * image_width;

  cout << "allocating device memory..." << endl;

  cudaMalloc((void**)& d_rects, rectNum * sizeof(Rectangle));
  cudaMalloc((void**)& d_image_complex, 2 * KERNEL_NUMBER * image_height * image_width * sizeof(float));
  cudaMalloc((void**)& d_image, image_size * sizeof(float));
  cudaMalloc((void**)& d_LUT, 2 * KERNEL_NUMBER * LUT_SIZE * sizeof(float));

  // set up grid dim and block dim
  dim3 dimGrid_LUT(rectNum);
  dim3 dimBlock_LUT(BLOCK_SIZE_LUT);

  dim3 dimBlock_combine(BLOCK_SIZE_COMBINE);
  dim3 dimGrid_combine(image_size / BLOCK_SIZE_COMBINE + ((image_size % BLOCK_SIZE_COMBINE == 0) ? 0 : 1));

  // copy rectangles
  cudaMemcpy(d_rects, polygonArray, rectNum * sizeof(Rectangle), cudaMemcpyHostToDevice);

  cudaMemset(d_image, 0, image_height * image_width * sizeof(float));

  cout << "start kernel..." << endl;
  // run through all the kernels

  // copy data
  cudaMemcpy(d_LUT, tableArray, 2 * KERNEL_NUMBER * LUT_SIZE * sizeof(float), cudaMemcpyHostToDevice);

//    cudaMemcpy(d_LUT_r, tableArray[i * 2 + 1], TABLE_SIZE * TABLE_SIZE * sizeof(float), cudaMemcpyHostToDevice);
//    cudaMemcpy(d_LUT_i, tableArray[i * 2], TABLE_SIZE * TABLE_SIZE * sizeof(float), cudaMemcpyHostToDevice);

  // timing
  cudaThreadSynchronize();
  clock_t start_time = clock();

  //copy const mem
  cudaMemcpyToSymbol(weights, weightArray, KERNEL_NUMBER * sizeof(float), 0, cudaMemcpyHostToDevice);

  // initialize image
  cudaMemset(d_image_complex, 0, 2 * KERNEL_NUMBER * image_height * image_width * sizeof(float));

  // call kernel
  kernel_compute_image_from_LUT_3<<<dimGrid_LUT, dimBlock_LUT>>>(d_image_complex, image_width, image_height, image_size, d_LUT, d_rects, rectNum, boundBox);

  kernel_combine_image_3<<<dimGrid_combine, dimBlock_combine>>>(d_image_complex, d_image, image_size);

  // timing
  cudaThreadSynchronize();
  runtime += clock() - start_time;

  // copy result image back
  cudaMemcpy(cudaImage, d_image, image_height * image_width * sizeof(float), cudaMemcpyDeviceToHost);

  //DEBUG
  //cudaMemcpy(cudaImage, d_image_i, image_height * image_width * sizeof(float), cudaMemcpyDeviceToHost);

  cudaFree(d_rects);
  cudaFree(d_image_complex);
  cudaFree(d_image);
  cudaFree(d_LUT);

  // in the end report the run time by modifying the value of cudaTime;
  cudaTime = runtime / (double) CLOCKS_PER_SEC;
}




