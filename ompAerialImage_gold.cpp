#include "ompAerialImage.h"
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <omp.h>

using namespace std;

//static bool overlapping2(Rectangle& area, int x, int y, Rectangle polygonImage);
bool overlapping2(Rectangle& area, int x, int y, Rectangle polygonImage);

void ompPrintImage_gold(Rectangle* polygonArray,
	INT4 rectNum,
	float* weightArray,
	DTYPE** tableArray,
	DTYPE* &ompImage,
	float &ompTime,
	Rectangle boundBox)
{
    // your code starts here
    omp_set_num_threads(NUM_THREADS);
    double ompTimer = omp_get_wtime();
    //clock_t startTime = clock();

    int boundwidth = (boundBox.right-boundBox.left) / GRID_SIZE;
    int boundheight = (boundBox.top-boundBox.bottom) / GRID_SIZE;

    for (int i=0;i<boundwidth*boundheight;i++) {
      ompImage[i] = 0;
    }

    int kernelIdx = 0;

    //DTYPE* image[omp_get_num_threads()];

    #pragma omp parallel
    {
      //cout << "The total # of threads is " << omp_get_num_threads() << endl;
      // Each thread owns its copy of field_real and field_imag. 
      DTYPE* field_real = (DTYPE *) malloc(sizeof(DTYPE)*boundwidth*boundheight);
      DTYPE* field_imag = (DTYPE *) malloc(sizeof(DTYPE)*boundwidth*boundheight);

#if 0
      image[omp_get_thread_num()] = (DTYPE *) malloc(sizeof(DTYPE)*boundwidth*boundheight);
      for (int i=0;i<boundwidth*boundheight;i++) {
        image[omp_get_thread_num()][i] = 0;
      }
#endif

      Rectangle polygonImage;
      Rectangle overlapRegion;

      //#pragma omp for schedule(static, 4) //private(polygonImage, overlapRegion)
      #pragma omp for
      for (int weightIdx = 0; weightIdx < KERNEL_NUMBER; weightIdx++) {
      //for (int weightIdx = omp_get_thread_num(); weightIdx < KERNEL_NUMBER; weightIdx+= omp_get_num_threads()) {
#if 0
        if (omp_get_thread_num() == 0)
          cout << "id: 0, weightIdx: " << weightIdx << endl;
        if (omp_get_thread_num() == 1)
          cout << "id: 1, weightIdx: " << weightIdx << endl;
        if (omp_get_thread_num() == 2)
          cout << "id: 2, weightIdx: " << weightIdx << endl;
        if (omp_get_thread_num() == 3)
          cout << "id: 3, weightIdx: " << weightIdx << endl;
#endif
        int kernelIdx = weightIdx << 1;
        for (int i = 0; i < boundwidth*boundheight;++i) {
          field_real[i] = 0;
          field_imag[i] = 0;
        }
	for (int i=0;i < rectNum;i++){
	    polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom) >> 2;
	    polygonImage.top    = (polygonArray[i].top    - boundBox.bottom) >> 2;
	    polygonImage.left   = (polygonArray[i].left   - boundBox.left)  >> 2;
	    polygonImage.right  = (polygonArray[i].right  - boundBox.left)  >> 2;

	    // for the topright corner
	    for (int j = max(polygonImage.bottom-KERNEL_RADIUS, 0); j < min(polygonImage.top+KERNEL_RADIUS, boundheight);++j)
		for (int k = max(polygonImage.left-KERNEL_RADIUS,0); k < min(polygonImage.right+KERNEL_RADIUS, boundwidth);++k)
		    {
		    //if(overlapping2(overlapRegion, k, j, polygonImage))
                        overlapping2(overlapRegion, k, j, polygonImage);
			field_real[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
			field_imag[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
			if (overlapRegion.left!=-1)
			{
			    field_real[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
			    field_imag[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
			}
			if (overlapRegion.bottom!=-1)
			{
			    field_real[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
			    field_imag[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
			}
			if (overlapRegion.bottom != -1 && overlapRegion.left != -1)
			{
			    field_real[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
			    field_imag[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
			}
		    }
	}

        #pragma omp critical
	for (int i=0;i<boundwidth*boundheight;i++) {
            //#pragma omp atomic
            //#pragma omp critical
	    ompImage[i]+=((DTYPE)weightArray[weightIdx])*(field_real[i]*field_real[i]+field_imag[i]*field_imag[i]);
	}
#if 0
        for (int i = 0, idx = omp_get_thread_num(); i < boundwidth*boundheight; ++i)
          image[idx][i]+=((DTYPE)weightArray[weightIdx])*(field_real[i]*field_real[i]+field_imag[i]*field_imag[i]);
#endif
      }

#if 0
      //#pragma omp critical
      for (int i = 0; i < boundwidth * boundheight; ++i)
        ompImage[i] += image[omp_get_thread_num()][i];
#endif

      free(field_real);
      free(field_imag);

      //free(image[omp_get_thread_num()]);
    }

#if 0
    for (int i = 0; i < boundwidth * boundheight; ++i)
      for (int j = 0; j < omp_get_num_threads(); ++j)
        ompImage[i] += image[j][i];

    //#pragma omp parallel for
    for (int i = 0; i < omp_get_num_threads(); ++i)
      free(image[i]);
#endif

    //ompTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
    ompTime = omp_get_wtime() - ompTimer;
}

bool overlapping2(Rectangle& area, int x, int y, Rectangle polygonImage)
{
    int tableTop, tableBottom, tableLeft, tableRight;
    tableTop    = y+TABLE_RADIUS+1;
    tableBottom = y-TABLE_RADIUS;
    tableLeft   = x-TABLE_RADIUS;
    tableRight  = x+TABLE_RADIUS+1;
    
#if 0
    // FIXME. This condition is NOP.
    if (polygonImage.top <= tableBottom ||		// modified here
	    polygonImage.bottom >= tableTop ||
	    polygonImage.left >= tableRight ||
	    polygonImage.right <= tableLeft)		// modified here
	return false;
#endif

    area.top = min(tableTop, polygonImage.top)-tableBottom-1;     // modified here
    area.bottom = max(tableBottom, polygonImage.bottom)-tableBottom-1;    // modified here
    area.left = max(tableLeft, polygonImage.left)-tableLeft-1;	    // modified here
    area.right = min(tableRight, polygonImage.right)-tableLeft-1;	// modified here

    return true;
}


