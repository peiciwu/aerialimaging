#include <iostream>
#include <time.h>
#include <complex>
#include <xmmintrin.h>
#include <emmintrin.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifndef __APPLE__
#include <malloc.h>
#endif

#include "sseAerialImage.h"

using namespace std;

static __m128 zeroAllMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0x0));
static __m128 zeroLeastMask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0xFFffFFff, 0x0));
static __m128 zeroLeast2Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0x0, 0x0));
static __m128 zeroLeast3Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0x0, 0x0, 0x0));
static __m128 zeroMostMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0xFFffFFff, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost2Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost3Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0xFFffFFff));
static __m128 zeroLeastMasks[3] = {zeroLeastMask, zeroLeast2Mask, zeroLeast3Mask};
static __m128 zeroMostMasks[3] = {zeroMostMask, zeroMost2Mask, zeroMost3Mask};
static __m128 leaveLeastMasks[4] = {zeroAllMask, zeroMost3Mask, zeroMost2Mask, zeroMostMask};
static __m128 leaveMostMasks[4] = {zeroAllMask, zeroLeast3Mask, zeroLeast2Mask, zeroLeastMask};

// /brief
// Each SSE instruction simultaneously updates several pixels in the same image.
// The framework is similar to "gold_continuous", except that "gold_continuous"
// only updates 1 pixel one time.
// Current implementation is to update 4 pixels at the same time. In order to
// use sse instructions, we have 
// 1. The size of sseImage MUST be a multiple of 4.
//  - Here, the memory padding/allocatio of sseImage is done inside the caller function,
//    i.e. main function, paddedImageSize is the size after padding
// 2. The size of field image MUST be a multiple of 4.
//  - The allocation is the same as sseImage
// 3. When updating field_sse by each row of haloTable, we have the following two
// situations:
//  3.1. If start idx and end idx is the start of a multiple of 4 and end of
//  multiple of 4, do nothing
//  3.2. If NOT,
//      a. (CURRENT IMPLEMENTATION), manually update the remainder at
//      the first and last slot
//      b. padding..........(has to change the allocation of haloTable)
// 3. The size of haloTable MUST be a multiple of 4
//  - pad into a multiple of 4 by inserting to the end of the array
// FIXME. According to impactRegion, allocate smaller haloTable???? But we
// cannot allocate a single haloTable that works on all rectangels...
void ssePrintImage_2(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weightArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox,
                   int paddedImageSize)
{
  clock_t startTime = clock();

  int boundwidth = (boundBox.right - boundBox.left) >> 2; // divide by GRID_SIZE(4)
  int boundheight = (boundBox.top - boundBox.bottom) >> 2;
  // Find the left/right/top/bottom-most boundary among all rectangels.
  Rectangle rectBound;
  rectBound.bottom = rectBound.left = 0; //(bottom-RADIUS) is always <= 0
  //rectBound.top = rectBound.right = INT_MIN;
  int rectWidth = 0;
  int rectHeight = 0;
  for (int i = 0; i < rectNum; ++i) {
    if ((polygonArray[i].right - polygonArray[i].left) > rectWidth)
      rectWidth = polygonArray[i].right - polygonArray[i].left;
    if ((polygonArray[i].top - polygonArray[i].bottom) > rectHeight)
      rectHeight = polygonArray[i].top - polygonArray[i].bottom;
  }
  rectWidth = rectWidth >> 2;
  rectHeight = rectHeight >> 2;

#if 0
  rectBound.top = (rectBound.top - boundBox.bottom) >> 2;
  rectBound.right = (rectBound.right - boundBox.left) >> 2;
  cout << "top: " << rectBound.top << ", height: " << rectHeight << endl;
  cout << "right: " << rectBound.right << ", width: " << rectWidth << endl;
  rectBound.top = min(boundheight + TABLE_RADIUS + 1, rectBound.top) + TABLE_RADIUS;
  rectBound.right = min(boundwidth + TABLE_RADIUS + 1, rectBound.right) + TABLE_RADIUS;
  int haloWidth = rectBound.right;
  int haloHeight = rectBound.top;
#endif
  rectBound.top = min(boundheight, rectHeight) + TABLE_SIZE;
  if (rectBound.top < TABLE_SIZE)
    rectBound.top = TABLE_SIZE;
  rectBound.right = min(boundwidth, rectWidth) + TABLE_SIZE;
  int haloWidth = rectBound.right;
  int haloHeight = rectBound.top;
  // We want haloWidth%4 == boundwidth%4
  haloWidth += (boundwidth%4 - haloWidth%4 + 4)%4;
  int haloTotalSize = haloWidth * haloHeight;

  __m128* image_sse = (__m128*) sseImage; //stored in reverse order.....
  for (int s = 0, i = 0; s < paddedImageSize; s += 4, i++) {
    image_sse[i] = _mm_setzero_ps();
  }

  float* field; //is an array of paddedImageSizee
//#ifdef __APPLE__
  field = (float *) malloc(sizeof(float) * paddedImageSize);
  __m128* field_sse = (__m128*) field; //stored in reverse order.....

  Rectangle polygonImage;

  for (int tableIdx = 0; tableIdx < TABLE_NUMBER; tableIdx++) {
    int weightIdx = tableIdx >> 1;

    for (int s = 0, i = 0; s < paddedImageSize; s += 4, i++) {
      field_sse[i] = _mm_setzero_ps(); 
    }

    // Create ONE halo-lookuptable that works for all rectangles. LUT is extended into
    // imageSize by adding halo to LUT. Halo is assigned boundary values.
    // Besides, we want haloWidth%4 is equal to boundwidth%4. So, although
    // haloWidth = boundwidth + TABLE_SIZE is enough, +3(=0+4-257%4) is necessary.
    // FIXME. Does haloHeight also need '+3'?
    float* haloTable[4]; //0: no shift, 1: shift right one index, 2: shift right two, 3: shift right three
    // Allocate and initialize haloTable[0]. The origianl haloTable but is
    // reversed.
//#ifdef __APPLE__
    haloTable[0] = (float*) malloc(sizeof(float) * haloTotalSize);
    int h = min(TABLE_SIZE, haloHeight);
    int w = min(TABLE_SIZE, haloWidth);
    // For the origianl rows >= TABLE_SIZE, now they start from row 0
    if (h != haloHeight) {
      float* upperBoundaryRow = (float*) malloc(sizeof(float)*haloWidth);
      // Set corner boundary value first, i.e. x >= TABLE_SIZE && y >=
      // TABLE_SIZE
      fill_n(upperBoundaryRow, haloWidth - w, tableArray[tableIdx][TABLE_SIZE*TABLE_SIZE-1]);
      // Reverse the upper boundary table row
      for (int x = haloWidth-w, tx = w-1; x < haloWidth; ++x, --tx) 
        upperBoundaryRow[x] = tableArray[tableIdx][(TABLE_SIZE-1)*TABLE_SIZE+tx];
      for (int y = 0; y < haloHeight-h; ++y) 
        memcpy(haloTable[0]+y*haloWidth, upperBoundaryRow, sizeof(float)*haloWidth);
    }
    // For the original rows from 0 to TABLE_SIZE-1, now they start from
    // haloHeight-h
    // (tx, ty) is for the reversed table index
    for (int y = haloHeight-h, ty = h - 1; y < haloHeight; ++y, --ty) {
      // Set boundary values first, i.e. x >= TABLE_SIZE
      fill_n(haloTable[0]+y*haloWidth, haloWidth - w, tableArray[tableIdx][ty*TABLE_SIZE+TABLE_SIZE-1]);
      for (int x = haloWidth - w, tx = w - 1; x < haloWidth; ++x, --tx)
        haloTable[0][y*haloWidth + x] = tableArray[tableIdx][ty*TABLE_SIZE+tx];
    }
    // Allocate and initialize other haloTables
    for (int i = 1; i < 4; ++i) {
//#ifdef __APPLE__
      haloTable[i] = (float*) malloc(sizeof(float) * (haloTotalSize + i));
    }
    for (int i = 1; i < 4; ++i)
      memset(haloTable[i], 0, sizeof(float)*i);
    for (int i = 1; i < 4; ++i) 
      memcpy(haloTable[i]+i, haloTable[0], sizeof(float)*haloTotalSize);

    __m128* table_sse[4];
    for (int i = 0; i < 4; ++i)
      table_sse[i] = (__m128*) haloTable[i];
      
    for (int i = 0; i < rectNum; i++) {
      // Note that all polygons are inside the left and bottom boundary of the
      // aerial image, but some of them exceed the right and top boundary.
      polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom) >> 2;
      polygonImage.top    = (polygonArray[i].top    - boundBox.bottom) >> 2;
      polygonImage.left   = (polygonArray[i].left   - boundBox.left)   >> 2;
      polygonImage.right  = (polygonArray[i].right  - boundBox.left)   >> 2;
      // Ignore the polygon that is totally outside the aerial image
      if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
        continue;
      // The polygon's top/right boundary over some limit is meaningless, i.e.
      // LUT won't be used to check those pixels. The limit is as follows:
      // FIXME. NOT SURE....
      // (boundheight-1) (the right most pixel in the aerial image) +
      // TABLE_RADIUS + 1.
      polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
      polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

      pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
      corners[0] = make_pair(polygonImage.right, polygonImage.top);
      corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
      corners[2] = make_pair(polygonImage.left, polygonImage.top);
      corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

      Rectangle impactRegions[4]; // one impactRegion per corner
      impactRegions[0].bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
      impactRegions[0].left = max(polygonImage.left - KERNEL_RADIUS, 0);
      impactRegions[0].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
      impactRegions[0].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);
      for (int i = 1; i < 4; ++i) {
        impactRegions[i].bottom = impactRegions[0].bottom;
        impactRegions[i].left = impactRegions[0].left;
      }
      impactRegions[1].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
      impactRegions[1].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);
      impactRegions[2].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
      impactRegions[2].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);
      impactRegions[3].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
      impactRegions[3].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);

      // top-left and bottom-right corners
      for (int c = 2; c < 4; ++c) {
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegions[c].bottom;
        int k = impactRegions[c].left;
        int rely = corners[c].second - (impactRegions[c].bottom - TABLE_RADIUS) - 1; 
        int relx = corners[c].first - (impactRegions[c].left - TABLE_RADIUS) - 1;
        int ySteps = min(impactRegions[c].top - impactRegions[c].bottom, rely+1);
        int xSteps = min(impactRegions[c].right - impactRegions[c].left, relx+1);
        if (xSteps <= 0 || ySteps <= 0)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
        for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
          int start_field = (fj + k); // start index of field 
          int start_field_sse = (fj + k) >> 2; // start index of field_sse
          int end_field_sse = (fj + k + xSteps - 1) >> 2;
          int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse
          
          int fi = start_field_sse, ti = start_table_sse;
          // First slot. Undo the paddings in the first slot
          field_sse[fi] = _mm_sub_ps(field_sse[fi], table_sse[numShifted][ti]);
          __m128 sub = _mm_and_ps(table_sse[numShifted][ti], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[fi] = _mm_add_ps(field_sse[fi], sub);
          ++fi, ++ti;
          // The remaining slots.
          for (; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_sub_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the last slot
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_add_ps(field_sse[end_field_sse], sub);

          fj += boundwidth;
          ty--;
        }
      }
      // top-right and bottom-left corners
      for (int c = 0; c < 2; ++c) {
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegions[c].bottom;
        int k = impactRegions[c].left;
        int rely = corners[c].second - (impactRegions[c].bottom - TABLE_RADIUS) - 1; 
        int relx = corners[c].first - (impactRegions[c].left - TABLE_RADIUS) - 1;
        int ySteps = min(impactRegions[c].top - impactRegions[c].bottom, rely+1);
        int xSteps = min(impactRegions[c].right - impactRegions[c].left, relx+1);
        if (xSteps <= 0 || ySteps <= 0)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
        for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
          int start_field = (fj + k); // start index of field 
          int start_field_sse = (fj + k) >> 2; // start index of field_sse
          int end_field_sse = (fj + k + xSteps - 1) >> 2;
          int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse

#if 0
          if (start_field_sse == end_field_sse) {
            // Only one
            for (int fi = start_field, ti = start_table; fi < start_field + xSteps; ++fi, ti++)
              field[fi] += haloTable[numShifted][ti];
          }
          else
          {
            int fi = start_field_sse;
            int ti = start_table_sse;
            // Zero the paddedings in the first slot and add back
            if (numFirstPadded_field > 0) {
              __m128 first_slot = _mm_and_ps(table_sse[numShifted][start_table_sse], zeroLeastMasks[numFirstPadded_field-1]);
              field_sse[start_field_sse] = _mm_add_ps(field_sse[start_field_sse], first_slot);
              fi++;
              ti++;
            }
            // Zero the paddings in the last slot and add back
            if (numLastPadded_field > 0 && fi <= end_field_sse) {
              int end_table_sse = (start_table + xSteps - 1) / 4;
              __m128 last_slot = _mm_and_ps(table_sse[numShifted][end_table_sse], zeroMostMasks[numLastPadded_field-1]);
              field_sse[end_field_sse] = _mm_add_ps(field_sse[end_field_sse], last_slot);
              end_field_sse--;
            }
            for (; fi <= end_field_sse; ++fi, ++ti)
              field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          }
#endif
          int fi = start_field_sse, ti = start_table_sse;
          // First slot. Undo the paddings in the first slot
          field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          __m128 sub = _mm_and_ps(table_sse[numShifted][ti], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[fi] = _mm_sub_ps(field_sse[fi], sub);
          ++fi, ++ti;
          // The remaining slots.
          for (; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the last slot
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);

          fj += boundwidth;
          ty--;
        }
      }
    }
    for (int i = 0; i < 4; ++i)
      free(haloTable[i]);
    int sz_sse = paddedImageSize/4;
    for (int i = 0; i < sz_sse; i++) {
      __m128 square = _mm_mul_ps(field_sse[i], field_sse[i]);
      __m128 weight_sse = _mm_set_ps(weightArray[weightIdx], weightArray[weightIdx], weightArray[weightIdx], weightArray[weightIdx]);
      __m128 tmp = _mm_mul_ps(square, weight_sse);
      image_sse[i] = _mm_add_ps(image_sse[i], tmp);
    }
  }
  free(field);

  sseTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
}

#if 0
// DEBUG!!!!!!!!!!
void ssePrintImage_2(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weightArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox,
                   int paddedImageSize)
{
  clock_t startTime = clock();

  int boundwidth = (boundBox.right - boundBox.left) >> 2; // divide by GRID_SIZE(4)
  int boundheight = (boundBox.top - boundBox.bottom) >> 2;
  // Find the left/right/top/bottom-most boundary among all rectangels.
  Rectangle rectBound;
  rectBound.bottom = rectBound.left = 0; //(bottom-RADIUS) is always <= 0
  //rectBound.top = rectBound.right = INT_MIN;
  int rectWidth = 0;
  int rectHeight = 0;
  for (int i = 0; i < rectNum; ++i) {
    if ((polygonArray[i].right - polygonArray[i].left) > rectWidth)
      rectWidth = polygonArray[i].right - polygonArray[i].left;
    if ((polygonArray[i].top - polygonArray[i].bottom) > rectHeight)
      rectHeight = polygonArray[i].top - polygonArray[i].bottom;
  }
  rectWidth = rectWidth >> 2;
  rectHeight = rectHeight >> 2;

  rectBound.top = min(boundheight, rectHeight) + TABLE_SIZE;
  if (rectBound.top < TABLE_SIZE)
    rectBound.top = TABLE_SIZE;
  rectBound.right = min(boundwidth, rectWidth) + TABLE_SIZE;
  int haloWidth = rectBound.right;
  int haloHeight = rectBound.top;
  // We want haloWidth%4 == boundwidth%4
  haloWidth += (boundwidth%4 - haloWidth%4 + 4)%4;
  int haloTotalSize = haloWidth * haloHeight;

  __m128* image_sse = (__m128*) sseImage; //stored in reverse order.....
  for (int s = 0, i = 0; s < paddedImageSize; s += 4, i++) {
    image_sse[i] = _mm_setzero_ps();
  }

  float* field; //is an array of paddedImageSizee
//#ifdef __APPLE__
  field = (float *) malloc(sizeof(float) * paddedImageSize);
  __m128* field_sse = (__m128*) field; //stored in reverse order.....

  Rectangle polygonImage;

  for (int tableIdx = 0; tableIdx < TABLE_NUMBER; tableIdx++) {
    int weightIdx = tableIdx >> 1;

    for (int s = 0, i = 0; s < paddedImageSize; s += 4, i++) {
      field_sse[i] = _mm_setzero_ps(); 
    }

    // Create ONE halo-lookuptable that works for all rectangles. LUT is extended into
    // imageSize by adding halo to LUT. Halo is assigned boundary values.
    // Besides, we want haloWidth%4 is equal to boundwidth%4. So, although
    // haloWidth = boundwidth + TABLE_SIZE is enough, +3(=0+4-257%4) is necessary.
    // FIXME. Does haloHeight also need '+3'?
    float* haloTable[4]; //0: no shift, 1: shift right one index, 2: shift right two, 3: shift right three
    // Allocate and initialize haloTable[0]. The origianl haloTable but is
    // reversed.
//#ifdef __APPLE__
    haloTable[0] = (float*) malloc(sizeof(float) * haloTotalSize);
    int h = min(TABLE_SIZE, haloHeight);
    int w = min(TABLE_SIZE, haloWidth);
    // For the origianl rows >= TABLE_SIZE, now they start from row 0
    if (h != haloHeight) {
      float* upperBoundaryRow = (float*) malloc(sizeof(float)*haloWidth);
      // Set corner boundary value first, i.e. x >= TABLE_SIZE && y >=
      // TABLE_SIZE
      fill_n(upperBoundaryRow, haloWidth - w, tableArray[tableIdx][TABLE_SIZE*TABLE_SIZE-1]);
      // Reverse the upper boundary table row
      for (int x = haloWidth-w, tx = w-1; x < haloWidth; ++x, --tx) 
        upperBoundaryRow[x] = tableArray[tableIdx][(TABLE_SIZE-1)*TABLE_SIZE+tx];
      for (int y = 0; y < haloHeight-h; ++y) 
        memcpy(haloTable[0]+y*haloWidth, upperBoundaryRow, sizeof(float)*haloWidth);
    }
    // For the original rows from 0 to TABLE_SIZE-1, now they start from
    // haloHeight-h
    // (tx, ty) is for the reversed table index
    for (int y = haloHeight-h, ty = h - 1; y < haloHeight; ++y, --ty) {
      // Set boundary values first, i.e. x >= TABLE_SIZE
      fill_n(haloTable[0]+y*haloWidth, haloWidth - w, tableArray[tableIdx][ty*TABLE_SIZE+TABLE_SIZE-1]);
      for (int x = haloWidth - w, tx = w - 1; x < haloWidth; ++x, --tx)
        haloTable[0][y*haloWidth + x] = tableArray[tableIdx][ty*TABLE_SIZE+tx];
    }
    // Allocate and initialize other haloTables
    for (int i = 1; i < 4; ++i) {
//#ifdef __APPLE__
      haloTable[i] = (float*) malloc(sizeof(float) * (haloTotalSize + i));
    }
    for (int i = 1; i < 4; ++i)
      memset(haloTable[i], 0, sizeof(float)*i);
    for (int i = 1; i < 4; ++i) 
      memcpy(haloTable[i]+i, haloTable[0], sizeof(float)*haloTotalSize);

    __m128* table_sse[4];
    for (int i = 0; i < 4; ++i)
      table_sse[i] = (__m128*) haloTable[i];
      
    for (int i = 0; i < rectNum; i++) {
      // Note that all polygons are inside the left and bottom boundary of the
      // aerial image, but some of them exceed the right and top boundary.
      polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom) >> 2;
      polygonImage.top    = (polygonArray[i].top    - boundBox.bottom) >> 2;
      polygonImage.left   = (polygonArray[i].left   - boundBox.left)   >> 2;
      polygonImage.right  = (polygonArray[i].right  - boundBox.left)   >> 2;
      // Ignore the polygon that is totally outside the aerial image
      if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
        continue;
      // The polygon's top/right boundary over some limit is meaningless, i.e.
      // LUT won't be used to check those pixels. The limit is as follows:
      // FIXME. NOT SURE....
      // (boundheight-1) (the right most pixel in the aerial image) +
      // TABLE_RADIUS + 1.
      polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
      polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

      pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
      corners[0] = make_pair(polygonImage.right, polygonImage.top);
      corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
      corners[2] = make_pair(polygonImage.left, polygonImage.top);
      corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

      Rectangle impactRegions[4]; // one impactRegion per corner
      impactRegions[0].bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
      impactRegions[0].left = max(polygonImage.left - KERNEL_RADIUS, 0);
      impactRegions[0].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
      impactRegions[0].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);
      for (int i = 1; i < 4; ++i) {
        impactRegions[i].bottom = impactRegions[0].bottom;
        impactRegions[i].left = impactRegions[0].left;
      }
      impactRegions[1].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
      impactRegions[1].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);
      impactRegions[2].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
      impactRegions[2].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);
      impactRegions[3].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
      impactRegions[3].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);

      // top-left and bottom-right corners
      for (int c = 2; c < 4; ++c) {
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegions[c].bottom;
        int k = impactRegions[c].left;
        int rely = corners[c].second - (impactRegions[c].bottom - TABLE_RADIUS) - 1; 
        int relx = corners[c].first - (impactRegions[c].left - TABLE_RADIUS) - 1;
#if 0
        int ySteps = min(impactRegions[c].top - impactRegions[c].bottom, rely+1);
        int xSteps = min(impactRegions[c].right - impactRegions[c].left, relx+1);
#endif
        int ySteps = min(impactRegions[1].top - impactRegions[c].bottom, rely+1);
        int xSteps = min(impactRegions[1].right - impactRegions[c].left, relx+1);
        if (xSteps <= 0 || ySteps <= 0)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
        for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
          int start_field = (fj + k); // start index of field 
          int start_field_sse = (fj + k) >> 2; // start index of field_sse
          int end_field_sse = (fj + k + xSteps - 1) >> 2;
          int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse
          
          int fi = start_field_sse, ti = start_table_sse;
          // First slot. Undo the paddings in the first slot
          field_sse[fi] = _mm_sub_ps(field_sse[fi], table_sse[numShifted][ti]);
          __m128 sub = _mm_and_ps(table_sse[numShifted][ti], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[fi] = _mm_add_ps(field_sse[fi], sub);
          ++fi, ++ti;
          // The remaining slots.
          for (; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_sub_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the last slot
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_add_ps(field_sse[end_field_sse], sub);

          fj += boundwidth;
          ty--;
        }
      }
      // top-right and bottom-left corners
      for (int c = 0; c < 2; ++c) {
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegions[c].bottom;
        int k = impactRegions[c].left;
        int rely = corners[c].second - (impactRegions[c].bottom - TABLE_RADIUS) - 1; 
        int relx = corners[c].first - (impactRegions[c].left - TABLE_RADIUS) - 1;
#if 0
        int ySteps = min(impactRegions[c].top - impactRegions[c].bottom, rely+1);
        int xSteps = min(impactRegions[c].right - impactRegions[c].left, relx+1);
#endif
        int ySteps = min(impactRegions[1].top - impactRegions[c].bottom, rely+1);
        int xSteps = min(impactRegions[1].right - impactRegions[c].left, relx+1);
        if (xSteps <= 0 || ySteps <= 0)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
        for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
          int start_field = (fj + k); // start index of field 
          int start_field_sse = (fj + k) >> 2; // start index of field_sse
          int end_field_sse = (fj + k + xSteps - 1) >> 2;
          int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse
          int fi = start_field_sse, ti = start_table_sse;
          // First slot. Undo the paddings in the first slot
          field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          __m128 sub = _mm_and_ps(table_sse[numShifted][ti], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[fi] = _mm_sub_ps(field_sse[fi], sub);
          ++fi, ++ti;
          // The remaining slots.
          for (; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the last slot
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);

          fj += boundwidth;
          ty--;
        }
      }
      {
        //region for corner 3
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegions[0].bottom;
        int k = impactRegions[0].left;
        int rely = corners[0].second - (impactRegions[0].bottom - TABLE_RADIUS) - 1; 
        int relx = corners[0].first - (impactRegions[0].left - TABLE_RADIUS) - 1;
#if 0
        int ySteps1 = 0;
        int xSteps1= min(impactRegions[1].right - impactRegions[0].left, relx+1);
        int ySteps2 = min(impactRegions[1].top - impactRegions[0].bottom, rely+1);
        int xSteps2 = min(impactRegions[0].right - impactRegions[0].left, relx+1);
#endif
        int ySteps1 = min(impactRegions[1].top - impactRegions[0].bottom, rely+1);
        int xSteps1= 0;
        int ySteps2 = min(impactRegions[0].top - impactRegions[0].bottom, rely+1);
        int xSteps2 = min(impactRegions[1].right - impactRegions[0].left, relx+1);
        if (ySteps1 >= ySteps2 || xSteps1 >= xSteps2)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = (((j+ySteps1)*boundwidth+k+xSteps1)%4 - (haloTotalSize-((rely-ySteps1)*haloWidth+relx-xSteps1)-1)%4 + 4)%4;
        for (int ys = ySteps1, fj = (j+ySteps1)*boundwidth, ty = rely-ySteps1; ys < ySteps2; ++ys) {
          int start_field = (fj + k+xSteps1); // start index of field 
          int start_field_sse = start_field >> 2; // start index of field_sse
          int end_field_sse = (start_field + xSteps2 - xSteps1 - 1) >> 2;
          int numFirstPadded_field = start_field % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((start_field + xSteps2 - xSteps1 - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx-xSteps1) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse
          int fi = start_field_sse, ti = start_table_sse;
          // First slot. Undo the paddings in the first slot
          field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          __m128 sub = _mm_and_ps(table_sse[numShifted][ti], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[fi] = _mm_sub_ps(field_sse[fi], sub);
          ++fi, ++ti;
          // The remaining slots.
          for (; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the last slot
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);

          fj += boundwidth;
          ty--;
      }
      }
#if 0
      {
        //region for corner 3
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegions[0].bottom;
        int k = impactRegions[0].left;
        int rely = corners[0].second - (impactRegions[0].bottom - TABLE_RADIUS) - 1; 
        int relx = corners[0].first - (impactRegions[0].left - TABLE_RADIUS) - 1;
        int ySteps1 = 0;
        int xSteps1= min(impactRegions[1].right - impactRegions[0].left, relx+1);
        int ySteps2 = min(impactRegions[1].top - impactRegions[0].bottom, rely+1);
        int xSteps2 = min(impactRegions[0].right - impactRegions[0].left, relx+1);
#if 0
        int ySteps1 = min(impactRegions[1].top - impactRegions[0].bottom, rely+1);
        int xSteps1= 0;
        int ySteps2 = min(impactRegions[0].top - impactRegions[0].bottom, rely+1);
        int xSteps2 = min(impactRegions[1].right - impactRegions[0].left, relx+1);
#endif
        if (ySteps1 >= ySteps2 || xSteps1 >= xSteps2)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = (((j+ySteps1)*boundwidth+k+xSteps1)%4 - (haloTotalSize-((rely-ySteps1)*haloWidth+relx-xSteps1)-1)%4 + 4)%4;
        for (int ys = ySteps1, fj = (j+ySteps1)*boundwidth, ty = rely-ySteps1; ys < ySteps2; ++ys) {
          int start_field = (fj + k+xSteps1); // start index of field 
          int start_field_sse = start_field >> 2; // start index of field_sse
          int end_field_sse = (start_field + xSteps2 - xSteps1 - 1) >> 2;
          int numFirstPadded_field = start_field % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((start_field + xSteps2 - xSteps1 - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx-xSteps1) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse
          int fi = start_field_sse, ti = start_table_sse;
          // First slot. Undo the paddings in the first slot
          field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          __m128 sub = _mm_and_ps(table_sse[numShifted][ti], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[fi] = _mm_sub_ps(field_sse[fi], sub);
          ++fi, ++ti;
          // The remaining slots.
          for (; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the last slot
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);

          fj += boundwidth;
          ty--;
      }
    }
#endif
#if 0
      {
        // (k, j): those pixels impacted by the polygon i
        // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
        int j = impactRegions[0].bottom;
        int k = impactRegions[0].left;
        int rely = corners[0].second - (impactRegions[0].bottom - TABLE_RADIUS) - 1; 
        int relx = corners[0].first - (impactRegions[0].left - TABLE_RADIUS) - 1;
        int ySteps1 = min(impactRegions[1].top - impactRegions[0].bottom, rely+1);
        int xSteps1= min(impactRegions[1].right - impactRegions[0].left, relx+1);
        int ySteps2 = min(impactRegions[0].top - impactRegions[0].bottom, rely+1);
        int xSteps2 = min(impactRegions[0].right - impactRegions[0].left, relx+1);
        if (ySteps1 >= ySteps2 || xSteps1 >= xSteps2)
          continue;

        // In order to align the field_sse and haloTable_sse, we need to shift
        // the haloTable 'numShifted' entries by using the pre-allocated
        // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
        // of rectangle, this shift value is fixed no matter which pixel is
        // updated now.
        // start_field: j*boundwidth+k
        // start_table: haloTotalSize-(rely*haloWidth+relx)-1
        // numshift: (start_field%4 - start_table%4 + 4) % 4
        int numShifted = (((j+ySteps1)*boundwidth+k+xSteps1)%4 - (haloTotalSize-((rely-ySteps1)*haloWidth+relx-xSteps1)-1)%4 + 4)%4;
        for (int ys = ySteps1, fj = (j+ySteps1)*boundwidth, ty = rely-ySteps1; ys < ySteps2; ++ys) {
          int start_field = (fj + k+xSteps1); // start index of field 
          int start_field_sse = start_field >> 2; // start index of field_sse
          int end_field_sse = (start_field + xSteps2 - xSteps1 - 1) >> 2;
          int numFirstPadded_field = start_field % 4; // number of the padded (0s) in the first slot in field_sse
          int numLastPadded_field = 3 - ((start_field + xSteps2 - xSteps1 - 1) % 4); // number of the padded (0s) in the last slot
          int start_table = haloTotalSize - (ty * haloWidth + relx-xSteps1) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
          start_table += numShifted;
          int start_table_sse = start_table >> 2; //start index of table_sse
          int fi = start_field_sse, ti = start_table_sse;
          // First slot. Undo the paddings in the first slot
          field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          __m128 sub = _mm_and_ps(table_sse[numShifted][ti], leaveLeastMasks[numFirstPadded_field]); 
          field_sse[fi] = _mm_sub_ps(field_sse[fi], sub);
          ++fi, ++ti;
          // The remaining slots.
          for (; fi <= end_field_sse; ++fi, ++ti)
            field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
          // Undo the paddings in the last slot
          sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
          field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);

          fj += boundwidth;
          ty--;
      }
    }
#endif
    }
    for (int i = 0; i < 4; ++i)
      free(haloTable[i]);
    int sz_sse = paddedImageSize/4;
    for (int i = 0; i < sz_sse; i++) {
      __m128 square = _mm_mul_ps(field_sse[i], field_sse[i]);
      __m128 weight_sse = _mm_set_ps(weightArray[weightIdx], weightArray[weightIdx], weightArray[weightIdx], weightArray[weightIdx]);
      __m128 tmp = _mm_mul_ps(square, weight_sse);
      image_sse[i] = _mm_add_ps(image_sse[i], tmp);
    }
  }
  free(field);

  sseTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
}
#endif

