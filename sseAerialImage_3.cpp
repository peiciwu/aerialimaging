#include <iostream>
#include <time.h>
#include <complex>
#include <xmmintrin.h>
#include <emmintrin.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifndef __APPLE__
#include <malloc.h>
#endif
//#include <omp.h>
#include <limits.h>

#include "sseAerialImage.h"

using namespace std;

static __m128 zeroAllMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0x0));
static __m128 zeroLeastMask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0xFFffFFff, 0x0));
static __m128 zeroLeast2Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0x0, 0x0));
static __m128 zeroLeast3Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0x0, 0x0, 0x0));
static __m128 zeroMostMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0xFFffFFff, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost2Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost3Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0xFFffFFff));
static __m128 zeroLeastMasks[3] = {zeroLeastMask, zeroLeast2Mask, zeroLeast3Mask};
static __m128 zeroMostMasks[3] = {zeroMostMask, zeroMost2Mask, zeroMost3Mask};
static __m128 leaveLeastMasks[4] = {zeroAllMask, zeroMost3Mask, zeroMost2Mask, zeroMostMask};
static __m128 leaveMostMasks[4] = {zeroAllMask, zeroLeast3Mask, zeroLeast2Mask, zeroLeastMask};

// /brief
// Each SSE instruction simultaneously updates several pixels in the same image.
// The framework is similar to "gold_continuous", except that "gold_continuous"
// only updates 1 pixel one time.
// Current implementation is to update 4 pixels at the same time. In order to
// use sse instructions, we have 
// 1. The size of sseImage MUST be a multiple of 4.
//  - Here, the memory padding/allocatio of sseImage is done inside the caller function,
//    i.e. main function, paddedImageSize is the size after padding
// 2. The size of field image MUST be a multiple of 4.
//  - The allocation is the same as sseImage
// 3. When updating field_sse by each row of haloTable, we have the following two
// situations:
//  3.1. If start idx and end idx is the start of a multiple of 4 and end of
//  multiple of 4, do nothing
//  3.2. If NOT,
//      a. (CURRENT IMPLEMENTATION), manually update the remainder at
//      the first and last slot
//      b. padding..........(has to change the allocation of haloTable)
// 3. The size of haloTable MUST be a multiple of 4
//  - pad into a multiple of 4 by inserting to the end of the array
// FIXME. According to impactRegion, allocate smaller haloTable???? But we
// cannot allocate a single haloTable that works on all rectangels...
void ssePrintImage_3(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weightArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize)
{
  clock_t startTime = clock();

  int boundwidth = (boundBox.right - boundBox.left) >> 2; // divide by GRID_SIZE(4)
  int boundheight = (boundBox.top - boundBox.bottom) >> 2;

  // FIXME. Note this version doesn't have halo outside each lookup table.
  // Extend a halo of size at most 4 (to be successfully divided by 4) for each
  // lookuptable
  int haloWidth, haloHeight;
  haloHeight = TABLE_SIZE;
  haloWidth = TABLE_SIZE;
#if 0
  for (int i = 0; i < 4; ++i) {
    haloWidth[i] = (TABLE_SIZE+i) + (4 - (TABLE_SIZE+i)%4);
  }
#endif
  
  // FIXME. NOT SURE ABOUT THIS!!!!! Disable it now. Has to recompute the shift
  // number for every row.
  // We want haloWidth%4 == boundwidth%4
  //haloWidth +=  (boundwidth%4 - haloWidth%4 + 4)%4;
  int haloTotalSize = haloWidth * haloHeight;
#if 0
  int haloTotalSize[4];
  for (int i = 0; i < 4; ++i) {
    haloTotalSize[i] = haloWidth[i] * haloHeight;
  }
#endif

  __m128* image_sse = (__m128*) sseImage; //stored in reverse order.....
  int sz_sse = paddedImageSize >> 2;
  for (int i = 0; i < sz_sse; i++) {
    image_sse[i] = _mm_setzero_ps();
  }

  {
    float* field; //is an array of paddedImageSizee
//#ifdef __APPLE__
    field = (float *) malloc(sizeof(float) * paddedImageSize);
    __m128* field_sse = (__m128*) field; //stored in reverse order.....

    Rectangle polygonImage;

    for (int tableIdx = 0; tableIdx < TABLE_NUMBER; tableIdx++) {
      int weightIdx = tableIdx >> 1;

      for (int s = 0, i = 0; s < paddedImageSize; s += 4, i++) {
        field_sse[i] = _mm_setzero_ps(); 
      }

      // Create ONE halo-lookuptable that works for all rectangles. Halo is assigned boundary values.
      float* haloTable[4]; //0: no shift, 1: shift right one index, 2: shift right two, 3: shift right three
      // Allocate and initialize haloTable[0]. The origianl haloTable but is
      // reversed.
//#ifdef __APPLE__
      haloTable[0] = (float*) malloc(sizeof(float) * haloTotalSize);
      for (int y = 0, ty = TABLE_SIZE-1; y < haloHeight; ++y, --ty) {
        for (int x = 0, tx = TABLE_SIZE-1; x < haloWidth; ++x , --tx)
          haloTable[0][y * haloWidth + x] = tableArray[tableIdx][ty * TABLE_SIZE + tx];
      }
      // Allocate and initialize other haloTables
      for (int i = 1; i < 4; ++i) {
//#ifdef __APPLE__
        haloTable[i] = (float*) malloc(sizeof(float) * (haloTotalSize + i));
      }
      for (int i = 1; i < 4; ++i)
        memset(haloTable[i], 0, sizeof(float)*i);
      for (int i = 1; i < 4; ++i) 
        memcpy(haloTable[i]+i, haloTable[0], sizeof(float)*haloTotalSize);

      __m128* table_sse[4];
      for (int i = 0; i < 4; ++i)
        table_sse[i] = (__m128*) haloTable[i];

      for (int i = 0; i < rectNum; i++) {
        // Note that all polygons are inside the left and bottom boundary of the
        // aerial image, but some of them exceed the right and top boundary.
        polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom) >> 2;
        polygonImage.top    = (polygonArray[i].top    - boundBox.bottom) >> 2;
        polygonImage.left   = (polygonArray[i].left   - boundBox.left)   >> 2;
        polygonImage.right  = (polygonArray[i].right  - boundBox.left)   >> 2;
        // Ignore the polygon that is totally outside the aerial image
        if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
          continue;
        // The polygon's top/right boundary over some limit is meaningless, i.e.
        // LUT won't be used to check those pixels. The limit is as follows:
        // FIXME. NOT SURE....
        // (boundheight-1) (the right most pixel in the aerial image) +
        // TABLE_RADIUS + 1.
        polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
        polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

        pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
        corners[0] = make_pair(polygonImage.right, polygonImage.top);
        corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
        corners[2] = make_pair(polygonImage.left, polygonImage.top);
        corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

        Rectangle impactRegions[4]; // one impactRegion per corner
        impactRegions[0].bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
        impactRegions[0].left = max(polygonImage.left - KERNEL_RADIUS, 0);
        impactRegions[0].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
        impactRegions[0].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);
        for (int i = 1; i < 4; ++i) {
          impactRegions[i].bottom = impactRegions[0].bottom;
          impactRegions[i].left = impactRegions[0].left;
        }
        impactRegions[1].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
        impactRegions[1].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);
        impactRegions[2].top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
        impactRegions[2].right = min(polygonImage.left + KERNEL_RADIUS, boundwidth);
        impactRegions[3].top = min(polygonImage.bottom + KERNEL_RADIUS, boundheight);
        impactRegions[3].right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);

        // top-left and bottom-right corners
        for (int c = 2; c < 4; ++c) {
          // (k, j): those pixels impacted by the polygon i
          // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
          int j = impactRegions[c].bottom;
          int k = impactRegions[c].left;
          int rely = corners[c].second - (impactRegions[c].bottom - TABLE_RADIUS) - 1; 
          int relx = corners[c].first - (impactRegions[c].left - TABLE_RADIUS) - 1;
          int ySteps = impactRegions[c].top - impactRegions[c].bottom;
          int xSteps = impactRegions[c].right - impactRegions[c].left;
#if 0
          if (xSteps <= 0)
            continue;
#endif

          // In order to align the field_sse and haloTable_sse, we need to shift
          // the haloTable 'numShifted' entries by using the pre-allocated
          // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
          // of rectangle, this shift value is fixed no matter which pixel is
          // updated now.
          // start_field: j*boundwidth+k
          // start_table: haloTotalSize-(rely*haloWidth+relx)-1
          // numshift: (start_field%4 - start_table%4 + 4) % 4
          //int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
          for (int ys = 0, fj = j*boundwidth, tty = rely; ys < ySteps; ++ys) {
            int ty = tty;
            if (tty >= haloHeight)
              ty = haloHeight - 1;
            int start_field = (fj + k); // start index of field 
            int start_field_sse; // start index of field_sse
            int end_field_sse;
            int xs = 0;
            int tx = min(relx, haloWidth-1);
            int start_table = haloTotalSize - (ty * haloWidth + tx) - 1; 
            if (relx >= haloWidth) {
              // Outside the table
              start_field_sse = start_field >> 2;
              int outsideSteps = min(relx - haloWidth + 1, xSteps);
              end_field_sse = (fj + k + outsideSteps - 1) >> 2;
              __m128 boundary_sse = _mm_set1_ps(haloTable[0][start_table]);
              for (int fi = start_field_sse; fi <= end_field_sse; ++fi)
                field_sse[fi] = _mm_sub_ps(field_sse[fi], boundary_sse);
              // Undo the first
              int numFirstPadded_field = (fj + k) - (start_field_sse<<2);
              __m128 sub = _mm_and_ps(boundary_sse, leaveLeastMasks[numFirstPadded_field]); 
              field_sse[start_field_sse] = _mm_add_ps(field_sse[start_field_sse], sub);
              // Undo the paddings in the last slot
              int numLastPadded_field = 3 - ((fj+k+outsideSteps-1)-(end_field_sse<<2));
              sub = _mm_and_ps(boundary_sse, leaveMostMasks[numLastPadded_field]);
              field_sse[end_field_sse] = _mm_add_ps(field_sse[end_field_sse], sub);

              xs += outsideSteps;
              start_field = fj + k + outsideSteps;
            }
            //if (xs < xSteps) {
            {
              // Inside the table
              start_field_sse = start_field >> 2;
              end_field_sse = (fj + k + xSteps - 1) >> 2;
              int numShifted = (start_field%4 - start_table%4 + 4)%4;
              start_table += numShifted;
              int start_table_sse = start_table >> 2; //start index of table_sse
              int ti = start_table_sse;
              for (int fi = start_field_sse; fi <= end_field_sse; ++fi, ++ti)
                field_sse[fi] = _mm_sub_ps(field_sse[fi], table_sse[numShifted][ti]);
              // Undo the paddings in the first slot
              int numFirstPadded_field = (start_field) - (start_field_sse<<2);
              __m128 sub = _mm_and_ps(table_sse[numShifted][start_table_sse], leaveLeastMasks[numFirstPadded_field]); 
              field_sse[start_field_sse] = _mm_add_ps(field_sse[start_field_sse], sub);
              // Undo the paddings in the last slot
              int numLastPadded_field = 3 - ((fj+k+xSteps-1)-(end_field_sse<<2));
              sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
              field_sse[end_field_sse] = _mm_add_ps(field_sse[end_field_sse], sub);
            }

            fj += boundwidth;
            tty--;
          }
        }
        // top-right and bottom-left corners
        for (int c = 0; c < 2; ++c) {
          // (k, j): those pixels impacted by the polygon i
          // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
          int j = impactRegions[c].bottom;
          int k = impactRegions[c].left;
          int rely = corners[c].second - (impactRegions[c].bottom - TABLE_RADIUS) - 1; 
          int relx = corners[c].first - (impactRegions[c].left - TABLE_RADIUS) - 1;
          int ySteps = impactRegions[c].top - impactRegions[c].bottom;
          int xSteps = impactRegions[c].right - impactRegions[c].left;
#if 0
          if (xSteps <= 0)
            continue;
#endif

          // In order to align the field_sse and haloTable_sse, we need to shift
          // the haloTable 'numShifted' entries by using the pre-allocated
          // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
          // of rectangle, this shift value is fixed no matter which pixel is
          // updated now.
          // start_field: j*boundwidth+k
          // start_table: haloTotalSize-(rely*haloWidth+relx)-1
          // numshift: (start_field%4 - start_table%4 + 4) % 4
          //int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
          for (int ys = 0, fj = j*boundwidth, tty = rely; ys < ySteps; ++ys) {
            int ty = tty;
            if (tty >= haloHeight)
              ty = haloHeight - 1;
            int start_field = (fj + k); // start index of field 
            int start_field_sse; // start index of field_sse
            int end_field_sse;
            int xs = 0;
            int tx = min(relx, haloWidth-1);
            int start_table = haloTotalSize - (ty * haloWidth + tx) - 1; 
            if (relx >= haloWidth) {
              // Outside the table
              start_field_sse = start_field >> 2;
              int outsideSteps = min(relx - haloWidth + 1, xSteps);
              end_field_sse = (fj + k + outsideSteps - 1) >> 2;
              int start_table = haloTotalSize - (ty * haloWidth + haloWidth-1) - 1; 
              //__m128 boundary_sse = _mm_set1_ps(tableArray[tableIdx][ty*TABLE_SIZE+TABLE_SIZE-1]);
              __m128 boundary_sse = _mm_set1_ps(haloTable[0][start_table]);
              for (int fi = start_field_sse; fi <= end_field_sse; ++fi)
                field_sse[fi] = _mm_add_ps(field_sse[fi], boundary_sse);
              // Undo the first
              int numFirstPadded_field = (fj + k) - (start_field_sse<<2);
              __m128 sub = _mm_and_ps(boundary_sse, leaveLeastMasks[numFirstPadded_field]); 
              field_sse[start_field_sse] = _mm_sub_ps(field_sse[start_field_sse], sub);
              // Undo the paddings in the last slot
              int numLastPadded_field = 3 - ((fj+k+outsideSteps-1)-(end_field_sse<<2));
              sub = _mm_and_ps(boundary_sse, leaveMostMasks[numLastPadded_field]);
              field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);

              xs += outsideSteps;
              start_field = fj + k + outsideSteps;
            }
            //if (xs < xSteps) {
            {
              // Inside the table
              start_field_sse = start_field >> 2;
              end_field_sse = (fj + k + xSteps - 1) >> 2;
              int numShifted = (start_field%4 - start_table%4 + 4)%4;
              start_table += numShifted;
              int start_table_sse = start_table >> 2; //start index of table_sse
              int ti = start_table_sse;
              for (int fi = start_field_sse; fi <= end_field_sse; ++fi, ++ti)
                field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
              // Undo the paddings in the first slot
              int numFirstPadded_field = (start_field) - (start_field_sse<<2);
              __m128 sub = _mm_and_ps(table_sse[numShifted][start_table_sse], leaveLeastMasks[numFirstPadded_field]); 
              field_sse[start_field_sse] = _mm_sub_ps(field_sse[start_field_sse], sub);
              // Undo the paddings in the last slot
              int numLastPadded_field = 3 - ((fj+k+xSteps-1)-(end_field_sse<<2));
              sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
              field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);
            }

            fj += boundwidth;
            tty--;
          }
        }
      }
      for (int i = 0; i < 4; ++i)
        free(haloTable[i]);
      int sz_sse = paddedImageSize>>2;
      float weightValue = weightArray[weightIdx];
      __m128 weight_sse = _mm_set1_ps(weightValue);
      for (int i = 0; i < sz_sse; i++) {
        __m128 square = _mm_mul_ps(field_sse[i], field_sse[i]);
        __m128 tmp = _mm_mul_ps(square, weight_sse);
        image_sse[i] = _mm_add_ps(image_sse[i], tmp);
      }
    }
    free(field);
  }

  ompTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
}

