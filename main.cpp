#include "datastruct.h"
#ifdef RUN_CUDA
#include "cudaAerialImage_PIXEL.h"
#include "cudaAerialImage_LUT.h"
#endif
#include "goldAerialImage.h"
#include "sseAerialImage.h"
#include "ompAerialImage.h"

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
#include <cassert>
#ifndef __APPLE__
#include <malloc.h>
#endif

using namespace std;

double maxError(const float* cudaImage, const double* goldImage, int imageSize);
double maxError(const double* cudaImage, const double* goldImage, int imageSize);
//bool checkResult(const float*, const float*, const Rectangle);
void readTable(const char*, float**, double**);
void readKernel(const char*, float [][KERNEL_SIZE*KERNEL_SIZE]);
void writeImage(const char*, const float *, const Rectangle);
void writeImage(const char*, const double *, const Rectangle);
void readPolygon(const char*, Rectangle *&, Rectangle &, INT4&);
void readKernelWeight(const char*, float*);

int main(int argc, char *argv[])
{

    if(argc!=3){
	cout<<"input error:"<<endl;
	cout<<"\tprintimage inputfile"<<endl;
	return 0;
    }

    cout<<"running..."<<endl;
// data setup here

    // kernel weight array
    float weightArray[PADDED_KERNEL_NUMBER];

    // table is build double, odd for real, even for image
    // To prevent exceeding the size of stack, allcoate dynamically
    float* tableArray[PADDED_TABLE_NUMBER];
    double* tableArray_d[PADDED_TABLE_NUMBER];
    for (int i = 0; i < PADDED_TABLE_NUMBER; ++i) {
      tableArray[i] = (float*) malloc(sizeof(float) * TABLE_SIZE * TABLE_SIZE);
      tableArray_d[i] = (double*) malloc(sizeof(double) * TABLE_SIZE * TABLE_SIZE);
    }

    // rectNum in the polygonArray
    INT4 rectNum;
    // input of the polygonArray
    Rectangle *polygonArray;
    // input of the layout bound
    Rectangle boundBox;
    cout<<"Reading polygon list"<<endl;
    readPolygon(argv[1], polygonArray, boundBox, rectNum);

    cout<<"Reading table list"<<endl;

#ifndef DDEBUG
    readTable("tablelist", tableArray, tableArray_d);
#else
    readTable("testlist", tableArray, tableArray_d);
#endif

//    cout<<"reading kernel list"<<endl;
//    readKernel("kernellist", kernelArray);

    cout<<"reading weight list"<<endl;
#ifndef DDEBUG
    readKernelWeight("lookuptable/weight.txt", weightArray);
#else
    readKernelWeight("lookuptable/test_weight.txt", weightArray);
#endif

// cuda/sse and gold code starts here

    float cudaTime;
    float goldTime;
    float sseTime;
    float ompTime;

    int imageSize = ((boundBox.right-boundBox.left)/GRID_SIZE)*((boundBox.top-boundBox.bottom)/GRID_SIZE);
    double* goldImage = (double*) malloc(imageSize*sizeof(double));

    cout << "running gold code..." << endl;
    goldPrintImage(polygonArray,
	    rectNum,
	    weightArray,
	    tableArray_d,
	    goldImage,
	    goldTime,
	    boundBox);

    cout<<"gold run time is "<<goldTime<<" s"<<endl;
    writeImage("goldImage.txt", goldImage, boundBox);

    float max_error;

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // For testing gold_continuous version!!!
    cout << endl << "====== running gold_continuous... ======" << endl;
    float goldTime_c;
    DTYPE* goldImage_c;
    goldImage_c = (DTYPE*) malloc(imageSize*sizeof(DTYPE));

    goldPrintImage_continuous(polygonArray,
      rectNum,
      weightArray,
#if USE_DOUBLE
      tableArray_d,
#else
      tableArray,
#endif
      goldImage_c,
      goldTime_c,
      boundBox);

    writeImage("goldImage_c.txt", goldImage_c, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(goldImage_c, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "gold_continuous run time is " << goldTime_c << " s\n";
    cout<<"speed up is "<<goldTime/goldTime_c<<" X"<<endl;

    free(goldImage_c);

    {
    cout << endl << "====== running gold_continuous_2... ======" << endl;
    float goldTime_c;
    DTYPE* goldImage_c;
    goldImage_c = (DTYPE*) malloc(imageSize*sizeof(DTYPE));

    goldPrintImage_continuous_2(polygonArray,
      rectNum,
      weightArray,
#if USE_DOUBLE
      tableArray_d,
#else
      tableArray,
#endif
      goldImage_c,
      goldTime_c,
      boundBox);

    writeImage("goldImage_c.txt", goldImage_c, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(goldImage_c, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "gold_continuous_2 run time is " << goldTime_c << " s\n";
    cout<<"speed up is "<<goldTime/goldTime_c<<" X"<<endl;

    free(goldImage_c);
    }
    // End of testing gold_continuous version!!!
    ///////////////////////////////////////////////////////////////////////////////////////////////

    cout << "running cuda/sse code..." << endl;
    // build up your own code for this function
    // no parameter change is allowed

    cout << "command is "<< argv[2] << endl;
    string command(argv[2]);

    bool all = false;
    int index = 0;
    if (command=="all"){
	all = true;
    } else {
	index = atoi(argv[2]);
    }

    cout << "command is "<< command << endl;

#define NONE 0
#define LUT_1 1
#define LUT_2 2
#define LUT_3 3
#define PIXEL_1 4
#define PIXEL_2 5
#define PIXEL_3 6
#define PIXEL_4 7 //the best
#define PIXEL_5 8 //the best
#define PIXEL_6 9 //error...
#define PIXEL_7 10 //....
#define PIXEL_8 11 //doesn't work
#define UCLA 12 //compare
#define SHARE_1 13 //recently changes... (local memory->shared memory)
// for sse instructions
#define SSE_ALL 14
#define SSE_1 15
#define SSE_2 16
// for OpenMP
#define OMP_ALL 17
#define OMP_GOLD 18
#define OMP_GOLD_CON 19
#define OMP_SSE_1 20
#define OMP_SSE_2 21
// for sse and OpenMP all (testing...)
#define SSE_OMP_ALL 22

  /* SSE_1 */
  if (all || index == SSE_ALL || index == SSE_1 || index == SSE_OMP_ALL) {
    cout << endl << "====== running SSE_1: update the same pixel in different images ======" << endl;
    float *sseImage_1;
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    sseImage_1 = (float*) malloc(imageSize * sizeof(float));
    ssePrintImage_1(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      sseImage_1,
      sseTime,
      boundBox);

    writeImage("sseImage_1.txt", sseImage_1, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(sseImage_1, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "sse_1 run time is " << sseTime << " s\n";
    cout<<"speed up is "<<goldTime/sseTime<<" X"<<endl;

    free(sseImage_1);
  }

#if 0
  /* SSE_2 */
  if (all || index == SSE_ALL || index == SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running SSE_2: update several pixels in the same image ======" << endl;
    float *sseImage_2; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    sseImage_2 = (float*) malloc(paddedImageSize * sizeof(float));
    ssePrintImage_2(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      sseImage_2,
      sseTime,
      boundBox,
      paddedImageSize);

    writeImage("sseImage_2.txt", sseImage_2, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(sseImage_2, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "sse_2 run time is " << sseTime << " s\n";
    cout<<"speed up is "<<goldTime/sseTime<<" X"<<endl;

    free(sseImage_2);
  }
#endif

  /* SSE_2_2 */
  if (all || index == SSE_ALL || index == SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running SSE_2_2: update several pixels in the same image ======" << endl;
    float *sseImage_2; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    sseImage_2 = (float*) malloc(paddedImageSize * sizeof(float));
    ssePrintImage_2_2(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      sseImage_2,
      sseTime,
      boundBox,
      paddedImageSize);

    writeImage("sseImage_2.txt", sseImage_2, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(sseImage_2, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "sse_2_2 run time is " << sseTime << " s\n";
    cout<<"speed up is "<<goldTime/sseTime<<" X"<<endl;

    free(sseImage_2);
  }

  /* SSE_3 */
  if (all || index == SSE_ALL || index == SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running SSE_3: update several pixels in the same image ======" << endl;
    float *sseImage_2; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    sseImage_2 = (float*) malloc(paddedImageSize * sizeof(float));
    ssePrintImage_3(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      sseImage_2,
      sseTime,
      boundBox,
      paddedImageSize);

    writeImage("sseImage_3.txt", sseImage_2, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(sseImage_2, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "sse_3 run time is " << sseTime << " s\n";
    cout<<"speed up is "<<goldTime/sseTime<<" X"<<endl;

    free(sseImage_2);
  }

  /* SSE_4 */
  if (all || index == SSE_ALL || index == SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running SSE_4: update several pixels in the same image ======" << endl;
    float *sseImage_2; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    sseImage_2 = (float*) malloc(paddedImageSize * sizeof(float));
    ssePrintImage_4(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      sseImage_2,
      sseTime,
      boundBox,
      paddedImageSize);

    writeImage("sseImage_3.txt", sseImage_2, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(sseImage_2, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "sse_4 run time is " << sseTime << " s\n";
    cout<<"speed up is "<<goldTime/sseTime<<" X"<<endl;

    free(sseImage_2);
  }

  /* OMP_GOLD */
  if (all || index == OMP_ALL || index == OMP_GOLD || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_GOLD: use OpenMP on gold codes.  ======" << endl;
    DTYPE *ompImage_gold;
    ompImage_gold = (DTYPE*) malloc(imageSize * sizeof(DTYPE));
    ompPrintImage_gold(polygonArray,
      rectNum,
      weightArray,
#if USE_DOUBLE
      tableArray_d,
#else
      tableArray,
#endif
      ompImage_gold,
      ompTime,
      boundBox);

    writeImage("ompImage_gold.txt", ompImage_gold, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_gold, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_gold run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_gold);
  }

  /* OMP_GOLD_CON */
  if (all || index == OMP_ALL || index == OMP_GOLD_CON || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_GOLD_CON: use OpenMP on gold_continuous codes.  ======" << endl;
    DTYPE *ompImage_gold_con;
    ompImage_gold_con = (DTYPE*) malloc(imageSize * sizeof(DTYPE));
    ompPrintImage_gold_con(polygonArray,
      rectNum,
      weightArray,
#if USE_DOUBLE
      tableArray_d,
#else
      tableArray,
#endif
      ompImage_gold_con,
      ompTime,
      boundBox);

    writeImage("ompImage_gold_con.txt", ompImage_gold_con, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_gold_con, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_gold_con run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_gold_con);
  }

  /* OMP_SSE_1 */
  if (all || index == OMP_ALL || index == OMP_SSE_1 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_1: update the same pixel in different images ======" << endl;
    float *ompImage_sse_1;
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_1 = (float*) malloc(imageSize * sizeof(float));
    ompPrintImage_sse_1(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_1,
      ompTime,
      boundBox);

    writeImage("ompImage_sse_1.txt", ompImage_sse_1, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_1, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_1 run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_1);
  }

  /* OMP_SSE_1_TEST */
  if (all || index == OMP_ALL || index == OMP_SSE_1 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_1_TEST: update the same pixel in different images ======" << endl;
    float *ompImage_sse_1;
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_1 = (float*) malloc(imageSize * sizeof(float));
    ompPrintImage_sse_1_test(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_1,
      ompTime,
      boundBox);

    writeImage("ompImage_sse_1.txt", ompImage_sse_1, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_1, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_1_test run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_1);
  }

#if 0
  /* OMP_SSE_2_TEST */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_2_TEST: update several pixels in the same image ======" << endl;
    float *ompImage_sse_2_test; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_2_test = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_2_test(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_2_test,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_2_test.txt", ompImage_sse_2_test, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_2_test, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_2 run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_2_test);
  }

  /* OMP_SSE_2 */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_2: update several pixels in the same image ======" << endl;
    float *ompImage_sse_2; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_2 = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_2(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_2,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_2.txt", ompImage_sse_2, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_2, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_2 run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_2);
  }
#endif

  /* OMP_SSE_2_2 */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_2_2: update several pixels in the same image ======" << endl;
    float *ompImage_sse_2; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_2 = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_2_2(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_2,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_2.txt", ompImage_sse_2, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_2, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_2_2 run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_2);
  }

  /* OMP_SSE_2_2_test */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_2_2_test: update several pixels in the same image ======" << endl;
    float *ompImage_sse_2; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_2 = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_2_2_test(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_2,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_2.txt", ompImage_sse_2, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_2, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_2_2_test run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_2);
  }


#if 0
  /* OMP_SSE_2_ITER */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_2_ITER: update several pixels in the same image ======" << endl;
    float *ompImage_sse_2_iter; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_2_iter = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_2_iter(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_2_iter,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_2_iter.txt", ompImage_sse_2_iter, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_2_iter, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_2_iter run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_2_iter);
  }
#endif

  /* OMP_SSE_3 */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_3: update several pixels in the same image ======" << endl;
    float *ompImage_sse_3; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_3 = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_3(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_3,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_3.txt", ompImage_sse_3, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_3, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_3 run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_3);
  }

  /* OMP_SSE_3_1 */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_3_1: update several pixels in the same image ======" << endl;
    float *ompImage_sse_3; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_3 = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_3_1(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_3,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_3_1.txt", ompImage_sse_3, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_3, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_3_1 run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_3);
  }

  /* OMP_SSE_4 */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_4: update several pixels in the same image ======" << endl;
    float *ompImage_sse_4; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_4 = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_4(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_4,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_4.txt", ompImage_sse_4, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_4, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_4 run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_4);
  }
  
  /* OMP_SSE_4_TEST */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_4_TEST: update several pixels in the same image ======" << endl;
    float *ompImage_sse_4; //size of sseImage_2 MUST be a multiple of 4
    int paddedImageSize = imageSize;
    if (imageSize%4)
      paddedImageSize += (4 - imageSize%4);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_4 = (float*) malloc(paddedImageSize * sizeof(float));
    ompPrintImage_sse_4_test(polygonArray,
      rectNum,
      weightArray,
      tableArray,
      ompImage_sse_4,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_4.txt", ompImage_sse_4, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_4, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_4_test run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_4);
  }

  /* OMP_SSE_4_TEST_DOUBLE */
  if (all || index == OMP_ALL || index == OMP_SSE_2 || index == SSE_OMP_ALL) {
    cout << endl << "====== running OMP_SSE_4_TEST_DOUBLE: update several pixels in the same image ======" << endl;
    double *ompImage_sse_4_d; //size MUST be a multiple of 2
    int paddedImageSize = imageSize;
    if (imageSize%2)
      paddedImageSize += (2 - imageSize%2);
//#ifdef __APPLE__ //memory allocation is always aligned by 16 byte in APPLE.
    ompImage_sse_4_d = (double*) malloc(paddedImageSize * sizeof(double));
    ompPrintImage_sse_4_test_double(polygonArray,
      rectNum,
      weightArray,
      tableArray_d,
      ompImage_sse_4_d,
      ompTime,
      boundBox,
      paddedImageSize);

    writeImage("ompImage_sse_4_d.txt", ompImage_sse_4_d, boundBox);

    cout << "comparing result..." << endl;
    max_error = maxError(ompImage_sse_4_d, goldImage, imageSize);

    cout << "max error = " << max_error << endl;
    cout << "omp_sse_4_test_double run time is " << ompTime << " s\n";
    cout<<"speed up is "<<goldTime/ompTime<<" X"<<endl;

    free(ompImage_sse_4_d);
  }
#ifdef RUN_CUDA //Enable this when cuda is supported by compiler(nvcc)
    /* LUT_1 */
/*    if (all || index==LUT_1){
	cout << endl << "=======  running cuda LUT_1  ======" << endl;
	float * cudaImage_LUT_1 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_LUT_1(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		boundBox,
		cudaImage_LUT_1,
		cudaTime);

	writeImage("cudaImage_LUT_1.txt", cudaImage_LUT_1, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_LUT_1, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda LUT_1 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_LUT_1);

	cout << endl << endl;
    }
*/

/* PIXEL_7 */
	cout << endl << "=======  running cuda initiate P7  ======" << endl;
	float * cudaImage_PIXEL_7 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_7(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_7,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_7.txt", cudaImage_PIXEL_7, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_7, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_7 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_7);

	cout << endl << endl;



    /* LUT_2 */
    if (all || index==LUT_2){
	cout << endl << "=======  running cuda LUT_2  ======" << endl;
	float * cudaImage_LUT_2 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_LUT_2(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		boundBox,
		cudaImage_LUT_2,
		cudaTime);

	writeImage("cudaImage_LUT_2.txt", cudaImage_LUT_2, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_LUT_2, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda LUT_2 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_LUT_2);

	cout << endl << endl;
    }


    /* LUT_3 */
    if (all || index==LUT_3){
	cout << endl << "=======  running cuda LUT_3  ======" << endl;
	float * cudaImage_LUT_3 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_LUT_3(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		boundBox,
		cudaImage_LUT_3,
		cudaTime);

	writeImage("cudaImage_LUT_3.txt", cudaImage_LUT_3, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_LUT_3, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda LUT_3 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;
	free(cudaImage_LUT_3);
	cout << endl << endl;
    }


    /* PIXEL_1 */
    if (all || index==PIXEL_1){
	cout << endl << "=======  running cuda PIXEL_1  ======" << endl;
	float * cudaImage_PIXEL_1 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_1(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_1,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_1.txt", cudaImage_PIXEL_1, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_1, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_1 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_1);

	cout << endl << endl;
    }
    /* PIXEL_2 */
    if (all || index==PIXEL_2){
	cout << endl << "=======  running cuda PIXEL_2  ======" << endl;
	float * cudaImage_PIXEL_2 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_2(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_2,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_2.txt", cudaImage_PIXEL_2, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_2, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_2 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_2);

	cout << endl << endl;
    }

    /* PIXEL_3 */
    if (all || index==PIXEL_3){
	cout << endl << "=======  running cuda PIXEL_3  ======" << endl;
	float * cudaImage_PIXEL_3 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_3(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_3,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_3.txt", cudaImage_PIXEL_3, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_3, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_3 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_3);

	cout << endl << endl;
    }

    /* PIXEL_4 */
    if (all || index==PIXEL_4){
	cout << endl << "=======  running cuda PIXEL_4  ======" << endl;
	float * cudaImage_PIXEL_4 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_4(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_4,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_4.txt", cudaImage_PIXEL_4, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_4, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_4 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_4);

	cout << endl << endl;
    }

    /* PIXEL_5 */
    if (all || index==PIXEL_5){
	cout << endl << "=======  running cuda PIXEL_5  ======" << endl;
	float * cudaImage_PIXEL_5 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_5(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_5,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_5.txt", cudaImage_PIXEL_5, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_5, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_5 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_5);

	cout << endl << endl;
    }

    /* PIXEL_6 */
    if (all || index==PIXEL_6){
	cout << endl << "=======  running cuda PIXEL_6  ======" << endl;
	float * cudaImage_PIXEL_6 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_6(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_6,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_6.txt", cudaImage_PIXEL_6, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_6, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_6 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_6);

	cout << endl << endl;
    }
    
    /* PIXEL_7 */
    if (all || index==PIXEL_7){
	cout << endl << "=======  running cuda PIXEL_7  ======" << endl;
	float * cudaImage_PIXEL_7 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_7(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_7,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_7.txt", cudaImage_PIXEL_7, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_7, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_7 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_7);

	cout << endl << endl;
    }

    /* PIXEL_8 */
    if (all || index==PIXEL_8){
	cout << endl << "=======  running cuda PIXEL_8  ======" << endl;
	float * cudaImage_PIXEL_8 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_8(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_8,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_8.txt", cudaImage_PIXEL_8, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_8, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_8 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_8);

	cout << endl << endl;
    }

    /* UCLA version */
    if (all || index==UCLA) {
	cout << endl <<"======== running cuda UCLA =========" << endl;
	float * cudaImage_UCLA = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_UCLA(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_UCLA,
		cudaTime,
		boundBox);
	writeImage("cudaImage_UCLA.txt", cudaImage_UCLA, boundBox);

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_UCLA, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	cout << "cuda UCLA run time is " << cudaTime << " s" << endl;
	cout << "speed up is "<<goldTime/cudaTime << " X" << endl;

	free(cudaImage_UCLA);

	cout << endl << endl;
    }

    /* SHARE_1 */
    if (all || index==SHARE_1){
	cout << endl << "=======  running cuda SHARE_1  ======" << endl;
	float * cudaImage_SHARE_1 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_SHARE_1(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_SHARE_1,
		cudaTime,
		boundBox);

	writeImage("cudaImage_SHARE_1.txt", cudaImage_SHARE_1, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_SHARE_1, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda SHARE_1 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_SHARE_1);

	cout << endl << endl;
    }


    /* FFT2 */
/*    if (all || index == FFT2){
	cout << endl; << "======== running cuda FFT2 =========" << endl;
	float * cudaImage_FFT2 = (float *) malloc (imageSize * sizeof(float));
	// create a cuda Image result matrix

	cudaPrintImage_FFT2(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_FFT2,
		cudaTime,
		boundBox);
	writeImage("cudaImage_FFT2.txt", cudaImage_FFT2, boundBox);

	cout << "comparing result ..." << endl;
	max_error = maxError(cudaImage_FFT2, goldImage, imageSize);

	cout << "Max error = " << max_error << endl;
	cout << "Cuda FFT2 run time is " << cudaTime << " s" <<endl;
	cout << "Speed up is " << goldTime/cudaTime << " X" <<endl;

	free(cudaImage_FFT2);
	
	cout << endl << endl;
    }
*/    
#endif
    free(goldImage);
    free(polygonArray);
    for (int i = 0; i < PADDED_TABLE_NUMBER; ++i) {
      free(tableArray[i]);
      free(tableArray_d[i]);
    }
    return 1;
}

double maxError(const float* cudaImage, const double* goldImage, int imageSize)
{
    double max_error = .0;
    double max_rel_error = 0.0;
    double currentGold = 0.0;
    double currentGold_rel = 0.0;
    int index;
    //DEBUG
    double total_error=.0;
    for (int i=0;i<imageSize;++i) {
	//DEBUG
//	    cout << "cuda: " << *(cudaImage) << "\t gold: " << *(goldImage) << "\t diff: " << fabs(*(cudaImage) - *(goldImage)) << endl;
	float e = fabs(((double)(cudaImage[i])) - (goldImage[i]))/ (cudaImage[i]);
	if (max_rel_error < e){
	    max_rel_error = e;
	    currentGold_rel = (goldImage[i]);
	}
	e = fabs(((double)(cudaImage[i])) - (goldImage[i]));
	if (max_error < e){
	    index = i;
	    max_error = e;
	    currentGold = (goldImage[i]);
	}
	total_error+=e;
//	max_rel_error = max(max_rel_error, fabs(*(cudaImage) - *(goldImage))/ *(cudaImage));
//	currentGold = *(goldImage);
//	max_error = max(max_error, fabs(*(cudaImage++) - *(goldImage++)));
    }
    cout << "Gold " << currentGold_rel << " relative error " << max_rel_error << endl;
    cout << "Gold " << currentGold << " error " << max_error << endl;
    cout << "index: " << index << endl;
    //cout << "total error: " << fixed << total_error << endl;
    cout << "total error: " << total_error << endl;
    return max_error;
}

double maxError(const double* cudaImage, const double* goldImage, int imageSize)
{
    double max_error = .0;
    double max_rel_error = 0.0;
    double currentGold = 0.0;
    double currentGold_rel = 0.0;
    for (int i=0;i<imageSize;++i) {
	//DEBUG
//	    cout << "cuda: " << *(cudaImage) << "\t gold: " << *(goldImage) << "\t diff: " << fabs(*(cudaImage) - *(goldImage)) << endl;
	double e = fabs(*(cudaImage) - *(goldImage))/ *(cudaImage);
	if (max_rel_error < e){
	    max_rel_error = e;
	    currentGold_rel = *(goldImage);
	}
	e = fabs(*(cudaImage) - *(goldImage));
	if (max_error < e){
	    max_error = e;
	    currentGold = *(goldImage);
	}
//	max_rel_error = max(max_rel_error, fabs(*(cudaImage) - *(goldImage))/ *(cudaImage));
//	currentGold = *(goldImage);
//	max_error = max(max_error, fabs(*(cudaImage++) - *(goldImage++)));
	cudaImage++;
	goldImage++;
    }
    cout << "Gold " << currentGold_rel << " relative error " << max_rel_error << endl;
    cout << "Gold " << currentGold << " error " << max_error << endl;
    return max_error;
}

void readKernel(const char* kernelfilelist, float kernelArray[KERNEL_NUMBER][KERNEL_SIZE*KERNEL_SIZE])
{
    cout<<kernelfilelist<<endl;

    ifstream kernellist;

    kernellist.open(kernelfilelist, fstream::in);
    if (!kernellist.is_open()){
	cout<<"Error in reading file "<<kernelfilelist<<endl;
	exit(1);
    }
    string kernelname;
    ifstream tfs;
    float *elementIdx;
    for (int i=0;i<KERNEL_NUMBER;i++){
	kernellist>>kernelname;
	tfs.open(kernelname.c_str(), fstream::in);
	if (!tfs.is_open()){
	    cout<<"Error in reading file "<<kernelname<<endl;
	    exit(1);
	}
	elementIdx = kernelArray[i];
	while (!tfs.eof())
	    tfs>>*(elementIdx++);
	tfs.close();
    }
    kernellist.close();
}

//void readTable(const char* tablefilelist, float tableArray[PADDED_TABLE_NUMBER][TABLE_SIZE * TABLE_SIZE], double tableArray_d[PADDED_TABLE_NUMBER][TABLE_SIZE*TABLE_SIZE])
//void readTable(const char* tablefilelist, double (*tableArray_d)[TABLE_SIZE*TABLE_SIZE])
void readTable(const char* tablefilelist, float** tableArray, double** tableArray_d)
{
    ifstream tablelist;
    tablelist.open(tablefilelist, fstream::in);
    if (!tablelist.is_open()){
	cout<<"Error in reading file "<<tablefilelist<<endl;
	exit(1);
    }
    string tablename;
    ifstream tfs;
    // FIXME. This is just a workaround. 
    // TABLE_NUM: real table # we have from the input file. 
    // PADDED_TABLE_NUM: padding to a multiple of 4 (the purpose
    // is for SSE instructions, just assing 0 to those dummy allocationss.)
    for (int i = 0; i < TABLE_NUMBER; ++i) {
      tablelist >> tablename;
      tfs.open(tablename.c_str(), fstream::in);
      if (!tfs.is_open()) {
        cout << "Error in readng file " << tablename << endl;
        exit(1);
      }
      int idx = 0;
      while (!tfs.eof()) {
        double value;
        tfs >> value;
        tableArray[i][idx] = value;
        tableArray_d[i][idx] = value;
        idx++;
      }
      tfs.close();
    }
    tablelist.close();
    // Pad zeros.
    for (int i = TABLE_NUMBER; i < PADDED_TABLE_NUMBER; ++i) {
      int num = TABLE_SIZE * TABLE_SIZE;
      for (int j = 0; j < TABLE_SIZE * TABLE_SIZE; ++j) {
        tableArray[i][j] = 0.f;
        tableArray_d[i][j] = 0.f;
      }
    }
}

void readKernelWeight(const char * weightfile, float* weightArray)
{
    // FIXME. This is just a workaround. 
    // KERNEL_NUM: real table # we have from the input file. 
    // PADDED_KERNEL_NUM: padding to a multiple of 2 (the purpose
    // is for SSE instructions, just assing 0 to those dummy allocationss.)
    ifstream ifs;
    ifs.open(weightfile, fstream::in);
    if (!ifs.is_open()){
	cout<<"Error in reading file "<<weightfile<<endl;
	exit(1);
    }
    for (int i=0;i<KERNEL_NUMBER;i++){
	ifs>>*(weightArray++);
    }
    ifs.close();
    for (int i = KERNEL_NUMBER; i < PADDED_KERNEL_NUMBER; ++i)
      *(weightArray++) = 0;
}

void writeImage(const char * outfile, const float * image,const Rectangle boundBox)
{
    int xLength, yLength;
    ofstream ofs;
    ofs.open(outfile, fstream::out);
    const float * imageidx = image;
    xLength = (boundBox.right - boundBox.left) / GRID_SIZE;
    yLength = (boundBox.top - boundBox.bottom) / GRID_SIZE;

    for (int j=0;j<yLength;++j){
	for (int i=0;i<xLength;++i){
	    ofs<<*(imageidx++)<<" ";
	}
	ofs<<endl;
    }
    ofs.close();
}

void writeImage(const char * outfile, const double * image,const Rectangle boundBox)
{
    int xLength, yLength;
    ofstream ofs;
    ofs.open(outfile, fstream::out);
    const double * imageidx = image;
    xLength = (boundBox.right - boundBox.left) / GRID_SIZE;
    yLength = (boundBox.top - boundBox.bottom) / GRID_SIZE;

    for (int j=0;j<yLength;++j){
	for (int i=0;i<xLength;++i){
	    ofs<<*(imageidx++)<<" ";
	}
	ofs<<endl;
    }
    ofs.close();
}

void readPolygon(const char * filename, Rectangle *& polygonArray, Rectangle &boundBox, INT4 &polygonNum)
{
    ifstream ifs;
    ifs.open(filename, fstream::in);
    if (!ifs.is_open()){
	cout<<"Error in opening "<<filename<<endl;
	exit(1);
    }
    int useless;
    
    ifs>>polygonNum;
    polygonArray = (Rectangle*) malloc(sizeof(Rectangle) * polygonNum);
    int i=0;
    ifs>>boundBox.left;
    ifs>>boundBox.bottom;
    ifs>>boundBox.right;
    ifs>>boundBox.top;
    while(!ifs.eof()){
	ifs>>polygonArray[i].left;
	ifs>>polygonArray[i].top;
	ifs>>polygonArray[i].right;
	ifs>>useless;
	ifs>>useless;
	ifs>>useless;
	ifs>>useless;
	ifs>>polygonArray[i].bottom;
	++i;
    }

    ifs.close();
}
