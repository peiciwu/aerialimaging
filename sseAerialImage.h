#ifndef SSEAERIALIMAGE_H
#define SSEAERIALIMAGE_H

#include "datastruct.h"

void ssePrintImage_1(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox);

void ssePrintImage_2(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ssePrintImage_2_2(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ssePrintImage_3(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ssePrintImage_4(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &sseTime,
                   Rectangle boundBox,
                   int paddedImageSize);
#endif
