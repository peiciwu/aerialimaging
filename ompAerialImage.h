#ifndef OMPAERIALIMAGE_H
#define OMPAERIALIMAGE_H

#include "datastruct.h"

void ompPrintImage_gold(Rectangle* polygonArray,
	    int rectNum,
	    float* weightArray,
	    DTYPE** tableArray,
	    DTYPE* &ompImage,
	    float &ompTime,
	    Rectangle boundBox);

void ompPrintImage_gold_con(Rectangle* polygonArray,
	    int rectNum,
	    float* weightArray,
	    DTYPE** tableArray,
	    DTYPE* &ompImage,
	    float &ompTime,
	    Rectangle boundBox);

void ompPrintImage_sse_1(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox);

void ompPrintImage_sse_1_test(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox);

void ompPrintImage_sse_2(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_2_2(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_2_2_test(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_2_test(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_2_iter(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_3(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_3_1(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_4(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_4_test(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);

void ompPrintImage_sse_4_test_double(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weighArray,
                   double** tableArray,
                   double* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize);
#endif
