#include <stdlib.h>
#include <math.h>
#include "fft.h"

#define PI 3.14159265358979323846264338327950288
#define TWO(x) (1<<(x))

using namespace std;

void fft(int N, complex<double> *X)
{
    shuffle(N,X);
    dftmerge(N,X);
}

void shuffle(int N, complex<double> *X)
{
    int n, r, B=1;
    while ((N>>B)>0)
	B++;
    B--;
    for (n=0;n<N;n++){
	r = bitrev(n, B);
	if (r<n)
	    continue;
	swap(X+n, X+r);
    }
}

void swap(complex<double> *a, complex<double> *b)
{
    complex<double> t;
    t  = *a;
    *a = *b;
    *b = t;
}

int bitrev(int n, int B)
{
    int m, r;
    for (r=0, m=B-1;m>=0;--m){
	if ((n>>m)==1){
	    r += TWO(B-1-m);
	    n -= TWO(m);
	}
    }
    return r;
}

void dftmerge(int N, complex<double> *XF)
{
    double pi = 4. * atan(1.0);
    int k, i, p, q, M;
    complex<double> A, B, V, W;

    M=2;
    while(M<=N){
	W = exp(complex<double>(0.0, -2*PI/M));
	V = complex<double>(1., 0.);
	for (k=0;k<M/2;k++){
	    for (i=0;i<N;i+=M){
		p = k+i;
		q = p+M/2;
		A = XF[p];
		B = XF[q] * V;
		XF[p] = A + B;
		XF[q] = A - B;
	    }
	    V = V * W;
	}
	M = 2*M;
    }
}

complex<double> conjugate(complex<double> k)
{
    return complex<double>(k.real(), -k.imag());
}

void ifft(int N, complex<double> *X)
{
    int k;
    for (k=0;k<N;k++)
	X[k] = conjugate(X[k]);

    fft(N, X);

    for (k=0;k<N;k++)
	X[k] = conjugate(X[k])/(double)N;
}

void ifft2(int N, int M, complex<double> *X)
{
    complex<double> *index = X;
    for (int i=0;i<N;i++){
	ifft(M, index);
	index += M;
    }
    complex<double> *array = (complex<double>*)malloc(sizeof(complex<double>)*N);
    for (int i=0;i<M;i++){
	int k=i;
	for (int j=0;j<N;j++){
	    array[j] = X[k];
	    k+=M;
	}
	ifft(N, array);
	k=i;
	for (int j=0;j<N;j++){
	    X[k] = array[j];
	    k+=M;
	}
    }
    free(array);
}

void fft2(int N, int M, complex<double> *X)
{
    complex<double> *index = X;
    for (int i=0;i<N;i++){
	fft(M, index);
	index += M;
    }
    complex<double> *array = (complex<double>*)malloc(sizeof(complex<double>)*N);
    for (int i=0;i<M;i++){
	int k=i;
	for (int j=0;j<N;j++){
	    array[j] = X[k];
	    k+=M;
	}
	fft(N, array);
	k=i;
	for (int j=0;j<N;j++){
	    X[k] = array[j];
	    k+=M;
	}
    }
    free(array);
}

