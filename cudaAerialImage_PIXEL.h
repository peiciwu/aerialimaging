#ifndef CUDAAERIALIMAGE_PIXEL_H
#define CUDAAERIALIMAGE_PIXEL_H

#include"datastruct.h"

void cudaPrintImage_PIXEL_1(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);

void cudaPrintImage_PIXEL_2(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);


void cudaPrintImage_PIXEL_3(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);

void cudaPrintImage_PIXEL_4(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);

void cudaPrintImage_PIXEL_5(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);

void cudaPrintImage_PIXEL_6(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);

void cudaPrintImage_PIXEL_7(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);

void cudaPrintImage_PIXEL_8(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);


void cudaPrintImage_UCLA(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);

void cudaPrintImage_SHARE_1(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &cudaImage,
	    float &cudaTime,
	    Rectangle);








#endif
