#ifndef CUDACOMMON_TAN_CUH
#define CUDACOMMON_TAN_CUH

#include "datastruct.h"

__constant__ float weight;

__forceinline__ __device__ void get_rc(int & r, int & c, const Rectangle & range, int range_width, int index){
  r = index / range_width;
  c = index - r * range_width;
  r += range.bottom;
  c += range.left;
}

__forceinline__ __device__ void get_r_i_idx(int & r_idx, int & i_idx, int kernel_idx, int array_size, int pix_idx) {
  i_idx = kernel_idx * 2 * array_size + pix_idx; 
  r_idx = i_idx + array_size;  
}

__forceinline__ __device__ void fetch_rect(Rectangle & rect, const Rectangle & boundBox, int rect_id, Rectangle * d_rects) {
  rect = d_rects[rect_id];
  rect.right = (rect.right - boundBox.left) / GRID_SIZE;
  rect.left = (rect.left - boundBox.left) / GRID_SIZE;
  rect.top = (rect.top - boundBox.bottom) / GRID_SIZE;
  rect.bottom = (rect.bottom - boundBox.bottom) / GRID_SIZE;
}

__forceinline__ __device__ void compute_range(Rectangle & range, const Rectangle & rect, INT4 image_width, INT4 image_height) {
  range.bottom = max(rect.bottom - KERNEL_RADIUS, 0);
  range.top = min(rect.top + KERNEL_RADIUS, image_height);
  range.left = max(rect.left - KERNEL_RADIUS, 0);
  range.right = min(rect.right + KERNEL_RADIUS, image_width);
}

//this atomicFloatAdd is from Nady
__forceinline__ __device__ void atomicFloatAdd(float *address, float val){

  int tmp0 = *address;
  int i_val = __float_as_int(val + __int_as_float(tmp0));
  int tmp1;

  while((tmp1 = atomicCAS((int *)address, tmp0, i_val)) != tmp0)
    {
      tmp0 = tmp1;
      i_val = __float_as_int(val + __int_as_float(tmp0));
    }
}

__forceinline__ __device__ void get_relative_loc(INT4 & relative_x, INT4 & relative_y, INT4 abs_x, INT4 abs_y, INT4 center_x, INT4 center_y) {
  relative_x = (abs_x - center_x) / GRID_SIZE;
  relative_y = (abs_y - center_y) / GRID_SIZE;
}

__forceinline__ __device__ INT4 get_idx(INT4 x, INT4 y, INT4 width) {
  return x + y * width;
}

__forceinline__ __device__ bool out_of_range(INT4 x, INT4 y) {
  return (x < 0) || (x >= TABLE_SIZE) || (y < 0) || (y >= TABLE_SIZE);
}

template <bool is_sub>
__forceinline__ __device__ void add_contribution(INT4 LUT_x, INT4 LUT_y, INT4 image_idx, float * d_image_r, float * d_image_i, float * d_LUT_r, float * d_LUT_i) {
  INT4 LUT_idx = get_idx(LUT_x, LUT_y, TABLE_SIZE);
  
  if (is_sub) {
      atomicFloatAdd(d_image_r + image_idx, -d_LUT_r[LUT_idx]);
      atomicFloatAdd(d_image_i + image_idx, -d_LUT_i[LUT_idx]);
  } else {
      atomicFloatAdd(d_image_r + image_idx, d_LUT_r[LUT_idx]);
      atomicFloatAdd(d_image_i + image_idx, d_LUT_i[LUT_idx]);
  }
}

template <bool is_sub>
__forceinline__ __device__ void add_contribution(INT4 LUT_x, INT4 LUT_y, INT4 image_pix_idx, int image_size, float * d_image_complex, float * d_LUT) {
  INT4 LUT_pix_idx = get_idx(LUT_x, LUT_y, TABLE_SIZE);

  for (int i = 0; i < KERNEL_NUMBER; ++i) {
    int image_real_idx, image_imaginary_idx;
    get_r_i_idx(image_real_idx, image_imaginary_idx, i, image_size, image_pix_idx);

    int LUT_real_idx, LUT_imaginary_idx;

    get_r_i_idx(LUT_real_idx, LUT_imaginary_idx, i, TABLE_SIZE * TABLE_SIZE, LUT_pix_idx);

    if (is_sub) {
      atomicFloatAdd(d_image_complex + image_real_idx, -d_LUT[LUT_real_idx]);
      atomicFloatAdd(d_image_complex + image_imaginary_idx, -d_LUT[LUT_imaginary_idx]);
    } else {
      atomicFloatAdd(d_image_complex + image_real_idx, d_LUT[LUT_real_idx]);
      atomicFloatAdd(d_image_complex + image_imaginary_idx, d_LUT[LUT_imaginary_idx]);
    }
  }
}

__forceinline__ __device__ void compute_window(Rectangle & window, INT4 r, INT4 c) {
  window.top = r + TABLE_RADIUS + 1;
  window.bottom = r - TABLE_RADIUS;
  window.left = c - TABLE_RADIUS;
  window.right = c + TABLE_RADIUS + 1;
}

__forceinline__ __device__ bool non_overlap(const Rectangle & rect1, const Rectangle & rect2) {
  return ((rect1.top <= rect2.bottom) ||
	  (rect1.bottom >= rect2.top) ||
	  (rect1.right <= rect2.left) ||
	  (rect1.left >= rect2.right));
}

__forceinline__ __device__ void compute_overlap(const Rectangle & rect1, Rectangle & overlap) {
  overlap.top = min(rect1.top, overlap.top);
  overlap.bottom = max(rect1.bottom, overlap.bottom);
  overlap.left = max(rect1.left, overlap.left);
  overlap.right = min(rect1.right, overlap.right);
}

__forceinline__ __device__ void shift_overlap(Rectangle & overlap, int base_bottom, int base_left) {
  overlap.top -= base_bottom;
  overlap.bottom -= base_bottom;
  overlap.left -= base_left;
  overlap.right -= base_left;
}

__forceinline__ __device__ void accumulate_one_pix(int r, int c, const Rectangle & rect, int image_width, float * d_image_r, float * d_image_i, float * d_LUT_r, float * d_LUT_i){
  INT4 image_idx = get_idx(c, r, image_width);
  Rectangle window;
  compute_window(window, r, c);
    
  if (non_overlap(rect, window)) {
    return;		
  }

  int base_bottom = window.bottom + 1;
  int base_left = window.left + 1;

  compute_overlap(rect, window); // window becomes overlap

  shift_overlap(window, base_bottom, base_left);

  // upper right corner
  add_contribution<false>(window.right, window.top, image_idx, d_image_r, d_image_i, d_LUT_r, d_LUT_i);

  bool in_left_bound, in_bottom_bound;

  // lower right corner
  if (in_bottom_bound = (window.bottom >= 0)) {
    add_contribution<true>(window.right, window.bottom, image_idx, d_image_r, d_image_i, d_LUT_r, d_LUT_i);      
  }

  // upper left corner
  if (in_left_bound = (window.left >= 0)) {
    add_contribution<true>(window.left, window.top, image_idx, d_image_r, d_image_i, d_LUT_r, d_LUT_i);
  }

  // lower left corner
  if (in_left_bound && in_bottom_bound) {
    add_contribution<false>(window.left, window.bottom, image_idx, d_image_r, d_image_i, d_LUT_r, d_LUT_i);
  }
}

__forceinline__ __device__ void accumulate_one_pix(int r, int c, const Rectangle & rect, int image_width, int image_size, float * d_image_complex, float * d_LUT){
  INT4 image_idx = get_idx(c, r, image_width);
  Rectangle window;
  compute_window(window, r, c);
    
  if (non_overlap(rect, window)) {
    return;		
  }

  int base_bottom = window.bottom + 1;
  int base_left = window.left + 1;

  compute_overlap(rect, window); // window becomes overlap

  shift_overlap(window, base_bottom, base_left);

  // upper right corner
  add_contribution<false>(window.right, window.top, image_idx, image_size, d_image_complex, d_LUT);

  bool in_left_bound, in_bottom_bound;

  // lower right corner
  if (in_bottom_bound = (window.bottom >= 0)) {
    add_contribution<true>(window.right, window.bottom, image_idx, image_size, d_image_complex, d_LUT);      
  }

  // upper left corner
  if (in_left_bound = (window.left >= 0)) {
    add_contribution<true>(window.left, window.top, image_idx, image_size, d_image_complex, d_LUT);
  }

  // lower left corner
  if (in_left_bound && in_bottom_bound) {
    add_contribution<false>(window.left, window.bottom, image_idx, image_size, d_image_complex, d_LUT);
  }
}

__global__ void kernel_combine_image(float * d_image_r, float * d_image_i, float * d_image, INT4 image_size);
void combine_image(const dim3 & dimGrid, const dim3 & dimBlock, float * d_image_r, float * d_image_i, float * d_image, INT4 image_size);

#endif