running...
Reading polygon list
Reading table list
reading weight list
running gold code...
gold run time is 13.71 s

====== running gold_continuous... ======
comparing result...
Gold 0.00684493 relative error 4.09347e-05
Gold 0.174207 error 2.59231e-06
max error = 2.59231e-06
gold_continuous run time is 3.71 s
speed up is 3.69542 X
running cuda/sse code...
command is 22
command is 22

====== running SSE_1: update the same pixel in different images ======
comparing result...
Gold 0.00626132 relative error 2.66027e-05
Gold 0.164946 error 1.58976e-06
max error = 1.58976e-06
sse_1 run time is 4.28 s
speed up is 3.20327 X

====== running SSE_2: update several pixels in the same image ======
comparing result...
Gold 0.00684493 relative error 4.09347e-05
Gold 0.174207 error 2.59231e-06
max error = 2.59231e-06
sse_2 run time is 2.35 s
speed up is 5.83404 X

====== running SSE_3: update several pixels in the same image ======
comparing result...
Gold 0.00684493 relative error 4.09347e-05
Gold 0.174207 error 2.59231e-06
max error = 2.59231e-06
sse_3 run time is 2.16 s
speed up is 6.34722 X

====== running OMP_GOLD: use OpenMP on gold codes.  ======
comparing result...
Gold 0.00626132 relative error 2.66027e-05
Gold 0.164946 error 1.57486e-06
max error = 1.57486e-06
omp_gold run time is 4.55126 s
speed up is 3.01235 X

====== running OMP_GOLD_CON: use OpenMP on gold_continuous codes.  ======
comparing result...
Gold 0.00684493 relative error 4.10708e-05
Gold 0.174207 error 2.59231e-06
max error = 2.59231e-06
omp_gold_con run time is 0.774322 s
speed up is 17.7058 X

====== running OMP_SSE_1: update the same pixel in different images ======
comparing result...
Gold 0.00626132 relative error 2.66027e-05
Gold 0.164946 error 1.57486e-06
max error = 1.57486e-06
omp_sse_1 run time is 0.812224 s
speed up is 16.8796 X

====== running OMP_SSE_2_TEST: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00684493 relative error 4.09347e-05
Gold 0.174207 error 2.59231e-06
max error = 2.59231e-06
omp_sse_2 run time is 0.698598 s
speed up is 19.625 X

====== running OMP_SSE_2: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00684493 relative error 4.10708e-05
Gold 0.174207 error 2.60721e-06
max error = 2.60721e-06
omp_sse_2 run time is 0.616532 s
speed up is 22.2373 X

====== running OMP_SSE_3: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00684493 relative error 4.10708e-05
Gold 0.174207 error 2.60721e-06
max error = 2.60721e-06
omp_sse_3 run time is 0.50651 s
speed up is 27.0676 X

====== running OMP_SSE_3_1: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00684493 relative error 4.09347e-05
Gold 0.174207 error 2.59231e-06
max error = 2.59231e-06
omp_sse_3_1 run time is 0.697654 s
speed up is 19.6516 X
