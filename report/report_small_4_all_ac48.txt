running...
Reading polygon list
Reading table list
reading weight list
running gold code...
gold run time is 3.66 s

====== running gold_continuous... ======
comparing result...
Gold 0.00504353 relative error 3.21424e-05
Gold 0.133877 error 1.0462e-06
index: 44056
max error = 1.0462e-06
gold_continuous run time is 0.94 s
speed up is 3.89362 X
running cuda/sse code...
command is 22
command is 22

====== running SSE_1: update the same pixel in different images ======
comparing result...
Gold 0.00660337 relative error 3.03784e-05
Gold 0.153024 error 1.69266e-06
index: 65401
max error = 1.69266e-06
sse_1 run time is 1.15 s
speed up is 3.18261 X

====== running SSE_2: update several pixels in the same image ======
comparing result...
Gold 0.00504353 relative error 3.21424e-05
Gold 0.133877 error 1.0462e-06
index: 44056
max error = 1.0462e-06
sse_2 run time is 0.6 s
speed up is 6.1 X

====== running SSE_3: update several pixels in the same image ======
comparing result...
Gold 0.00504353 relative error 3.21424e-05
Gold 0.133877 error 1.0462e-06
index: 44056
max error = 1.0462e-06
sse_3 run time is 0.62 s
speed up is 5.90323 X

====== running OMP_GOLD: use OpenMP on gold codes.  ======
comparing result...
Gold 0.00660337 relative error 3.03784e-05
Gold 0.153024 error 1.67775e-06
index: 65401
max error = 1.67775e-06
omp_gold run time is 0.635095 s
speed up is 5.76292 X

====== running OMP_GOLD_CON: use OpenMP on gold_continuous codes.  ======
comparing result...
Gold 0.00504353 relative error 3.21424e-05
Gold 0.133877 error 1.0313e-06
index: 44056
max error = 1.0313e-06
omp_gold_con run time is 0.179581 s
speed up is 20.3808 X

====== running OMP_SSE_1: update the same pixel in different images ======
comparing result...
Gold 0.00660337 relative error 3.03784e-05
Gold 0.153024 error 1.69266e-06
index: 65401
max error = 1.69266e-06
omp_sse_1 run time is 0.20616 s
speed up is 17.7532 X

====== running OMP_SSE_2_TEST: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00504353 relative error 3.21424e-05
Gold 0.133877 error 1.0462e-06
index: 44056
max error = 1.0462e-06
omp_sse_2 run time is 0.228025 s
speed up is 16.0508 X

====== running OMP_SSE_2: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00504353 relative error 3.20501e-05
Gold 0.133877 error 1.0313e-06
index: 44056
max error = 1.0313e-06
omp_sse_2 run time is 0.141886 s
speed up is 25.7953 X

====== running OMP_SSE_3: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00504353 relative error 3.20501e-05
Gold 0.133877 error 1.0313e-06
index: 44056
max error = 1.0313e-06
omp_sse_3 run time is 0.133402 s
speed up is 27.4358 X

====== running OMP_SSE_3_1: update several pixels in the same image ======
The total # of threads: 12
comparing result...
Gold 0.00504353 relative error 3.21424e-05
Gold 0.133877 error 1.0462e-06
index: 44056
max error = 1.0462e-06
omp_sse_3_1 run time is 0.209203 s
speed up is 17.4949 X
