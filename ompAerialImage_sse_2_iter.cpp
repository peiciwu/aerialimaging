#include <iostream>
#include <time.h>
#include <complex>
#include <xmmintrin.h>
#include <emmintrin.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#ifndef __APPLE__
#include <malloc.h>
#endif
#include <omp.h>
#include <limits.h>

#include "ompAerialImage.h"

using namespace std;

static __m128 zeroAllMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0x0));
static __m128 zeroLeastMask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0xFFffFFff, 0x0));
static __m128 zeroLeast2Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0xFFffFFff, 0x0, 0x0));
static __m128 zeroLeast3Mask = _mm_castsi128_ps(_mm_set_epi32(0xFFffFFff, 0x0, 0x0, 0x0));
static __m128 zeroMostMask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0xFFffFFff, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost2Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0xFFffFFff, 0xFFffFFff));
static __m128 zeroMost3Mask = _mm_castsi128_ps(_mm_set_epi32(0x0, 0x0, 0x0, 0xFFffFFff));
static __m128 zeroLeastMasks[3] = {zeroLeastMask, zeroLeast2Mask, zeroLeast3Mask};
static __m128 zeroMostMasks[3] = {zeroMostMask, zeroMost2Mask, zeroMost3Mask};
static __m128 leaveLeastMasks[4] = {zeroAllMask, zeroMost3Mask, zeroMost2Mask, zeroMostMask};
static __m128 leaveMostMasks[4] = {zeroAllMask, zeroLeast3Mask, zeroLeast2Mask, zeroLeastMask};

// /brief
// Each SSE instruction simultaneously updates several pixels in the same image.
// The framework is similar to "gold_continuous", except that "gold_continuous"
// only updates 1 pixel one time.
// Current implementation is to update 4 pixels at the same time. In order to
// use sse instructions, we have 
// 1. The size of sseImage MUST be a multiple of 4.
//  - Here, the memory padding/allocatio of sseImage is done inside the caller function,
//    i.e. main function, paddedImageSize is the size after padding
// 2. The size of field image MUST be a multiple of 4.
//  - The allocation is the same as sseImage
// 3. When updating field_sse by each row of haloTable, we have the following two
// situations:
//  3.1. If start idx and end idx is the start of a multiple of 4 and end of
//  multiple of 4, do nothing
//  3.2. If NOT,
//      a. (CURRENT IMPLEMENTATION), manually update the remainder at
//      the first and last slot
//      b. padding..........(has to change the allocation of haloTable)
// 3. The size of haloTable MUST be a multiple of 4
//  - pad into a multiple of 4 by inserting to the end of the array
// FIXME. According to impactRegion, allocate smaller haloTable???? But we
// cannot allocate a single haloTable that works on all rectangels...
void ompPrintImage_sse_2_iter(Rectangle* polygonArray,
                   INT4 rectNum,
                   float* weightArray,
                   float** tableArray,
                   float* &sseImage,
                   float &ompTime,
                   Rectangle boundBox,
                   int paddedImageSize)
{
  omp_set_num_threads(12);
  #pragma omp parallel
  {
    if (omp_get_thread_num() == 0)
      cout << "The total # of threads: " << omp_get_num_threads() << endl;
  }
  double ompTimer = omp_get_wtime();
  //clock_t startTime = clock();

  int boundwidth = (boundBox.right - boundBox.left) >> 2; // divide by GRID_SIZE(4)
  int boundheight = (boundBox.top - boundBox.bottom) >> 2;
  // Find the left/right/top/bottom-most boundary among all rectangels.
  Rectangle rectBound;
  rectBound.bottom = rectBound.left = 0; //(bottom-RADIUS) is always <= 0
  rectBound.top = rectBound.right = INT_MIN;
  for (int i = 0; i < rectNum; ++i) {
    if (polygonArray[i].right > rectBound.right)
      rectBound.right = polygonArray[i].right;
    if (polygonArray[i].top > rectBound.top)
      rectBound.top = polygonArray[i].top;
  }
  rectBound.top = (rectBound.top - boundBox.bottom) >> 2;
  rectBound.right = (rectBound.right - boundBox.left) >> 2;
  rectBound.top = min(boundheight + TABLE_RADIUS + 1, rectBound.top) + TABLE_RADIUS;
  rectBound.right = min(boundwidth + TABLE_RADIUS + 1, rectBound.right) + TABLE_RADIUS;
  int haloWidth = rectBound.right;
  int haloHeight = rectBound.top;
  // We want haloWidth%4 == boundwidth%4
  haloWidth += (boundwidth%4 - haloWidth%4 + 4)%4;
  int haloTotalSize = haloWidth * haloHeight;

  //DEBUG......
  double memTime = omp_get_wtime();
  // Create ONE halo-lookuptable that works for all rectangles. LUT is extended into
  // imageSize by adding halo to LUT. Halo is assigned boundary values.
  // 0: no shift, 1: shift right one index, 2: shift right two, 3: shift right three
  // Allocate and initialize haloTable[0]. The origianl haloTable but is
  // reversed.
  float* haloTableArray[PADDED_TABLE_NUMBER][4];
  int h = min(TABLE_SIZE, haloHeight);
  int w = min(TABLE_SIZE, haloWidth);
  #pragma omp parallel for
  for (int i = 0; i < TABLE_NUMBER; ++i) {
    for (int j = 0; j < 4; ++j)
      haloTableArray[i][j] = (float*) memalign(16, sizeof(float) * (haloTotalSize+j));
    // For the origianl rows >= TABLE_SIZE, now they start from row 0
    if (h != haloHeight) {
      float* upperBoundaryRow = (float*) malloc(sizeof(float)*haloWidth);
      // Set corner boundary value first, i.e. x >= TABLE_SIZE && y >=
      // TABLE_SIZE
      fill_n(upperBoundaryRow, haloWidth - w, tableArray[i][TABLE_SIZE*TABLE_SIZE-1]);
      // Reverse the upper boundary table row
      for (int x = haloWidth-w, tx = w-1; x < haloWidth; ++x, --tx) 
        upperBoundaryRow[x] = tableArray[i][(TABLE_SIZE-1)*TABLE_SIZE+tx];
      for (int y = 0; y < haloHeight-h; ++y) 
        memcpy(haloTableArray[i][0]+y*haloWidth, upperBoundaryRow, sizeof(float)*haloWidth);
      free(upperBoundaryRow);
    }
    // For the original rows from 0 to TABLE_SIZE-1, now they start from
    // haloHeight-h
    // (tx, ty) is for the reversed table index
    for (int y = haloHeight-h, ty = h - 1; y < haloHeight; ++y, --ty) {
      // Set boundary values first, i.e. x >= TABLE_SIZE
      fill_n(haloTableArray[i][0]+y*haloWidth, haloWidth - w, tableArray[i][ty*TABLE_SIZE+TABLE_SIZE-1]);
      for (int x = haloWidth - w, tx = w - 1; x < haloWidth; ++x, --tx)
        haloTableArray[i][0][y*haloWidth + x] = tableArray[i][ty*TABLE_SIZE+tx];
    }
    // Allocate and initialize other haloTables
    for (int j = 1; j < 4; ++j) {
      memset(haloTableArray[i][j], 0, sizeof(float)*j);
      memcpy(haloTableArray[i][j]+j, haloTableArray[i][0], sizeof(float)*haloTotalSize);
    }
  }
  memTime = omp_get_wtime() - memTime;

  __m128* image_sse = (__m128*) sseImage; //stored in reverse order.....
  int sz_sse = paddedImageSize >> 2;

  float* field[12];
  #pragma omp parallel for
  for (int i = 0; i < 12; ++i) {
//#ifdef __APPLE__
    field[i] = (float *) malloc(sizeof(float) * paddedImageSize);
  }

  for (int iter = 0; iter < ITER_NUM; ++iter) {
    #pragma omp parallel for
    for (int i = 0; i < sz_sse; i++) {
      image_sse[i] = _mm_setzero_ps();
    }
  //#pragma omp parallel
  #pragma omp parallel firstprivate(paddedImageSize)
  {
#if 0
    float* field; //is an array of paddedImageSizee
//#ifdef __APPLE__
    field = (float *) malloc(sizeof(float) * paddedImageSize);
#endif
    __m128* field_sse = (__m128*) field[omp_get_thread_num()]; //stored in reverse order.....

    Rectangle polygonImage;

    //#pragma omp for
    #pragma omp for private(polygonImage) firstprivate(haloWidth, haloHeight, haloTotalSize, boundBox, boundwidth, boundheight)
    for (int tableIdx = 0; tableIdx < TABLE_NUMBER; tableIdx++) {
      int weightIdx = tableIdx >> 1;

      for (int s = 0, i = 0; s < paddedImageSize; s += 4, i++) {
        field_sse[i] = _mm_setzero_ps(); 
      }

      __m128* table_sse[4];
      table_sse[0] = (__m128*) haloTableArray[tableIdx][0];
      table_sse[1] = (__m128*) haloTableArray[tableIdx][1];
      table_sse[2] = (__m128*) haloTableArray[tableIdx][2];
      table_sse[3] = (__m128*) haloTableArray[tableIdx][3];

      for (int i = 0; i < rectNum; i++) {
        // Note that all polygons are inside the left and bottom boundary of the
        // aerial image, but some of them exceed the right and top boundary.
        polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom) >> 2;
        polygonImage.top    = (polygonArray[i].top    - boundBox.bottom) >> 2;
        polygonImage.left   = (polygonArray[i].left   - boundBox.left)   >> 2;
        polygonImage.right  = (polygonArray[i].right  - boundBox.left)   >> 2;
        // Ignore the polygon that is totally outside the aerial image
        if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
          continue;
        // The polygon's top/right boundary over some limit is meaningless, i.e.
        // LUT won't be used to check those pixels. The limit is as follows:
        // FIXME. NOT SURE....
        // (boundheight-1) (the right most pixel in the aerial image) +
        // TABLE_RADIUS + 1.
        polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
        polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

        pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
        corners[0] = make_pair(polygonImage.right, polygonImage.top);
        corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
        corners[2] = make_pair(polygonImage.left, polygonImage.top);
        corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

        Rectangle impactRegion;
        impactRegion.bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
        impactRegion.top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
        impactRegion.left = max(polygonImage.left - KERNEL_RADIUS, 0);
        impactRegion.right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);

        // top-right and bottom-left corners
        for (int c = 0; c < 2; ++c) {
          // (k, j): those pixels impacted by the polygon i
          // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
          int j = impactRegion.bottom;
          int k = impactRegion.left;
          int rely = corners[c].second - (impactRegion.bottom - TABLE_RADIUS) - 1; 
          int relx = corners[c].first - (impactRegion.left - TABLE_RADIUS) - 1;
          int ySteps = min(impactRegion.top - impactRegion.bottom, rely+1);
          int xSteps = min(impactRegion.right - impactRegion.left, relx+1);
          if (xSteps <= 0)
            continue;

          // In order to align the field_sse and haloTable_sse, we need to shift
          // the haloTable 'numShifted' entries by using the pre-allocated
          // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
          // of rectangle, this shift value is fixed no matter which pixel is
          // updated now.
          // start_field: j*boundwidth+k
          // start_table: haloTotalSize-(rely*haloWidth+relx)-1
          // numshift: (start_field%4 - start_table%4 + 4) % 4
          int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
          for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
            int start_field = (fj + k); // start index of field 
            int start_field_sse = (fj + k) >> 2; // start index of field_sse
            int end_field_sse = (fj + k + xSteps - 1) >> 2;
            //int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
            //int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
            int numFirstPadded_field = (fj + k) - (start_field_sse<<2);
            int numLastPadded_field = 3 - ((fj+k+xSteps-1)-(end_field_sse<<2));
            int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
            start_table += numShifted;
            int start_table_sse = start_table >> 2; //start index of table_sse

            int ti = start_table_sse;
            for (int fi = start_field_sse; fi <= end_field_sse; ++fi, ++ti)
              field_sse[fi] = _mm_add_ps(field_sse[fi], table_sse[numShifted][ti]);
            // Undo the paddings in the first slot
            __m128 sub = _mm_and_ps(table_sse[numShifted][start_table_sse], leaveLeastMasks[numFirstPadded_field]); 
            field_sse[start_field_sse] = _mm_sub_ps(field_sse[start_field_sse], sub);
            // Undo the paddings in the last slot
            //int end_table_sse = (start_table + xSteps - 1) / 4;
            sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
            field_sse[end_field_sse] = _mm_sub_ps(field_sse[end_field_sse], sub);

            fj += boundwidth;
            ty--;
          }
        }

        // top-left and bottom-right corners
        for (int c = 2; c < 4; ++c) {
          // (k, j): those pixels impacted by the polygon i
          // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
          int j = impactRegion.bottom;
          int k = impactRegion.left;
          int rely = corners[c].second - (impactRegion.bottom - TABLE_RADIUS) - 1; 
          int relx = corners[c].first - (impactRegion.left - TABLE_RADIUS) - 1;
          int ySteps = min(impactRegion.top - impactRegion.bottom, rely+1);
          int xSteps = min(impactRegion.right - impactRegion.left, relx+1);
          if (xSteps <= 0)
            continue;

          // In order to align the field_sse and haloTable_sse, we need to shift
          // the haloTable 'numShifted' entries by using the pre-allocated
          // haloTables. Moreover, since boundwidth%4 == halowidth%4, for a corner
          // of rectangle, this shift value is fixed no matter which pixel is
          // updated now.
          // start_field: j*boundwidth+k
          // start_table: haloTotalSize-(rely*haloWidth+relx)-1
          // numshift: (start_field%4 - start_table%4 + 4) % 4
          int numShifted = ((j*boundwidth+k)%4 - (haloTotalSize-(rely*haloWidth+relx)-1)%4 + 4)%4;
          for (int ys = 0, fj = j*boundwidth, ty = rely; ys < ySteps; ++ys) {
            int start_field = (fj + k); // start index of field 
            int start_field_sse = (fj + k) >> 2; // start index of field_sse
            int end_field_sse = (fj + k + xSteps - 1) >> 2;
            //int numFirstPadded_field = (fj + k) % 4; // number of the padded (0s) in the first slot in field_sse
            //int numLastPadded_field = 3 - ((fj + k + xSteps - 1) % 4); // number of the padded (0s) in the last slot
            int numFirstPadded_field = (fj + k) - (start_field_sse<<2);
            int numLastPadded_field = 3 - ((fj+k+xSteps-1)-(end_field_sse<<2));
            int start_table = haloTotalSize - (ty * haloWidth + relx) - 1; // start index of haloTable[0] (ty*haloWidth + relx is start index of tableArray)
            start_table += numShifted;
            int start_table_sse = start_table >> 2; //start index of table_sse

            int ti = start_table_sse;
            for (int fi = start_field_sse; fi <= end_field_sse; ++fi, ++ti)
              field_sse[fi] = _mm_sub_ps(field_sse[fi], table_sse[numShifted][ti]);
            // Undo the paddings in the first slot
            __m128 sub = _mm_and_ps(table_sse[numShifted][start_table_sse], leaveLeastMasks[numFirstPadded_field]); 
            field_sse[start_field_sse] = _mm_add_ps(field_sse[start_field_sse], sub);
            // Undo the paddings in the last slot
            //int end_table_sse = (start_table + xSteps - 1) / 4;
            sub = _mm_and_ps(table_sse[numShifted][ti-1], leaveMostMasks[numLastPadded_field]);
            field_sse[end_field_sse] = _mm_add_ps(field_sse[end_field_sse], sub);

            fj += boundwidth;
            ty--;
          }
        }
      }
      int sz_sse = paddedImageSize>>2;
      float weightValue = weightArray[weightIdx];
      #pragma omp critical
      for (int i = 0; i < sz_sse; i++) {
        __m128 square = _mm_mul_ps(field_sse[i], field_sse[i]);
        __m128 weight_sse = _mm_set_ps(weightValue, weightValue, weightValue, weightValue);
        __m128 tmp = _mm_mul_ps(square, weight_sse);
        image_sse[i] = _mm_add_ps(image_sse[i], tmp);
      }
    }
    //free(field);
  }
  }
  
  #pragma omp parallel for
  for (int i = 0; i < 12; ++i)
    free(field[i]);

  double freememTime = omp_get_wtime();
  for (int i = 0; i < TABLE_NUMBER; ++i) {
    for (int j = 0; j < 4; ++j)
      free(haloTableArray[i][j]);
  }
  freememTime = omp_get_wtime() - freememTime;

  //ompTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
  ompTime = omp_get_wtime() - ompTimer;
  //debug
  //ompTime = ompTime - memTime - freememTime;
  cout << "totalTime:" << ompTime << ", memTime: " << memTime << ", freememtime: " << freememTime << endl;
}

