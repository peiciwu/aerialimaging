#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <utility>
#include <iostream>
#include <omp.h>
#include "ompAerialImage.h"

using namespace std;

// /brief 
// Each corner is computed its relative position inside the LUT only once. Every
// pixel is updated continuously from the left to right in each row, and its corner
// coordinates is updated accordingly.
void ompPrintImage_gold_con(Rectangle* polygonArray,
  INT4 rectNum,
  float* weightArray,
  DTYPE** tableArray,
  DTYPE* &ompImage,
  float &ompTime,
  Rectangle boundBox) 
{
  omp_set_num_threads(NUM_THREADS);
  double ompTimer = omp_get_wtime();
  //clock_t startTime = clock();

  int boundwidth = (boundBox.right - boundBox.left) / GRID_SIZE;
  int boundheight = (boundBox.top - boundBox.bottom) / GRID_SIZE;

  for (int i = 0; i < boundwidth * boundheight; i++){
    ompImage[i] = 0;
  }

  #pragma omp parallel
  {
    DTYPE * field_real = (DTYPE *) malloc(sizeof(DTYPE)*boundwidth*boundheight);
    DTYPE * field_imag = (DTYPE *) malloc(sizeof(DTYPE)*boundwidth*boundheight);

    Rectangle polygonImage;

    #pragma omp for
    for (int weightIdx = 0; weightIdx < KERNEL_NUMBER; weightIdx++) {
      int kernelIdx = weightIdx << 1;
      for (int i = 0; i < boundwidth * boundheight; i++) {
        field_real[i] = 0;
        field_imag[i] = 0;
      }

      // Create ONE halo-lookuptable that works for all rectangles. LUT is extended into
      // imageSize by adding halo to LUT. Halo is assigned boundary values.
      int haloWidth = boundwidth + TABLE_SIZE;
      int haloHeight = boundheight + TABLE_SIZE; 
      DTYPE* haloTable1 = (DTYPE*) malloc(sizeof(DTYPE) * haloWidth * haloHeight);
      DTYPE* haloTable2 = (DTYPE*) malloc(sizeof(DTYPE) * haloWidth * haloHeight);
      int h = min(TABLE_SIZE, haloHeight);
      int w = min(TABLE_SIZE, haloWidth);
      for (int y = 0; y < h; ++y) {
        memcpy(haloTable1+y*haloWidth, tableArray[kernelIdx]+y*TABLE_SIZE, sizeof(DTYPE)*w);
        memcpy(haloTable2+y*haloWidth, tableArray[kernelIdx+1]+y*TABLE_SIZE, sizeof(DTYPE)*w);
      }   
      // Set boundary values to halo
      for (int y = 0; y < h; ++y) {
        fill_n(haloTable1+y*haloWidth+w, haloWidth - w, tableArray[kernelIdx][y*TABLE_SIZE+TABLE_SIZE-1]);
        fill_n(haloTable2+y*haloWidth+w, haloWidth - w, tableArray[kernelIdx+1][y*TABLE_SIZE+TABLE_SIZE-1]);
      }   
      if (h != haloHeight) {
        DTYPE* upperBoundaryRow1 = (DTYPE*) malloc(sizeof(DTYPE)*haloWidth);
        DTYPE* upperBoundaryRow2 = (DTYPE*) malloc(sizeof(DTYPE)*haloWidth);
        int x = 0;
        for (; x < w; ++x) {
          upperBoundaryRow1[x] = tableArray[kernelIdx][(TABLE_SIZE-1)*TABLE_SIZE+x];
          upperBoundaryRow2[x] = tableArray[kernelIdx+1][(TABLE_SIZE-1)*TABLE_SIZE+x];
        }
        for (; x < haloWidth; ++x) {
          upperBoundaryRow1[x] = tableArray[kernelIdx][TABLE_SIZE*TABLE_SIZE-1];
          upperBoundaryRow2[x] = tableArray[kernelIdx+1][TABLE_SIZE*TABLE_SIZE-1];
        }
        for (int y = h; y < haloHeight; ++y) {
          memcpy(haloTable1+y*haloWidth, upperBoundaryRow1, sizeof(DTYPE)*haloWidth);
          memcpy(haloTable2+y*haloWidth, upperBoundaryRow2, sizeof(DTYPE)*haloWidth);
        }
        free(upperBoundaryRow1);
        free(upperBoundaryRow2);
      }

      for (int i = 0; i < rectNum; i++) {
        // Note that all polygons are inside the left and bottom boundary of the
        // aerial image, but some of them exceed the right and top boundary.
        polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom)/GRID_SIZE;
        polygonImage.top    = (polygonArray[i].top    - boundBox.bottom)/GRID_SIZE;
        polygonImage.left   = (polygonArray[i].left   - boundBox.left)  /GRID_SIZE;
        polygonImage.right  = (polygonArray[i].right  - boundBox.left)  /GRID_SIZE;
        // Ignore the polygon that is totally outside the aerial image
        if (polygonImage.bottom > boundheight + TABLE_RADIUS || polygonImage.left > boundwidth + TABLE_RADIUS)
          continue;
        // The polygon's top/right boundary over some limit is meaningless, i.e.
        // LUT won't be used to check those pixels. The limit is as follows:
        // FIXME. NOT SURE....
        // (boundheight-1) (the right most pixel in the aerial image) +
        // TABLE_RADIUS + 1.
        polygonImage.top = min(polygonImage.top, boundheight + TABLE_RADIUS + 1);
        polygonImage.right = min(polygonImage.right, boundwidth + TABLE_RADIUS + 1);

        pair<int, int> corners[4];//0: top-right, 1: bottom-left, 2: top-left, 3: bottom-right
        corners[0] = make_pair(polygonImage.right, polygonImage.top);
        corners[1] = make_pair(polygonImage.left, polygonImage.bottom);
        corners[2] = make_pair(polygonImage.left, polygonImage.top);
        corners[3] = make_pair(polygonImage.right, polygonImage.bottom);

        Rectangle impactRegion;
        impactRegion.bottom = max(polygonImage.bottom - KERNEL_RADIUS, 0);
        impactRegion.top = min(polygonImage.top + KERNEL_RADIUS, boundheight);
        impactRegion.left = max(polygonImage.left - KERNEL_RADIUS, 0);
        impactRegion.right = min(polygonImage.right + KERNEL_RADIUS, boundwidth);


        // top-left and bottom-right corners
        for (int c = 2; c < 4; ++c) {
          // (k, j): those pixels impacted by the polygon i
          // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
          int j = impactRegion.bottom;
          int k = impactRegion.left;
          int rely = corners[c].second - (impactRegion.bottom - TABLE_RADIUS) - 1; 
          int relx = corners[c].first - (impactRegion.left - TABLE_RADIUS) - 1;
          int ySteps = min(impactRegion.top - impactRegion.bottom, rely+1);
          int xSteps = min(impactRegion.right - impactRegion.left, relx+1);
          for (int ys = 0; ys < ySteps; ++ys)
            for (int xs = 0; xs < xSteps; ++xs) {
              field_real[(j+ys) * boundwidth + (k+xs)] -= haloTable1[(rely - ys) * haloWidth + (relx - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] -= haloTable2[(rely - ys) * haloWidth + (relx - xs)];
            }
        }
        // top-right and bottom-left corners
        for (int c = 0; c < 2; ++c) {
          // (k, j): those pixels impacted by the polygon i
          // (relx, rely): the relative position of the corner for the LUT where (k, j) is the center pixel
          int j = impactRegion.bottom;
          int k = impactRegion.left;
          int rely = corners[c].second - (impactRegion.bottom - TABLE_RADIUS) - 1; 
          int relx = corners[c].first - (impactRegion.left - TABLE_RADIUS) - 1;
          int ySteps = min(impactRegion.top - impactRegion.bottom, rely+1);
          int xSteps = min(impactRegion.right - impactRegion.left, relx+1);
          for (int ys = 0; ys < ySteps; ++ys)
            for (int xs = 0; xs < xSteps; ++xs) {
              field_real[(j+ys) * boundwidth + (k+xs)] += haloTable1[(rely - ys) * haloWidth + (relx - xs)];
              field_imag[(j+ys) * boundwidth + (k+xs)] += haloTable2[(rely - ys) * haloWidth + (relx - xs)];
            }
        }
      }
      free(haloTable1);
      free(haloTable2);

      #pragma omp critical
      for (int i=0;i<boundwidth*boundheight;i++) {
        ompImage[i]+=((DTYPE)weightArray[weightIdx])*(field_real[i]*field_real[i]+field_imag[i]*field_imag[i]);
      }
    }
    free(field_real);
    free(field_imag);

  }

  //ompTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
  ompTime = omp_get_wtime() - ompTimer;
}


