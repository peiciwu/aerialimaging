#ifndef CUDAAERIALIMAGE_LUT_1_H
#define CUDAAERIALIMAGE_LUT_1_H

#include"datastruct.h"

void cudaPrintImage_LUT_1(Rectangle * polygonArray,
		    INT4 rectNum,
		    float * weightArray,
		    float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
		    Rectangle & boundBox,
		    float * cudaImage,
		    float & cudaTime);


#endif
