#include <complex>
#include "goldAerialImage.h"
#include "fft.h"
#include <time.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

bool overlapping(Rectangle& area, int x, int y, Rectangle polygonImage);

int max(int a, int b)
{
    return (a>b)?a:b;
}

int min(int a, int b)
{
    return (a<b)?a:b;
}

void goldPrintImage(Rectangle* polygonArray,
	INT4 rectNum,
	float* weightArray,
	float tableArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	float* &goldImage,
	float &goldTime,
	Rectangle boundBox)
{
    // your code starts here

    clock_t startTime = clock();

    int boundwidth = (boundBox.right-boundBox.left) / GRID_SIZE;
    int boundheight = (boundBox.top-boundBox.bottom) / GRID_SIZE;

    double * field_real1 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag1 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real2 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag2 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real3 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag3 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real4 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag4 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real5 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag5 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real6 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag6 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real7 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag7 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real8 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag8 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real9 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag9 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real10 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag10 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real11 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag11 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_real12 = (double *) malloc(sizeof(double)*boundwidth*boundheight);
    double * field_imag12 = (double *) malloc(sizeof(double)*boundwidth*boundheight);


    Rectangle polygonImage;
    Rectangle overlapRegion;

    for (int i=0;i<boundwidth*boundheight;i++){
	goldImage[i] = 0;
    }

    for (int weightIdx=0, kernelIdx=0; weightIdx<KERNEL_NUMBER; weightIdx++, kernelIdx+=2){
	for (int i=0;i<boundwidth*boundheight;i++){
	    field_real1[i] = 0;
	    field_imag1[i] = 0;
	    field_real2[i] = 0;
	    field_imag2[i] = 0;
	    field_real3[i] = 0;
	    field_imag3[i] = 0;
	    field_real4[i] = 0;
	    field_imag4[i] = 0;
	    field_real5[i] = 0;
	    field_imag5[i] = 0;
	    field_real6[i] = 0;
	    field_imag6[i] = 0;
	    field_real7[i] = 0;
	    field_imag7[i] = 0;
	    field_real8[i] = 0;
	    field_imag8[i] = 0;
	    field_real9[i] = 0;
	    field_imag9[i] = 0;
	    field_real10[i] = 0;
	    field_imag10[i] = 0;
	    field_real11[i] = 0;
	    field_imag11[i] = 0;
	    field_real12[i] = 0;
	    field_imag12[i] = 0;
	}

	for (int i=0;i < rectNum;i++){
	    polygonImage.bottom = (polygonArray[i].bottom - boundBox.bottom)/GRID_SIZE;
	    polygonImage.top    = (polygonArray[i].top    - boundBox.bottom)/GRID_SIZE;
	    polygonImage.left   = (polygonArray[i].left   - boundBox.left)  /GRID_SIZE;
	    polygonImage.right  = (polygonArray[i].right  - boundBox.left)  /GRID_SIZE;

	    // for the topright corner
	    for (int j = max(polygonImage.bottom-KERNEL_RADIUS, 0); j < min(polygonImage.top+KERNEL_RADIUS, boundheight);++j)
		for (int k = max(polygonImage.left-KERNEL_RADIUS,0); k < min(polygonImage.right+KERNEL_RADIUS, boundwidth);++k)
		    if(overlapping(overlapRegion, k, j, polygonImage)){
			int index = j*boundwidth+k;

			switch (index%12) {
			    case 0:
				field_real1[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag1[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real1[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag1[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real1[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag1[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real1[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag1[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 1:
				field_real2[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag2[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real2[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag2[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real2[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag2[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real2[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag2[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 2:
				field_real3[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag3[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real3[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag3[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real3[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag3[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real3[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag3[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 3:
				field_real4[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag4[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real4[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag4[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real4[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag4[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real4[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag4[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 4:
				field_real5[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag5[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real5[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag5[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real5[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag5[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real5[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag5[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 5:
				field_real6[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag6[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real6[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag6[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real6[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag6[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real6[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag6[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 6:
				field_real7[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag7[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real7[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag7[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real7[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag7[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real7[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag7[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 7:
				field_real8[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag8[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real8[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag8[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real8[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag8[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real8[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag8[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 8:
				field_real9[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag9[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real9[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag9[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real9[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag9[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real9[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag9[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 9:
				field_real10[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag10[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real10[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag10[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real10[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag10[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real10[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag10[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 10:
				field_real11[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag11[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real11[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag11[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real11[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag11[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real11[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag11[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			    case 11:
				field_real12[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				field_imag12[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.right];
				if (overlapRegion.left!=-1){
				    field_real12[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				    field_imag12[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.top*TABLE_SIZE+overlapRegion.left];
				}
				if (overlapRegion.bottom!=-1){
				    field_real12[j*boundwidth+k]-=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				    field_imag12[j*boundwidth+k]-=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.right];
				}
				if (overlapRegion.bottom != -1 && overlapRegion.left != -1){
				    field_real12[j*boundwidth+k]+=tableArray[kernelIdx][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				    field_imag12[j*boundwidth+k]+=tableArray[kernelIdx+1][overlapRegion.bottom*TABLE_SIZE+overlapRegion.left];
				}
				break;
			}
		    }
	}
	
	for (int i=0;i<boundwidth*boundheight;i++){
	    field_real1[i]+=(field_real2[i]+
		    field_real3[i]+
		    field_real4[i]+
		    field_real5[i]+
		    field_real6[i]+
		    field_real7[i]+
		    field_real8[i]+
		    field_real9[i]+
		    field_real10[i]+
		    field_real11[i]+
		    field_real12[i]);
	    field_imag1[i]+=(field_imag2[i]+
		    field_imag3[i]+
		    field_imag4[i]+
		    field_imag5[i]+
		    field_imag6[i]+
		    field_imag7[i]+
		    field_imag8[i]+
		    field_imag9[i]+
		    field_imag10[i]+
		    field_imag11[i]+
		    field_imag12[i]);
	}

	for (int i=0;i<boundwidth*boundheight;i++){
	    goldImage[i]+=weightArray[weightIdx]*(field_real1[i]*field_real1[i]+field_imag1[i]*field_imag1[i]);
	}
    }
    free(field_real1);
    free(field_imag1);
    free(field_real2);
    free(field_imag2);
    free(field_real3);
    free(field_imag3);
    free(field_real4);
    free(field_imag4);
    free(field_real5);
    free(field_imag5);
    free(field_real6);
    free(field_imag6);
    free(field_real7);
    free(field_imag7);
    free(field_real8);
    free(field_imag8);
    free(field_real9);
    free(field_imag9);
    free(field_real10);
    free(field_imag10);
    free(field_real11);
    free(field_imag11);
    free(field_real12);
    free(field_imag12);

    goldTime = (clock()-startTime)/(double)CLOCKS_PER_SEC;
}

bool overlapping(Rectangle& area, int x, int y, Rectangle polygonImage)
{
    int tableTop, tableBottom, tableLeft, tableRight;
    tableTop    = y+TABLE_RADIUS+1;
    tableBottom = y-TABLE_RADIUS;
    tableLeft   = x-TABLE_RADIUS;
    tableRight  = x+TABLE_RADIUS+1;

    if (polygonImage.top <= tableBottom ||		// modified here
	    polygonImage.bottom >= tableTop ||
	    polygonImage.left >= tableRight ||
	    polygonImage.right <= tableLeft)		// modified here
	return false;

    area.top = min(tableTop, polygonImage.top)-tableBottom-1;     // modified here
    area.bottom = max(tableBottom, polygonImage.bottom)-tableBottom-1;    // modified here
    area.left = max(tableLeft, polygonImage.left)-tableLeft-1;	    // modified here
    area.right = min(tableRight, polygonImage.right)-tableLeft-1;	// modified here

    return true;
}



