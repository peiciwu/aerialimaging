CC = g++
TCMALLOC_FLAGS = -fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free # for tcmalloc on x86-64
CPPFLAGS = -g -m64 -O2 -march=native #$(TCMALLOC_FLAGS) #-m64 -O2 -march=native -funroll-loops#-Ofast -flto -march=native -funroll-loops#-O is needed in Mac OS
OMPFLAGS = -fopenmp
SSEFLAGS = -msse4.1
TCMALLOC_LINK = #-L/home/peiciwu1/gperftools/lib -ltcmalloc
LINK = -lgomp $(TCMALLOC_LINK)#-lomp is needed for openMP/ export OMP_NUM_THREADS=NUM; -ltcmalloc is for mutli-threading tcmalloc
DEFINE = 
BUILDDIR = build

SRC = main.cpp goldAerialImage.cpp goldAerialImage_continuous.cpp goldAerialImage_continuous_2.cpp sseAerialImage_1.cpp sseAerialImage_2.cpp ompAerialImage_gold.cpp ompAerialImage_gold_continuous.cpp ompAerialImage_sse_1.cpp ompAerialImage_sse_2.cpp ompAerialImage_sse_2_test.cpp ompAerialImage_sse_2_iter.cpp ompAerialImage_sse_3.cpp ompAerialImage_sse_3_1.cpp sseAerialImage_3.cpp sseAerialImage_2_2.cpp sseAerialImage_3_2.cpp 
HEAD = datastruct.h goldAerialImage.h sseAerialImage.h ompAerialImage.h
#OBJ = $(BUILDDIR)/goldAerialImage.o $(BUILDDIR)/goldAerialImage_continuous.o $(BUILDDIR)/goldAerialImage_continuous_2.o $(BUILDDIR)/sseAerialImage_1.o $(BUILDDIR)/sseAerialImage_2.o $(BUILDDIR)/ompAerialImage_gold.o $(BUILDDIR)/ompAerialImage_gold_continuous.o $(BUILDDIR)/ompAerialImage_sse_1.o $(BUILDDIR)/ompAerialImage_sse_2.o $(BUILDDIR)/ompAerialImage_sse_2_test.o $(BUILDDIR)/ompAerialImage_sse_2_iter.o $(BUILDDIR)/ompAerialImage_sse_3.o $(BUILDDIR)/ompAerialImage_sse_3_1.o $(BUILDDIR)/sseAerialImage_3.o $(BUILDDIR)/sseAerialImage_3_2.o $(BUILDDIR)/sseAerialImage_2_2.o $(BUILDDIR)/main.o
OBJ = $(BUILDDIR)/goldAerialImage.o $(BUILDDIR)/goldAerialImage_continuous.o $(BUILDDIR)/goldAerialImage_continuous_2.o $(BUILDDIR)/sseAerialImage_1.o $(BUILDDIR)/sseAerialImage_2.o $(BUILDDIR)/ompAerialImage_gold.o $(BUILDDIR)/ompAerialImage_gold_continuous.o $(BUILDDIR)/ompAerialImage_sse_1.o $(BUILDDIR)/ompAerialImage_sse_2.o $(BUILDDIR)/ompAerialImage_sse_2_test.o $(BUILDDIR)/ompAerialImage_sse_2_iter.o $(BUILDDIR)/ompAerialImage_sse_3.o $(BUILDDIR)/ompAerialImage_sse_3_1.o $(BUILDDIR)/sseAerialImage_3.o $(BUILDDIR)/sseAerialImage_2_2.o $(BUILDDIR)/main.o $(BUILDDIR)/sseAerialImage_4.o $(BUILDDIR)/ompAerialImage_sse_2_2.o $(BUILDDIR)/ompAerialImage_sse_2_2_test.o $(BUILDDIR)/ompAerialImage_sse_4.o $(BUILDDIR)/ompAerialImage_sse_4_test.o $(BUILDDIR)/ompAerialImage_sse_1_test.o $(BUILDDIR)/ompAerialImage_sse_4_test_double.o
EXE = $(BUILDDIR)/aerial
DEBUG_EXE = $(BUILDDIR)/aerial_debug 

default : $(OBJ) 
	$(CC) $(DEFINE) $(CPPFLAGS) $(LINK) -o $(EXE) $(OBJ) 

debug : DEFINE = -DDDEBUG
debug : $(OBJ)
	$(CC) $(DEFINE) $(CPPFLAGS) -o $(DEBUG_EXE) $(OBJ)

$(BUILDDIR)/goldAerialImage.o: goldAerialImage.cpp datastruct.h goldAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c goldAerialImage.cpp -o $(BUILDDIR)/goldAerialImage.o $(TCMALLOC_LINK)

$(BUILDDIR)/goldAerialImage_continuous.o: goldAerialImage_continuous.cpp datastruct.h goldAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c goldAerialImage_continuous.cpp -o $(BUILDDIR)/goldAerialImage_continuous.o $(TCMALLOC_LINK)

$(BUILDDIR)/goldAerialImage_continuous_2.o: goldAerialImage_continuous_2.cpp datastruct.h goldAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c goldAerialImage_continuous_2.cpp -o $(BUILDDIR)/goldAerialImage_continuous_2.o $(TCMALLOC_LINK)

$(BUILDDIR)/sseAerialImage_1.o: sseAerialImage_1.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(SSEFLAGS) -c sseAerialImage_1.cpp -o $(BUILDDIR)/sseAerialImage_1.o $(TCMALLOC_LINK)

$(BUILDDIR)/sseAerialImage_2.o: sseAerialImage_2.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c sseAerialImage_2.cpp -o $(BUILDDIR)/sseAerialImage_2.o $(TCMALLOC_LINK)

$(BUILDDIR)/sseAerialImage_2_2.o: sseAerialImage_2_2.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c sseAerialImage_2_2.cpp -o $(BUILDDIR)/sseAerialImage_2_2.o $(TCMALLOC_LINK)

$(BUILDDIR)/sseAerialImage_3.o: sseAerialImage_3.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c sseAerialImage_3.cpp -o $(BUILDDIR)/sseAerialImage_3.o $(TCMALLOC_LINK)

$(BUILDDIR)/sseAerialImage_3_2.o: sseAerialImage_3_2.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c sseAerialImage_3_2.cpp -o $(BUILDDIR)/sseAerialImage_3_2.o $(TCMALLOC_LINK)

$(BUILDDIR)/sseAerialImage_4.o: sseAerialImage_4.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(SSEFLAGS) -c sseAerialImage_4.cpp -o $(BUILDDIR)/sseAerialImage_4.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_gold.o: ompAerialImage_gold.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_gold.cpp -o $(BUILDDIR)/ompAerialImage_gold.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_gold_continuous.o: ompAerialImage_gold_continuous.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_gold_continuous.cpp -o $(BUILDDIR)/ompAerialImage_gold_continuous.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_1.o: ompAerialImage_sse_1.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(SSEFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_1.cpp -o $(BUILDDIR)/ompAerialImage_sse_1.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_1_test.o: ompAerialImage_sse_1_test.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(SSEFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_1_test.cpp -o $(BUILDDIR)/ompAerialImage_sse_1_test.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_2.o: ompAerialImage_sse_2.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_2.cpp -o $(BUILDDIR)/ompAerialImage_sse_2.o $(TCMALLOC_LINK) 

$(BUILDDIR)/ompAerialImage_sse_2_2.o: ompAerialImage_sse_2_2.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_2_2.cpp -o $(BUILDDIR)/ompAerialImage_sse_2_2.o $(TCMALLOC_LINK) 

$(BUILDDIR)/ompAerialImage_sse_2_test.o: ompAerialImage_sse_2_test.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_2_test.cpp -o $(BUILDDIR)/ompAerialImage_sse_2_test.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_2_2_test.o: ompAerialImage_sse_2_2_test.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_2_2_test.cpp -o $(BUILDDIR)/ompAerialImage_sse_2_2_test.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_2_iter.o: ompAerialImage_sse_2_iter.cpp datastruct.h ompAerialImage.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_2_iter.cpp -o $(BUILDDIR)/ompAerialImage_sse_2_iter.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_3.o: ompAerialImage_sse_3.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_3.cpp -o $(BUILDDIR)/ompAerialImage_sse_3.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_3_1.o: ompAerialImage_sse_3_1.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_3_1.cpp -o $(BUILDDIR)/ompAerialImage_sse_3_1.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_4.o: ompAerialImage_sse_4.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_4.cpp -o $(BUILDDIR)/ompAerialImage_sse_4.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_4_test.o: ompAerialImage_sse_4_test.cpp sseAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_4_test.cpp -o $(BUILDDIR)/ompAerialImage_sse_4_test.o $(TCMALLOC_LINK)

$(BUILDDIR)/ompAerialImage_sse_4_test_double.o: ompAerialImage_sse_4_test_double.cpp ompAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) $(OMPFLAGS) -c ompAerialImage_sse_4_test_double.cpp -o $(BUILDDIR)/ompAerialImage_sse_4_test_double.o $(TCMALLOC_LINK)

$(BUILDDIR)/main.o: main.cpp goldAerialImage.h sseAerialImage.h ompAerialImage.h datastruct.h
	$(CC) $(DEFINE) $(CPPFLAGS) -c main.cpp -o $(BUILDDIR)/main.o $(TCMALLOC_LINK)

clean: 
	rm -f $(EXE) $(DEBUG_EXE) $(OBJ)
