#include "datastruct.h"
#include "cudaAerialImage_PIXEL.h"
#include "cudaAerialImage_LUT.h"
#include "goldAerialImage.h"
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cmath>

using namespace std;

float maxError(const float* cudaImage, const float* goldImage, int imageSize);
//bool checkResult(const float*, const float*, const Rectangle);
void readTable(const char*, float (*) [TABLE_SIZE*TABLE_SIZE]);
void readKernel(const char*, float (*) [KERNEL_SIZE*KERNEL_SIZE]);
void writeImage(const char*, const float *, const Rectangle);
void readPolygon(const char*, Rectangle *&, Rectangle &, INT4&);
void readKernelWeight(const char*, float*);

int main(int argc, char **argv)
{

    if(argc!=3){
	cout<<"input error:"<<endl;
	cout<<"\tprintimage inputfile"<<endl;
	return 0;
    }

    cout<<"running..."<<endl;
// data setup here

    // kernel weight array
    float weightArray[KERNEL_NUMBER];

    // table is build double, odd for real, even for image
    float tableArray[TABLE_NUMBER][TABLE_SIZE*TABLE_SIZE];
    float kernelArray[KERNEL_NUMBER][KERNEL_SIZE*KERNEL_SIZE];

    // rectNum in the polygonArray
    INT4 rectNum;
    // input of the polygonArray
    Rectangle *polygonArray;
    // input of the layout bound
    Rectangle boundBox;

    cout<<"Reading polygon list"<<endl;
    readPolygon(argv[1], polygonArray, boundBox, rectNum);

    cout<<"Reading table list"<<endl;

#ifndef DDEBUG
    readTable("tablelist", tableArray);
#else
    readTable("testlist", tableArray);
#endif

//    cout<<"reading kernel list"<<endl;
//    readKernel("kernellist", kernelArray);

    cout<<"reading weight list"<<endl;
#ifndef DDEBUG
    readKernelWeight("lookuptable/weight.txt", weightArray);
#else
    readKernelWeight("lookuptable/test_weight.txt", weightArray);
#endif

// cuda and gold code starts here

    float *goldImage;
    float cudaTime;
    float goldTime;

    int imageSize = ((boundBox.right-boundBox.left)/GRID_SIZE)*((boundBox.top-boundBox.bottom)/GRID_SIZE);
    goldImage = (float*) malloc(imageSize*sizeof(float));

    cout << "running gold code..." << endl;
    goldPrintImage(polygonArray,
	    rectNum,
	    weightArray,
	    tableArray,
	    goldImage,
	    goldTime,
	    boundBox);

    cout<<"gold run time is "<<goldTime<<" s"<<endl;
    writeImage("goldImage.txt", goldImage, boundBox);
    float max_error;

    cout << "running cuda code..." << endl;
    // build up your own code for this function
    // no parameter change is allowed

    cout << "command is "<< argv[2] << endl;
    string command(argv[2]);

    bool all = false;
    int index = 0;
    if (command=="all"){
	all = true;
    } else {
	index = atoi(argv[2]);
    }

    cout << "command is "<< command << endl;

#define NONE 0
#define LUT_1 1
#define LUT_2 2
#define LUT_3 3
#define PIXEL_1 4
#define PIXEL_2 5
#define PIXEL_3 6
#define PIXEL_4 7 //the best
#define PIXEL_5 8 //the best
#define PIXEL_6 9 //error...
#define PIXEL_7 10 //....
#define PIXEL_8 11 //doesn't work
#define UCLA 12 //compare
#define SHARE_1 13 //recently changes... (local memory->shared memory)

    /* LUT_1 */
/*    if (all || index==LUT_1){
	cout << endl << "=======  running cuda LUT_1  ======" << endl;
	float * cudaImage_LUT_1 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_LUT_1(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		boundBox,
		cudaImage_LUT_1,
		cudaTime);

	writeImage("cudaImage_LUT_1.txt", cudaImage_LUT_1, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_LUT_1, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda LUT_1 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_LUT_1);

	cout << endl << endl;
    }
*/

/* PIXEL_7 */
	cout << endl << "=======  running cuda initiate P7  ======" << endl;
	float * cudaImage_PIXEL_7 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_7(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_7,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_7.txt", cudaImage_PIXEL_7, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_7, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_7 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_7);

	cout << endl << endl;



    /* LUT_2 */
    if (all || index==LUT_2){
	cout << endl << "=======  running cuda LUT_2  ======" << endl;
	float * cudaImage_LUT_2 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_LUT_2(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		boundBox,
		cudaImage_LUT_2,
		cudaTime);

	writeImage("cudaImage_LUT_2.txt", cudaImage_LUT_2, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_LUT_2, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda LUT_2 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_LUT_2);

	cout << endl << endl;
    }


    /* LUT_3 */
    if (all || index==LUT_3){
	cout << endl << "=======  running cuda LUT_3  ======" << endl;
	float * cudaImage_LUT_3 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_LUT_3(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		boundBox,
		cudaImage_LUT_3,
		cudaTime);

	writeImage("cudaImage_LUT_3.txt", cudaImage_LUT_3, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_LUT_3, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda LUT_3 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;
	free(cudaImage_LUT_3);
	cout << endl << endl;
    }


    /* PIXEL_1 */
    if (all || index==PIXEL_1){
	cout << endl << "=======  running cuda PIXEL_1  ======" << endl;
	float * cudaImage_PIXEL_1 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_1(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_1,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_1.txt", cudaImage_PIXEL_1, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_1, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_1 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_1);

	cout << endl << endl;
    }
    /* PIXEL_2 */
    if (all || index==PIXEL_2){
	cout << endl << "=======  running cuda PIXEL_2  ======" << endl;
	float * cudaImage_PIXEL_2 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_2(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_2,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_2.txt", cudaImage_PIXEL_2, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_2, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_2 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_2);

	cout << endl << endl;
    }

    /* PIXEL_3 */
    if (all || index==PIXEL_3){
	cout << endl << "=======  running cuda PIXEL_3  ======" << endl;
	float * cudaImage_PIXEL_3 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_3(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_3,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_3.txt", cudaImage_PIXEL_3, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_3, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_3 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_3);

	cout << endl << endl;
    }

    /* PIXEL_4 */
    if (all || index==PIXEL_4){
	cout << endl << "=======  running cuda PIXEL_4  ======" << endl;
	float * cudaImage_PIXEL_4 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_4(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_4,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_4.txt", cudaImage_PIXEL_4, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_4, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_4 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_4);

	cout << endl << endl;
    }

    /* PIXEL_5 */
    if (all || index==PIXEL_5){
	cout << endl << "=======  running cuda PIXEL_5  ======" << endl;
	float * cudaImage_PIXEL_5 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_5(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_5,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_5.txt", cudaImage_PIXEL_5, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_5, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_5 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_5);

	cout << endl << endl;
    }

    /* PIXEL_6 */
    if (all || index==PIXEL_6){
	cout << endl << "=======  running cuda PIXEL_6  ======" << endl;
	float * cudaImage_PIXEL_6 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_6(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_6,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_6.txt", cudaImage_PIXEL_6, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_6, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_6 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_6);

	cout << endl << endl;
    }
    
    /* PIXEL_7 */
    if (all || index==PIXEL_7){
	cout << endl << "=======  running cuda PIXEL_7  ======" << endl;
	float * cudaImage_PIXEL_7 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_7(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_7,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_7.txt", cudaImage_PIXEL_7, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_7, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_7 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_7);

	cout << endl << endl;
    }

    /* PIXEL_8 */
    if (all || index==PIXEL_8){
	cout << endl << "=======  running cuda PIXEL_8  ======" << endl;
	float * cudaImage_PIXEL_8 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_PIXEL_8(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_PIXEL_8,
		cudaTime,
		boundBox);

	writeImage("cudaImage_PIXEL_8.txt", cudaImage_PIXEL_8, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_PIXEL_8, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda PIXEL_8 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_PIXEL_8);

	cout << endl << endl;
    }

    /* UCLA version */
    if (all || index==UCLA) {
	cout << endl <<"======== running cuda UCLA =========" << endl;
	float * cudaImage_UCLA = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_UCLA(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_UCLA,
		cudaTime,
		boundBox);
	writeImage("cudaImage_UCLA.txt", cudaImage_UCLA, boundBox);

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_UCLA, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	cout << "cuda UCLA run time is " << cudaTime << " s" << endl;
	cout << "speed up is "<<goldTime/cudaTime << " X" << endl;

	free(cudaImage_UCLA);

	cout << endl << endl;
    }

    /* SHARE_1 */
    if (all || index==SHARE_1){
	cout << endl << "=======  running cuda SHARE_1  ======" << endl;
	float * cudaImage_SHARE_1 = (float*) malloc(imageSize*sizeof(float));
	cudaPrintImage_SHARE_1(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_SHARE_1,
		cudaTime,
		boundBox);

	writeImage("cudaImage_SHARE_1.txt", cudaImage_SHARE_1, boundBox); 

	cout << "comparing result..." << endl;
	max_error = maxError(cudaImage_SHARE_1, goldImage, imageSize);

	cout << "max error = " << max_error << endl;
	printf("cuda SHARE_1 run time is %.4f s", cudaTime);
	cout<<"speed up is "<<goldTime/cudaTime<<" X"<<endl;

	free(cudaImage_SHARE_1);

	cout << endl << endl;
    }


    /* FFT2 */
/*    if (all || index == FFT2){
	cout << endl; << "======== running cuda FFT2 =========" << endl;
	float * cudaImage_FFT2 = (float *) malloc (imageSize * sizeof(float));
	// create a cuda Image result matrix

	cudaPrintImage_FFT2(polygonArray,
		rectNum,
		weightArray,
		tableArray,
		cudaImage_FFT2,
		cudaTime,
		boundBox);
	writeImage("cudaImage_FFT2.txt", cudaImage_FFT2, boundBox);

	cout << "comparing result ..." << endl;
	max_error = maxError(cudaImage_FFT2, goldImage, imageSize);

	cout << "Max error = " << max_error << endl;
	cout << "Cuda FFT2 run time is " << cudaTime << " s" <<endl;
	cout << "Speed up is " << goldTime/cudaTime << " X" <<endl;

	free(cudaImage_FFT2);
	
	cout << endl << endl;
    }
*/    
    free(goldImage);
    free(polygonArray);
    return 1;
}

float maxError(const float* cudaImage, const float* goldImage, int imageSize)
{
    float max_error = .0;
    float max_rel_error = 0.0;
    float currentGold = 0.0;
    float currentGold_rel = 0.0;
    for (int i=0;i<imageSize;++i) {
	//DEBUG
//	    cout << "cuda: " << *(cudaImage) << "\t gold: " << *(goldImage) << "\t diff: " << fabs(*(cudaImage) - *(goldImage)) << endl;
	float e = fabs(*(cudaImage) - *(goldImage))/ *(cudaImage);
	if (max_rel_error < e){
	    max_rel_error = e;
	    currentGold_rel = *(goldImage);
	}
	e = fabs(*(cudaImage) - *(goldImage));
	if (max_error < e){
	    max_error = e;
	    currentGold = *(goldImage);
	}
//	max_rel_error = max(max_rel_error, fabs(*(cudaImage) - *(goldImage))/ *(cudaImage));
//	currentGold = *(goldImage);
//	max_error = max(max_error, fabs(*(cudaImage++) - *(goldImage++)));
	cudaImage++;
	goldImage++;
    }
    cout << "Gold " << currentGold_rel << " relative error " << max_rel_error << endl;
    cout << "Gold " << currentGold << " error " << max_error << endl;
    return max_error;
}

void readKernel(const char* kernelfilelist, float kernelArray[KERNEL_NUMBER][KERNEL_SIZE*KERNEL_SIZE])
{
    cout<<kernelfilelist<<endl;

    ifstream kernellist;

    kernellist.open(kernelfilelist, fstream::in);
    if (!kernellist.is_open()){
	cout<<"Error in reading file "<<kernelfilelist<<endl;
	exit(1);
    }
    string kernelname;
    ifstream tfs;
    float *elementIdx;
    for (int i=0;i<KERNEL_NUMBER;i++){
	kernellist>>kernelname;
	tfs.open(kernelname.c_str(), fstream::in);
	if (!tfs.is_open()){
	    cout<<"Error in reading file "<<kernelname<<endl;
	    exit(1);
	}
	elementIdx = kernelArray[i];
	while (!tfs.eof())
	    tfs>>*(elementIdx++);
	tfs.close();
    }
    kernellist.close();
}

void readTable(const char* tablefilelist, float tableArray[TABLE_NUMBER][TABLE_SIZE*TABLE_SIZE])
{
    ifstream tablelist;
    tablelist.open(tablefilelist, fstream::in);
    if (!tablelist.is_open()){
	cout<<"Error in reading file "<<tablefilelist<<endl;
	exit(1);
    }
    string tablename;
    ifstream tfs;
    float *elementIdx;
    for (int i=0;i<TABLE_NUMBER;i++){
	tablelist>>tablename;
	tfs.open(tablename.c_str(), fstream::in);
	if (!tfs.is_open()){
	    cout<<"Error in reading file "<<tablename<<endl;
	    exit(1);
	}
	elementIdx = tableArray[i];
	while (!tfs.eof())
	    tfs>>*(elementIdx++);
	tfs.close();
    }
    tablelist.close();
}

void readKernelWeight(const char * weightfile, float weightArray[KERNEL_NUMBER])
{
    ifstream ifs;
    ifs.open(weightfile, fstream::in);
    if (!ifs.is_open()){
	cout<<"Error in reading file "<<weightfile<<endl;
	exit(1);
    }
    for (int i=0;i<KERNEL_NUMBER;i++){
	ifs>>*(weightArray++);
    }
    ifs.close();
}

void writeImage(const char * outfile, const float * image,const Rectangle boundBox)
{
    int xLength, yLength;
    ofstream ofs;
    ofs.open(outfile, fstream::out);
    const float * imageidx = image;
    xLength = (boundBox.right - boundBox.left) / GRID_SIZE;
    yLength = (boundBox.top - boundBox.bottom) / GRID_SIZE;

    for (int j=0;j<yLength;++j){
	for (int i=0;i<xLength;++i){
	    ofs<<*(imageidx++)<<" ";
	}
	ofs<<endl;
    }
    ofs.close();
}

void readPolygon(const char * filename, Rectangle *& polygonArray, Rectangle &boundBox, INT4 &polygonNum)
{
    ifstream ifs;
    ifs.open(filename, fstream::in);
    if (!ifs.is_open()){
	cout<<"Error in opening "<<filename<<endl;
	exit(1);
    }
    int useless;
    
    ifs>>polygonNum;
    polygonArray = (Rectangle*) malloc(sizeof(Rectangle) * polygonNum);
    int i=0;
    ifs>>boundBox.left;
    ifs>>boundBox.bottom;
    ifs>>boundBox.right;
    ifs>>boundBox.top;
    while(!ifs.eof()){
	ifs>>polygonArray[i].left;
	ifs>>polygonArray[i].top;
	ifs>>polygonArray[i].right;
	ifs>>useless;
	ifs>>useless;
	ifs>>useless;
	ifs>>useless;
	ifs>>polygonArray[i].bottom;
	++i;
    }

    ifs.close();
}
