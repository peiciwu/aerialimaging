/*
 * Copyright 1993-2007 NVIDIA Corporation.  All rights reserved.
 *
 * NOTICE TO USER:
 *
 * This source code is subject to NVIDIA ownership rights under U.S. and
 * international Copyright laws.  Users and possessors of this source code
 * are hereby granted a nonexclusive, royalty-free license to use this code
 * in individual and commercial software.
 *
 * NVIDIA MAKES NO REPRESENTATION ABOUT THE SUITABILITY OF THIS SOURCE
 * CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT EXPRESS OR
 * IMPLIED WARRANTY OF ANY KIND.  NVIDIA DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOURCE CODE, INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
 * IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS,  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE
 * OR OTHER TORTIOUS ACTION,  ARISING OUT OF OR IN CONNECTION WITH THE USE
 * OR PERFORMANCE OF THIS SOURCE CODE.
 *
 * U.S. Government End Users.   This source code is a "commercial item" as
 * that term is defined at  48 C.F.R. 2.101 (OCT 1995), consisting  of
 * "commercial computer  software"  and "commercial computer software
 * documentation" as such terms are  used in 48 C.F.R. 12.212 (SEPT 1995)
 * and is provided to the U.S. Government only as a commercial end item.
 * Consistent with 48 C.F.R.12.212 and 48 C.F.R. 227.7202-1 through
 * 227.7202-4 (JUNE 1995), all U.S. Government End Users acquire the
 * source code with only those rights set forth herein.
 *
 * Any use of this source code in individual and commercial software must
 * include, in the user documentation and internal comments to the code,
 * the above Disclaimer and U.S. Government End Users Notice.
 */

/* Matrix transpose with Cuda
* Device code.
*/

#ifndef _LITHO_KERNEL_H_
#define _LITHO_KERNEL_H_
#define BLOCK_DIM 16
#define N 200
#define pixel_max 40
#define IMUL(a, b) __mul24(a, b)



//Input data texture reference
texture<float, 2, cudaReadModeElementType> texData;

// This code implemented the rectangle based litho  simulation
//   It also uses __mul24 for 
// fast integer arithmetic when computing indices.  

__global__ void litho( const int* input_x,const int* input_y, float* img_data,float *kernel)
{
   __shared__ float img_data_shared[pixel_max*pixel_max];
    //__shared__ float kernel_shared[pixel_max*pixel_max];
  
	for( unsigned int x=threadIdx.x;x<pixel_max;x+=blockDim.x)
		for( unsigned int y=threadIdx.y;y<pixel_max;y+=blockDim.y)	
				img_data_shared[x*pixel_max+y]=1.0f/gridDim.x;
				
	const unsigned int const1=IMUL(pixel_max,5);
	const unsigned int const2=IMUL(pixel_max,10);
	
   //__syncthreads();
    for( unsigned int i = blockIdx.x; i < 2*N; i+=gridDim.x) 
    {	int dx=input_x[i];
		int dy=input_y[i];
		for( unsigned int x=(threadIdx.x);x<pixel_max;x+=(blockDim.x))
			for( unsigned int y=(threadIdx.y);y<pixel_max;y+=(blockDim.y))
			{
				int dx_diff=dx-IMUL(x,5)+const1;
				int dy_diff=dy-IMUL(y,5)+const1;
				if(dx_diff<0) dx_diff=0;
				if(dx_diff>=const2) dx_diff=const2-1;
		
				if(dy_diff<0) dy_diff=0;
				if(dy_diff>=const2) dy_diff=const2-1;
			
				if ((i%2)==0) img_data_shared[x*pixel_max+y]+=tex2D(texData,dx_diff,dy_diff);	else img_data_shared[x*pixel_max+y]-=tex2D(texData,dx_diff,dy_diff);
			//   if(i%2==0) img_data_shared[x*pixel_max+y]+=kernel[dx_diff*const2+dy_diff];else img_data_shared[x*pixel_max+y]-=kernel[dx_diff*const2+dy_diff];
	
			}
			__syncthreads();		
    }  
		 
		   
  	 for( unsigned int x=threadIdx.x;x<pixel_max;x+=blockDim.x)
		for( unsigned int y=threadIdx.y;y<pixel_max;y+=blockDim.y)	
				img_data[blockIdx.x*(pixel_max*pixel_max)+x*pixel_max+y]=img_data_shared[x*pixel_max+y];
				
		  __syncthreads();		
} 


#endif // _LITHO_KERNEL_H_
