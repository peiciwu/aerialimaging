#include <complex>
#include "goldAerialImage.h"
#include "fft.h"
#include "datastruct.h"
#include <stdlib.h>
#include <math.h>
#include <string.h>

using namespace std;

#define SAMPLE_RATIO 2 

int upper2pow(int k);

extern int write_bmp(const char *filename, int width, int height, char *rgb);

void mul(complex<double>*, int,	int,
	complex<double> *, int, int,
	complex<double>*, int);

void draw_layout_bmp(complex<double> *layout, size_t height, size_t width, const char *filename)
{
//    size_t WIDTH = width;
    size_t WIDTH = (width/4+!!(width%4))*4;
    char * rgb = (char*) malloc (3*height*WIDTH*sizeof(char));
    size_t i=0;
    for (size_t y=0; y<height; ++y){
        for (size_t x=0;x<WIDTH; ++x){
            uint8_t value;
            uint8_t color;
            if (x<width){
                value = (uint8_t) layout[y*width+x].real();
//                color = value;
                color = (value==0) ? 0: 255;
            } else color = 0;
            rgb[i++] = color;
            rgb[i++] = color;
            rgb[i++] = color;
        }
    }
    write_bmp(filename, WIDTH, height, rgb);
    free(rgb);
}


void goldPrintImage_fft(Rectangle* polygonArray,
	    INT4 rectNum,
	    float* weightArray,
	    float kernelArray[KERNEL_NUMBER*2][TABLE_SIZE*TABLE_SIZE],
	    float* &goldImage,
	    float &goldTime,
	    Rectangle boundBox)
{
    // your code starts here
    clock_t start = clock();

    int boundwidth = (boundBox.right-boundBox.left) / SAMPLE_RATIO;
    int boundheight = (boundBox.top-boundBox.bottom) / SAMPLE_RATIO;
    int imagewidth = (boundBox.right-boundBox.left)/GRID_SIZE;
    int imageheight = (boundBox.top-boundBox.bottom)/GRID_SIZE;

    int width = upper2pow(boundwidth);
    int height = upper2pow(boundheight);
    int iwidth = upper2pow(imagewidth);
    int iheight = upper2pow(imageheight);
    int ikernel = upper2pow(KERNEL_SIZE);

    complex<double> * layout = (complex<double> *) malloc(sizeof(complex<double>)*width*height);

    int bot, top, lef, rig;
    for (int i=0;i < rectNum;i++){
	bot = (polygonArray[i].bottom - boundBox.bottom)/SAMPLE_RATIO;
	top = (polygonArray[i].top    - boundBox.bottom)/SAMPLE_RATIO;
	lef = (polygonArray[i].left   - boundBox.left)  /SAMPLE_RATIO;
	rig = (polygonArray[i].right  - boundBox.left)  /SAMPLE_RATIO;
	for (int j = bot;j < top; j++)
	    for (int k = lef;k < rig; k++)
		layout[j*width+k] = complex<double>(1,0);
    }
    draw_layout_bmp(layout, height, width, "input_layout.bmp");


    complex<double>* templayout = (complex<double> *) malloc(sizeof(complex<double>)*width*height);
    complex<double>* kernel = (complex<double> *) malloc(sizeof(complex<double>)*ikernel*ikernel);
    complex<double>* field = (complex<double> *) malloc(sizeof(complex<double>)*imagewidth*imageheight);
    for (int m=0;m<KERNEL_NUMBER;m++){
	memcpy(templayout, layout, sizeof(complex<double>)*width*height);
	for (int i=0;i<ikernel;i++)
	    for (int j=0;j<ikernel;j++)
		if (i<KERNEL_SIZE && j<KERNEL_SIZE)
		    kernel[i*ikernel+j]=complex<double>(kernelArray[2*m][i*KERNEL_SIZE+j], kernelArray[2*m+1][i*KERNEL_SIZE+j]);
		else
		    kernel[i*ikernel+j]=complex<double>(0,0);

	fft2(ikernel, ikernel, kernel);
	fft2(height, width, templayout);
	mul(field, iheight, iwidth, templayout, height, width, kernel, ikernel);
	ifft2(iheight, iwidth, field);

	for (int j=0;j<imageheight;j++)
	    for (int i=0;i<imagewidth;i++){
		goldImage[j*imagewidth+i] += (field[j*iwidth+i].real()*field[j*iwidth+i].real()+
			                      field[j*iwidth+i].imag()*field[j*iwidth+i].imag())*weightArray[m];
	    }
    }
    free(layout);
    free(templayout);
    free(kernel);
    free(field);

    // in the end report the run time by modifying the value of goldTime;
    goldTime = (clock()-start)/(double)CLOCKS_PER_SEC;
}

int upper2pow(int k)
{
    int m=1;
    while ((k>>m)>1){
	m++;
    }
    int width = 1<<m;
    if (width<k)
	width = width<<1;
    return width;
}

void mul(complex<double>* field, int imageheight, int imagewidth, complex<double> *layout, int height, int width, complex<double>* kernel, int ikernel)
{
    for (int i=0;i<imageheight;++i){
	for (int j=0;j<imagewidth;++j){
	    if (i<height && i<ikernel && j<width && j<ikernel)
		field[i*imagewidth+j] = layout[i*width+j]*kernel[i*ikernel+j];
	    else field[i*imagewidth+j] = complex<double>(0,0);
	}
    }
}

